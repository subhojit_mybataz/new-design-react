# frozen_string_literal: true

Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  config.serve_static_assets = true

  # Show full error reports.
  config.consider_all_requests_local = true

  # Enable/disable caching. By default caching is disabled.
  if Rails.root.join('tmp/caching-dev.txt').exist?
    config.action_controller.perform_caching = true

    config.cache_store = :memory_store
    config.public_file_server.headers = {
      'Cache-Control' => 'public, max-age=172800'
    }
  else
    config.action_controller.perform_caching = false

    config.cache_store = :null_store
  end

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = true

  config.action_mailer.perform_caching = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Suppress logger output for asset requests.
  config.assets.quiet = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true

  # Use an evented file watcher to asynchronously detect changes in source code,
  # routes, locales, etc. This feature depends on the listen gem.
  config.file_watcher = ActiveSupport::EventedFileUpdateChecker

  # Added for devise as per https://github.com/plataformatec/devise#getting-started
  config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }
  config.action_mailer.perform_deliveries = true
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.default_options = { from: 'noreply@mybataz.com' }
  # config.action_mailer.smtp_settings = {
  #   :address => "email-smtp.us-east-1.amazonaws.com",
  #   :port => 587,
  #   :domain => "mybataz.com",
  #   :authentication => 'plain',
  #   :user_name => Rails.application.secrets.MAIL_ID,
  #   :password => Rails.application.secrets.MAIL_PASSWORD,
  #   :enable_starttls_auto => true
  # }

  config.action_mailer.smtp_settings = {
    domain: 'sendgrid.mybataz.com',
    user_name: 'mybataz_email',
    password: 'USER@mybataz001',
    address: 'smtp.sendgrid.net',
    port: 587,
    authentication: :plain,
    enable_starttls_auto: true
  }

  config.FACEBOOK_APP_ID = '1764962843515579'
  config.FACEBOOK_APP_SECRET = 'aeacd7fafe63616d9abe51afa942fcd4'
  config.FACEBOOK_CALLBACK_URL = 'http://localhost:3000/users/auth/facebook/callback'

  config.GOOGLE_APP_ID = '325976187343-fc7ef8q11e5o0h3f6iai8cp8b4af48ia.apps.googleusercontent.com'
  config.GOOGLE_APP_SECRET = 'VI3oL-MoHbu7qUimDmiZwlo4'
  config.GOOGLE_CALLBACK_URL = 'http://localhost:3000/users/auth/google_oauth2/callback'

  config.INSTAGRAM_APP_ID = 'a975892b9cae41cead246784af24243d'
  config.INSTAGRAM_APP_SECRET = 'a822a15d3d7640ce9464c99dfbf09ab1'
  config.INSTAGRAM_CALLBACK_URL = 'http://localhost:3000/users/auth/instagram/callback'

  config.SLACK_ERROR_WEBHOOK_URL = 'https://hooks.slack.com/services/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'

  # config.paperclip_defaults = {
  #   storage: :s3,
  #   s3_host_name: Rails.application.secrets.S3_HOSTNAME,
  #   s3_protocol: :https,
  #   url: ':s3_alias_url',
  #   s3_host_alias: Rails.application.secrets.S3_HOST_ALIAS,
  #   path: 'attachments/:id/:style',
  #   s3_credentials: {
  #     bucket: Rails.application.secrets.S3_BUCKET,
  #     access_key_id: Rails.application.secrets.AWS_ACCESS_KEY_ID,
  #     secret_access_key: Rails.application.secrets.AWS_SECRET_ACCESS_KEY,
  #     s3_region: Rails.application.secrets.S3_REGION
  #   }
  # }
  config.paperclip_defaults = {
    storage: :s3,
    s3_host_name: Rails.application.secrets.S3_HOSTNAME,
    s3_protocol: :https,
    s3_credentials: {
      bucket: Rails.application.secrets.S3_BUCKET,
      access_key_id: Rails.application.secrets.AWS_ACCESS_KEY_ID,
      secret_access_key: Rails.application.secrets.AWS_SECRET_ACCESS_KEY,
      s3_region: Rails.application.secrets.S3_REGION,
    }
  }
end
