# frozen_string_literal: true

Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # Code is not reloaded between requests.
  config.cache_classes = true

  # Eager load code on boot. This eager loads most of Rails and
  # your application in memory, allowing both threaded web servers
  # and those relying on copy on write to perform better.
  # Rake tasks automatically ignore this option for performance.
  config.eager_load = true

  # Full error reports are disabled and caching is turned on.
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  # Disable serving static files from the `/public` folder by default since
  # Apache or NGINX already handles this.
  config.public_file_server.enabled = ENV['RAILS_SERVE_STATIC_FILES'].present?

  # Compress JavaScripts and CSS.
  config.assets.js_compressor = :uglifier
  # config.assets.css_compressor = :sass

  # Do not fallback to assets pipeline if a precompiled asset is missed.
  config.assets.compile = false

  config.serve_static_assets = true

  # `config.assets.precompile` and `config.assets.version` have moved to config/initializers/assets.rb

  # Enable serving of images, stylesheets, and JavaScripts from an asset server.
  # config.action_controller.asset_host = 'http://assets.example.com'

  # Specifies the header that your server uses for sending files.
  # config.action_dispatch.x_sendfile_header = 'X-Sendfile' # for Apache
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for NGINX

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  # config.force_ssl = true

  # Use the lowest log level to ensure availability of diagnostic information
  # when problems arise.
  config.log_level = :debug

  # Prepend all log lines with the following tags.
  config.log_tags = [:request_id]

  # Use a different cache store in production.
  # config.cache_store = :mem_cache_store

  # Use a real queuing backend for Active Job (and separate queues per environment)
  # config.active_job.queue_adapter     = :resque
  # config.active_job.queue_name_prefix = "rails-app_#{Rails.env}"
  config.action_mailer.perform_caching = false

  # Ignore bad email addresses and do not raise email delivery errors.
  # Set this to true and configure the email server for immediate delivery to raise delivery errors.
  # config.action_mailer.raise_delivery_errors = false

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation cannot be found).
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners.
  config.active_support.deprecation = :notify

  # Use default logging formatter so that PID and timestamp are not suppressed.
  config.log_formatter = ::Logger::Formatter.new

  # config.paperclip_defaults = {
  #:storage => :s3,
  #:s3_host_name => 'REMOVE_THIS_LINE_IF_UNNECESSARY',
  #:bucket => 'S3_BUCKET_NAME'
  # }

  config.paperclip_defaults = {
    storage: :s3,
    s3_host_name: Rails.application.secrets.S3_HOSTNAME,
    s3_protocol: :https,
    url: ':s3_alias_url',
    s3_host_alias: Rails.application.secrets.S3_HOST_ALIAS,
    s3_credentials: {
      bucket: Rails.application.secrets.S3_BUCKET,
      access_key_id: Rails.application.secrets.AWS_ACCESS_KEY_ID,
      secret_access_key: Rails.application.secrets.AWS_SECRET_ACCESS_KEY,
      s3_region: Rails.application.secrets.S3_REGION
    }
  }

  # Use a different logger for distributed setups.
  # require 'syslog/logger'
  # config.logger = ActiveSupport::TaggedLogging.new(Syslog::Logger.new 'app-name')

  if ENV['RAILS_LOG_TO_STDOUT'].present?
    logger           = ActiveSupport::Logger.new(STDOUT)
    logger.formatter = config.log_formatter
    config.logger = ActiveSupport::TaggedLogging.new(logger)
  end

  # Do not dump schema after migrations.
  config.active_record.dump_schema_after_migration = false

  # TODO: Need to change this
  config.action_mailer.default_url_options = { host: 'mybataz.com' }
  config.action_mailer.perform_deliveries = true
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.default_options = { from: 'noreply@mybataz.com' }

  config.action_mailer.smtp_settings = {
    domain: 'sendgrid.mybataz.com',
    user_name: 'mybataz_email',
    password: 'USER@mybataz001',
    address: 'smtp.sendgrid.net',
    port: 587,
    authentication: :plain,
    enable_starttls_auto: true
  }

  # Assign values to these as specified by Facebook
  config.FACEBOOK_APP_ID = Rails.application.secrets.FACEBOOK_APP_ID
  config.FACEBOOK_APP_SECRET = Rails.application.secrets.FACEBOOK_APP_SECRET
  config.FACEBOOK_CALLBACK_URL = Rails.application.secrets.FACEBOOK_CALLBACK_URL

  # Commented for now to not send emails from the server on exceptions
  # Rails.application.config.middleware.use ExceptionNotification::Rack,
  #:email => {
  #:email_prefix => "[Bataz Exception] ",
  #:sender_address => %{"Bataz Production" <#{Rails.application.secrets.MAIL_ID}>},
  #:exception_recipients => %w{aniruddh@akrity.com rathan.kumar@akrity.com}
  # }
  config.GOOGLE_APP_ID = '325976187343-fc7ef8q11e5o0h3f6iai8cp8b4af48ia.apps.googleusercontent.com'
  config.GOOGLE_APP_SECRET = 'VI3oL-MoHbu7qUimDmiZwlo4'
  config.GOOGLE_CALLBACK_URL = 'http://localhost:3000/users/auth/google_oauth2/callback'

  config.INSTAGRAM_APP_ID = 'a975892b9cae41cead246784af24243d'
  config.INSTAGRAM_APP_SECRET = 'a822a15d3d7640ce9464c99dfbf09ab1'
  config.INSTAGRAM_CALLBACK_URL = 'http://localhost:3000/users/auth/instagram/callback'

  config.SLACK_ERROR_WEBHOOK_URL = 'https://hooks.slack.com/services/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'
end
