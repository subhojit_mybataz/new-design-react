# frozen_string_literal: true

Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # The test environment is used exclusively to run your application's
  # test suite. You never need to work with it otherwise. Remember that
  # your test database is "scratch space" for the test suite and is wiped
  # and recreated between test runs. Don't rely on the data there!
  config.cache_classes = true

  # Do not eager load code on boot. This avoids loading your whole application
  # just for the purpose of running a single test. If you are using a tool that
  # preloads Rails for running tests, you may have to set it to true.
  config.eager_load = true

  # Configure public file server for tests with Cache-Control for performance.
  config.public_file_server.enabled = true
  config.public_file_server.headers = {
    'Cache-Control' => 'public, max-age=3600'
  }

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Raise exceptions instead of rendering exception templates.
  config.action_dispatch.show_exceptions = false

  # Disable request forgery protection in test environment.
  config.action_controller.allow_forgery_protection = false
  config.action_mailer.perform_caching = false

  # Tell Action Mailer not to deliver emails to the real world.
  # The :test delivery method accumulates sent emails in the
  # ActionMailer::Base.deliveries array.
  config.action_mailer.delivery_method = :test

  # Print deprecation notices to the stderr.
  config.active_support.deprecation = :stderr

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true
  config.FACEBOOK_APP_ID = '1764962843515579'
  config.FACEBOOK_APP_SECRET = 'aeacd7fafe63616d9abe51afa942fcd4'
  config.FACEBOOK_CALLBACK_URL = 'http://localhost:3000/users/auth/facebook/callback'

  config.GOOGLE_APP_ID = '325976187343-fc7ef8q11e5o0h3f6iai8cp8b4af48ia.apps.googleusercontent.com'
  config.GOOGLE_APP_SECRET = 'VI3oL-MoHbu7qUimDmiZwlo4'
  config.GOOGLE_CALLBACK_URL = 'http://localhost:3000/users/auth/google_oauth2/callback'

  config.INSTAGRAM_APP_ID = 'a975892b9cae41cead246784af24243d'
  config.INSTAGRAM_APP_SECRET = 'a822a15d3d7640ce9464c99dfbf09ab1'
  config.INSTAGRAM_CALLBACK_URL = 'http://localhost:3000/users/auth/instagram/callback'
end
