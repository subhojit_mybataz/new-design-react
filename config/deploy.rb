# frozen_string_literal: true

# config valid only for current version of Capistrano
lock '3.4.1'
require 'slack-notifier'
set :application, 'mybataz.com'
set :repo_url, 'git@bitbucket.org:mybataz/rails-application.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

namespace :deploy do
  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

  after :finished, :notify do
    env = fetch(:rails_env)
    branch = fetch(:branch)
    notifier = Slack::Notifier.new 'https://hooks.slack.com/services/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' do
      defaults username: 'echidna'
    end
    text = "Latest code has been deployed for the following \n\n Branch:  #{branch} \n\n Enviroment: #{env}"
    res = notifier.post text: text, icon_url: 'https://img.purch.com/w/660/aHR0cDovL3d3dy5saXZlc2NpZW5jZS5jb20vaW1hZ2VzL2kvMDAwLzA4OC80Mjkvb3JpZ2luYWwvZWNoaWRuYS5qcGc='
  end

  after :failed, :notify_failure do
    env = fetch(:rails_env)
    branch = fetch(:branch)
    notifier = Slack::Notifier.new 'https://hooks.slack.com/services/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' do
      defaults username: 'echidna'
    end
    text = "Following deployment has failed \n\n Branch:  #{branch} \n\n Enviroment: #{env}"
    res = notifier.post text: text, icon_url: 'https://img.purch.com/w/660/aHR0cDovL3d3dy5saXZlc2NpZW5jZS5jb20vaW1hZ2VzL2kvMDAwLzA4OC80Mjkvb3JpZ2luYWwvZWNoaWRuYS5qcGc='
  end
end

namespace :bower do
  desc 'Install bower'
  task :install do
    on roles(:web) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, 'bower:install CI=true'
        end
      end
    end
  end
end
before 'deploy:compile_assets', 'bower:install'
# after 'deploy:publishing', 'deploy:restart'

set :yarn_target_path, -> { release_path.join('client') } #
set :yarn_flags, '--production --silent --no-progress'    # default
set :yarn_roles, :all                                     # default
set :yarn_env_variables, {}

namespace :webpacker do
  task :install do
    on roles(:web) do
      within release_path.join('client') do
        with rails_env: fetch(:rails_env) do
          # rake 'react_on_rails:locale'
          execute :yarn, :run, 'build:production'
        end
      end
    end
  end
end

after 'npm:install', 'webpacker:install'