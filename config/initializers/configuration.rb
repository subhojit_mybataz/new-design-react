# frozen_string_literal: true

CONFIG = YAML.safe_load(ERB.new(File.read("#{Rails.root}/config/configuration.yml.erb")).result).with_indifferent_access
