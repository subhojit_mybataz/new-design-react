# frozen_string_literal: true

Rails.application.routes.draw do

  ActiveAdmin.routes(self)
  root to: 'hello_world#index'
  get 'social_preview/index'
  get 'profile/:id', to: 'hello_world#profile'
  get 'notifications', to: 'hello_world#notification'
  get 'follownotifications', to: 'hello_world#follow_notification'
  get 'posts/:id', to: 'hello_world#post_details'
  get 'looks/:id', to: 'hello_world#post_details'
  get 'search/:term', to: 'hello_world#search'
  get 'socialpreview/post/:id', to: 'social_preview#index'
  get 'socialpreview/brandcommunity/:countryname/:brandname', to: 'social_preview#brand_preview'

  use_doorkeeper
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations',
    omniauth_callbacks: 'users/omniauth_callbacks'
  }

  devise_scope :user do
    get 'users/bataz_icon_sign_up' => 'users/registrations#bataz_icon_sign_up'
  end

  get 'static_pages/home'

  namespace :api do
    namespace :v1 do

      ###### add non-rest routes for resources here for milestone 1
      put 'notifications/read' => 'notifications#read'
      #############################################################

      post 'user/registration' => 'users#register'
      post 'user/social_register' => 'users#social_register'
      post 'user/authentication' => 'users#authentication'
      put 'user/change_password' => 'users#change_password'
      delete 'user/signout' => 'users#signout'
      post 'posts/toggle_like' => 'posts#toggleLike'
      get 'user/auth_code_callback' => 'users#confirm_auth_callback'
      post 'user/send_email' => 'users#sendEmail'
      get 'user/getFollowers' => 'users#getFollowers'
      get 'user/get_profile' => 'users#getProfile'
      post 'user/follow_user' => 'users#follow_user'
      put 'user/update_profile_pic' => 'users#update_profile_pic'
      get 'posts/brandfeed' => 'posts#brand_feed'
      get 'posts/userfeed' => 'posts#user_feed'
      get 'posts/homefeed' => 'posts#feed'
      get 'posts/postdetails' => 'posts#get_post_details'
      get 'posts/notificationdetails' => 'posts#get_notification_details'
      get 'user/follownotificationdetails' => 'users#get_follow_notification_details'
      get 'posts/newpost' => 'posts#new_post'
      get 'posts/notificatoncount' => 'posts#notification_count'
      post 'posts/new_post_create' => 'posts#new_post_create'
      post 'posts/add_comment' => 'posts#addComment'
      get 'brand/get_brand' => 'brands#getBrand'
      post 'brand/follow_brand' => 'brands#followBrand'
      post 'user/new_profile_pic' => 'users#new_profile_pic'
      get 'brand/get_all_brand_products' => 'brands#getAllBrandProducts'
      post 'user/send_push_notification' => 'users#send_push_notification'
      get 'user/getCountry' => 'users#get_country'
      get 'post/search' => 'posts#search_api'
      post 'user/social_auth' => 'users#social_authentication'
      post 'post/delete_post' => 'posts#delete_post'
      post 'post/delete_comment' => 'posts#delete_comment'
      get 'post/mark_all_notification_read' => 'posts#mark_all_notification_read'
      post 'user/sendAppVersion' => 'users#store_app_version'
      post 'user/sendPnId' => 'users#store_firebase_push_notif_id'
      post 'user/getProfilePic' => 'users#getProfilePic'
      post 'user/sendInvite' => 'users#send_invite'
      post 'user/getRightSidebarElements' => 'users#getRightSidebarElements'
      get 'user/referer_link' => 'users#referer_link'
      post 'user/send_referer_link' => 'users#send_referer_link'

      resources :posts, only: %i[index create show update destroy]
      resources :looks, only: %i[index create show update destroy]
      resources :users, only: %i[index update show]
      resources :user_types, only: %i[index update create show]
      resources :preferences, only: %i[index update create show]
      resources :abuse_types, only: [:index]
      resources :report_abuses, only: [:create]
      resources :reset_passwords, only: [:index]
      resources :brands, only: %i[index show]
      resources :products, only: %i[index]
      resources :notifications, only: %i[index show]
      resources :emoticons, only: %i[index show]
    end
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'privacy' => 'static_pages#privacy'
  get 'terms' => 'static_pages#terms'
  get ':country/:brand' => 'hello_world#brand'
end
