# frozen_string_literal: true

class AddTypeToPosts < ActiveRecord::Migration[5.0]
  def change
    add_column :posts, :type, :string, null: false, default: 'Post'
    rename_table :posts, :stories
  end
end
