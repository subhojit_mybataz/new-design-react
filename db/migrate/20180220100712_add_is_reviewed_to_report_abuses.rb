# frozen_string_literal: true

class AddIsReviewedToReportAbuses < ActiveRecord::Migration[5.0]
  def change
    add_column :report_abuses, :is_reviewed, :boolean, default: false
  end
end
