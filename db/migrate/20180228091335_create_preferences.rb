# frozen_string_literal: true

class CreatePreferences < ActiveRecord::Migration[5.0]
  def change
    create_table :preferences do |t|
      t.integer :users_count, default: 0, null: false
      t.string :preference, null: false
      t.timestamps
    end
    Preference.create(preference: 'shirt')
    Preference.create(preference: 'suit')
  end
end
