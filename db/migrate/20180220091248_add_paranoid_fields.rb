# frozen_string_literal: true

class AddParanoidFields < ActiveRecord::Migration[5.0]
  def change
    add_column :posts, :deleted_at, :time
    add_column :users, :deleted_at, :time
  end
end
