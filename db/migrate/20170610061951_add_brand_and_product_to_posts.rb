# frozen_string_literal: true

class AddBrandAndProductToPosts < ActiveRecord::Migration[5.0]
  def change
    add_reference :posts, :brand, foreign_key: true
    add_reference :posts, :product, foreign_key: true
  end
end
