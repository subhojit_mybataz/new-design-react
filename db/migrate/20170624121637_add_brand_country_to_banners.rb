# frozen_string_literal: true

class AddBrandCountryToBanners < ActiveRecord::Migration[5.0]
  def change
    add_reference :banners, :brand_country, foreign_key: true, index: true
  end
end
