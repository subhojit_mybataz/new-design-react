# frozen_string_literal: true

class CreateBrandCountries < ActiveRecord::Migration[5.0]
  def change
    create_table :brand_countries, &:timestamps
  end
end
