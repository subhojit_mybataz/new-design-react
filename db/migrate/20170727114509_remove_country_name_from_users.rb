# frozen_string_literal: true

class RemoveCountryNameFromUsers < ActiveRecord::Migration[5.0]
  def change
    remove_column :users, :country_name
  end
end
