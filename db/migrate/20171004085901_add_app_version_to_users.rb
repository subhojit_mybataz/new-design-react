# frozen_string_literal: true

class AddAppVersionToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :app_version, :string
  end
end
