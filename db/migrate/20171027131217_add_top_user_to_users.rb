# frozen_string_literal: true

class AddTopUserToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :top_user, :boolean, default: false
  end
end
