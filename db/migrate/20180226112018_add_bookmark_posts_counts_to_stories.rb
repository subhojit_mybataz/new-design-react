# frozen_string_literal: true

class AddBookmarkPostsCountsToStories < ActiveRecord::Migration[5.0]
  def change
    add_column :stories, :bookmark_posts_count, :integer, default: 0
  end
end
