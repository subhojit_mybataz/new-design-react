# frozen_string_literal: true

class CreateBookmarkPosts < ActiveRecord::Migration[5.0]
  def change
    create_table :bookmark_posts do |t|
      t.integer :post_id
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :bookmark_posts, %i[post_id user_id], unique: true
  end
end
