# frozen_string_literal: true

class ChangeDefaultTotalLikesToBrands < ActiveRecord::Migration[5.0]
  def change
    add_column :brands, :claps_count, :integer, null: false, default: 0
    add_column :brands, :comments_count, :integer, null: false, default: 0
  end
end
