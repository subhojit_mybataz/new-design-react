# frozen_string_literal: true

class CreateAttachments < ActiveRecord::Migration[5.0]
  def change
    create_table :attachments do |t|
      t.integer :attachable_id
      t.string :attachable_type
      t.datetime :created_at
    end
    add_index :attachments, %w[attachable_id attachable_type], name: 'fk_attachment'
  end
end
