class AddFieldsToBrands < ActiveRecord::Migration[5.0]
  def change
    add_column :brands, :preferred_for, :string, default: 'all'
    add_column :brands, :priority, :integer, default: 1
  end
end
