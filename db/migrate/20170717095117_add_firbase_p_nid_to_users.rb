# frozen_string_literal: true

class AddFirbasePNidToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :firebasePNId, :string
  end
end
