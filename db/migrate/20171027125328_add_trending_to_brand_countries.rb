# frozen_string_literal: true

class AddTrendingToBrandCountries < ActiveRecord::Migration[5.0]
  def change
    add_column :brand_countries, :trending, :boolean, default: false
  end
end
