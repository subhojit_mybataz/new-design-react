# frozen_string_literal: true

class AddFieldsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :username, :string
    add_column :users, :phone, :string
    add_column :users, :bio, :text
  end
end
