class AddClapsCountToStories < ActiveRecord::Migration[5.0]
  def change
  	add_column :stories, :claps_count, :integer, default: nil, default: 0
  end
end
