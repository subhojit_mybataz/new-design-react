# frozen_string_literal: true

class AddCountryIdToUsers < ActiveRecord::Migration[5.0]
  def change
    add_reference :users, :country, index: true
    add_foreign_key :users, :countries
  end
end
