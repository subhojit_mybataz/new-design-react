# frozen_string_literal: true

class AddCountryNameToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :country_name, :string
  end
end
