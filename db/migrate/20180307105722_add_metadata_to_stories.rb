class AddMetadataToStories < ActiveRecord::Migration[5.0]
  def change
    add_column :stories, :metadata, :jsonb, default: {}, null: false
  end
end
