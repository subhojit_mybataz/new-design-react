class AddDateDiffFunction < ActiveRecord::Migration[5.0]
  def up
    ActiveRecord::Base.connection.execute <<-SQL
      CREATE OR REPLACE FUNCTION date_diff(range NUMERIC, created_at TIMESTAMP) 
      RETURNS NUMERIC AS $$
      DECLARE temp_diff NUMERIC;
      DECLARE diff NUMERIC;
      BEGIN
       temp_diff := date_part('day', now() - created_at);
       diff := temp_diff + (temp_diff%range);
       RETURN diff;
      END; $$
      LANGUAGE plpgsql;

      CREATE OR REPLACE FUNCTION date_diff(range NUMERIC, created_at TIMESTAMP WITH TIME ZONE) 
      RETURNS NUMERIC AS $$
      DECLARE temp_diff NUMERIC;
      DECLARE diff NUMERIC;
      BEGIN
      temp_diff := date_part('day', now() - created_at);
      diff := temp_diff + (temp_diff%range);
      RETURN diff;
      END; $$
      LANGUAGE plpgsql;
    SQL
    say "date_diff database function created."
  end

  def down
    ActiveRecord::Base.connection.execute <<-SQL
      DROP FUNCTION date_diff(numeric, timestamp without time zone);
      DROP FUNCTION date_diff(numeric, timestamp with time zone);
    SQL
    say "date_diff database function removed."
  end
end
