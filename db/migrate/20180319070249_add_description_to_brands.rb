class AddDescriptionToBrands < ActiveRecord::Migration[5.0]
  def change
    add_column :brands, :description, :string
  end
end
