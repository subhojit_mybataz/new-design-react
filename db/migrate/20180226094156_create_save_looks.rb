# frozen_string_literal: true

class CreateSaveLooks < ActiveRecord::Migration[5.0]
  def change
    create_table :save_looks do |t|
      t.references :user, foreign_key: true
      t.integer :look_id

      t.timestamps
    end
    add_index :save_looks, %i[look_id user_id], unique: true
  end
end
