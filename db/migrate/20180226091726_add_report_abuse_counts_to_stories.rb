# frozen_string_literal: true

class AddReportAbuseCountsToStories < ActiveRecord::Migration[5.0]
  def change
    add_column :stories, :report_abuses_count, :integer, default: 0, null: false
    add_column :users, :report_abuses_count, :integer, default: 0, null: false
  end
end
