# frozen_string_literal: true

class CreateTags < ActiveRecord::Migration[5.0]
  def change
    create_table :tags do |t|
      t.string :name
      t.integer :posts_count, null: false, default: 0
      t.integer :looks_count, null: false, default: 0
      t.integer :brands_count, null: false, default: 0
      t.references :user
      t.time :deleted_at, default: nil
      t.timestamps
    end

    create_table :story_tags do |t|
      t.string :name
      t.string :tagable_type, null: false
      t.integer :tagable_id, null: false
      t.time :deleted_at, default: nil
      t.references :tag
    end
    add_index :story_tags, %w[tagable_type tagable_id], name: 'fk_tagable'
  end
end
