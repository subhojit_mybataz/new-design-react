# frozen_string_literal: true

class AddRandomAuthApiTokenToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :random_auth_token, :string
  end
end
