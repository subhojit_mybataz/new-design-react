# frozen_string_literal: true

class UpdateAccessAndRefershTokenLength < ActiveRecord::Migration[5.0]
  def up
    change_column :identities, :accesstoken, :string, limit: 800
    change_column :identities, :refreshtoken, :string, limit: 800
  end

  def down
    change_column :identities, :accesstoken, :string
    change_column :identities, :refreshtoken, :string
  end
end
