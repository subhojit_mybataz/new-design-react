# frozen_string_literal: true

class AddActionToReportAbuseActions < ActiveRecord::Migration[5.0]
  def change
    add_column :report_abuse_actions, :action, :string
  end
end
