# frozen_string_literal: true

class AddGenderCityCountyToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :gender, :string
    add_column :users, :country, :string
    add_column :users, :city, :string
  end
end
