# frozen_string_literal: true

class AddColumnToBrandCountry < ActiveRecord::Migration[5.0]
  def change
    add_reference :brand_countries, :brand, index: true
    add_reference :brand_countries, :country, index: true
  end
end
