# frozen_string_literal: true

class UpdateDeletedAtForStories < ActiveRecord::Migration[5.0]
  def change
    change_column :stories, :deleted_at, :time, default: nil
  end
end
