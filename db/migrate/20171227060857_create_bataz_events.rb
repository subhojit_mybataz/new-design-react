# frozen_string_literal: true

class CreateBatazEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :bataz_events do |t|
      t.string :name
      t.string :link

      t.timestamps
    end
  end
end
