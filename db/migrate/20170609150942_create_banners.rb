# frozen_string_literal: true

class CreateBanners < ActiveRecord::Migration[5.0]
  def change
    create_table :banners do |t|
      t.string :img_url
      t.references :brand, foreign_key: true
      t.boolean :active

      t.timestamps
    end
  end
end
