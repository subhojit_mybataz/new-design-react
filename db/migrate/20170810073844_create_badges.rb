# frozen_string_literal: true

class CreateBadges < ActiveRecord::Migration[5.0]
  def change
    create_table :badges do |t|
      t.references :brand, foreign_key: true
      t.references :user, foreign_key: true
      t.string :badge_type

      t.timestamps
    end
  end
end
