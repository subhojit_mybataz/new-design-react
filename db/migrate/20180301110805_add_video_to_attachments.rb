# frozen_string_literal: true

class AddVideoToAttachments < ActiveRecord::Migration[5.0]
  def self.up
    add_attachment :attachments, :image
    add_attachment :attachments, :video
    remove_attachment :attachments, :media
    add_column :attachments, :file_type, :string, null: false, default: 'image'
  end

  def self.down
    remove_attachment :attachments, :image
    remove_attachment :attachments, :video
    add_attachment :attachments, :media
    remove_column :attachments, :file_type
  end
end
