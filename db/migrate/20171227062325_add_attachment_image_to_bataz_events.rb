# frozen_string_literal: true

class AddAttachmentImageToBatazEvents < ActiveRecord::Migration
  def self.up
    change_table :bataz_events do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :bataz_events, :image
  end
end
