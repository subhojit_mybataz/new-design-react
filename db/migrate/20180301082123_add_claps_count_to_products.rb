# frozen_string_literal: true

class AddClapsCountToProducts < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :claps_count, :integer, null: false, default: 0
    add_column :products, :comments_count, :integer, null: false, default: 0
  end
end
