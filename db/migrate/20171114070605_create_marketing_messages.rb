# frozen_string_literal: true

class CreateMarketingMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :marketing_messages do |t|
      t.text :message

      t.timestamps
    end
  end
end
