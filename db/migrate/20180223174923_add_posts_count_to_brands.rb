# frozen_string_literal: true

class AddPostsCountToBrands < ActiveRecord::Migration[5.0]
  def change
    add_column :brands, :posts_count, :integer, null: false, default: 0
  end
end
