class AddReportAbusesCountToComments < ActiveRecord::Migration[5.0]
  def change
    add_column :comments, :report_abuses_count, :integer, default: 0
  end
end
