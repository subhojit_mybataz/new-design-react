class AddDeafultPriceValue < ActiveRecord::Migration[5.0]
  def change
    change_column :stories, :price, :float, default: 0.0
  end
end
