# frozen_string_literal: true

class CreatePendingInvitees < ActiveRecord::Migration[5.0]
  def change
    create_table :pending_invitees do |t|
      t.string :ShoptazId
      t.string :InitiatorId
      t.string :emailId

      t.timestamps
    end
  end
end
