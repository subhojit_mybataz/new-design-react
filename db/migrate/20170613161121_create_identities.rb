# frozen_string_literal: true

class CreateIdentities < ActiveRecord::Migration[5.0]
  def change
    create_table :identities do |t|
      t.references :user, index: true
      t.string :provider
      t.string :accesstoken
      t.string :refreshtoken
      t.string :uid
      t.string :name
      t.string :email

      t.timestamps null: false
    end
    add_foreign_key :identities, :users
  end
end
