# frozen_string_literal: true

class AddAbuseTypeForToAbuseTypes < ActiveRecord::Migration[5.0]
  def change
    add_column :abuse_types, :abuse_type_for, :string
  end
end
