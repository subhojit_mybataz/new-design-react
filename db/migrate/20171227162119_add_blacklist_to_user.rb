# frozen_string_literal: true

class AddBlacklistToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :blacklisted, :boolean, default: false
  end
end
