# frozen_string_literal: true

class AddlikespostcountToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :total_likes, :integer, default: 0
    add_column :users, :total_posts, :integer, default: 0
  end
end
