# frozen_string_literal: true

class CreateAbuseTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :abuse_types do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
