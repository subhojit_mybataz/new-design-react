# frozen_string_literal: true

class AddBrandRanksToBrandCountires < ActiveRecord::Migration[5.0]
  def change
    add_column :brand_countries, :brand_rank, :integer
  end
end
