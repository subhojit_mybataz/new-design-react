class AddLooksCountToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :posts_count, :integer, null: false, default: 0
    rename_column :users, :total_posts, :looks_count
    remove_column :users, :is_active
  end
end
