# frozen_string_literal: true

class CreateUserTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :user_types do |t|
      t.string :user_type, null: false, default: 'user'
      t.integer :users_count, null: false, default: 0
      t.timestamps
    end

    UserType.create(user_type: 'fashion_designer')
    UserType.create(user_type: 'photographer')
  end
end
