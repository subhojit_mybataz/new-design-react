# frozen_string_literal: true

class CreatePointRules < ActiveRecord::Migration[5.0]
  def change
    create_table :point_rules do |t|
      t.string :action_type
      t.integer :points
    end
  end
end
