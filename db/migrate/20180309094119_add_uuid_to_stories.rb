class AddUuidToStories < ActiveRecord::Migration[5.0]
  def change
  	enable_extension 'uuid-ossp'
    add_column :stories, :uuid, :uuid, null: false, default: 'uuid_generate_v4()'

    add_index :stories, %w[uuid], name: 'uuid_index_on_stories'
  end
end
