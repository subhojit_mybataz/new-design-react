# frozen_string_literal: true

class AddPointRuleIdToPoints < ActiveRecord::Migration[5.0]
  def change
    add_column :points, :item_id, :integer
    add_column :points, :item_type, :string
    add_column :points, :points, :integer
    add_column :points, :point_rule_id, :integer
    add_column :points, :action_by, :string
  end
end
