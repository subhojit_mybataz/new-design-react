# frozen_string_literal: true

class AddDeletedAtToComments < ActiveRecord::Migration[5.0]
  def change
    add_column :comments, :deleted_at, :time, default: nil
    add_column :claps, :deleted_at, :time, default: nil
    add_column :story_brands, :deleted_at, :time, default: nil
    add_column :story_products, :deleted_at, :time, default: nil
    add_column :attachments, :deleted_at, :time, default: nil
    add_column :report_abuses, :deleted_at, :time, default: nil
    add_column :save_looks, :deleted_at, :time, default: nil
    add_column :bookmark_posts, :deleted_at, :time, default: nil
  end
end
