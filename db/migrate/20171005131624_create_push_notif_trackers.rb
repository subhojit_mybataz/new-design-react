# frozen_string_literal: true

class CreatePushNotifTrackers < ActiveRecord::Migration[5.0]
  def change
    create_table :push_notif_trackers do |t|
      t.references :user, index: true, foreign_key: true
      t.datetime :created_at, null: false
    end
  end
end
