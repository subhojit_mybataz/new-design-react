# frozen_string_literal: true

class AddFirebaseAuthTokenToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :firebase_auth_token, :text
  end
end
