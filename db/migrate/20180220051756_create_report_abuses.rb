# frozen_string_literal: true

class CreateReportAbuses < ActiveRecord::Migration[5.0]
  def change
    create_table :report_abuses do |t|
      t.references :reportable, polymorphic: true
      t.references :user, foreign_key: true
      t.references :abuse_type, foreign_key: true

      t.timestamps
    end
  end
end
