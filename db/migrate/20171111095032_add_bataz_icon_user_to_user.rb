# frozen_string_literal: true

class AddBatazIconUserToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :bataz_icon_user, :boolean, default: false
  end
end
