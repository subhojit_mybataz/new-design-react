# frozen_string_literal: true

class CreatePostBrands < ActiveRecord::Migration[5.0]
  def change
    create_table :post_brands do |t|
      t.integer :post_id
      t.integer :brand_id
      t.datetime :created_at
    end
  end
end
