# frozen_string_literal: true

class RemoveBrandIdFromBanners < ActiveRecord::Migration[5.0]
  def change
    remove_reference(:banners, :brand, index: true, foreign_key: true)
  end
end
