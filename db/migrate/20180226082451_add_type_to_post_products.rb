# frozen_string_literal: true

class AddTypeToPostProducts < ActiveRecord::Migration[5.0]
  def change
    add_column :post_products, :productable_type, :string, default: 'Post', null: false
    rename_table :post_products, :story_products
    rename_column :story_products, :post_id, :productable_id
    add_index :story_products, %w[productable_id productable_type], name: 'fk_productable_id_type'
  end
end
