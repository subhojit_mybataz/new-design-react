# frozen_string_literal: true

class AddForeignKeyToBrandCountry < ActiveRecord::Migration[5.0]
  def change
    add_foreign_key :brand_countries, :brands, column: :brand_id
    add_foreign_key :brand_countries, :countries, column: :country_id
  end
end
