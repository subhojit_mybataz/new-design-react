# frozen_string_literal: true

class AddMetadataToNotifications < ActiveRecord::Migration[5.0]
  def change
    add_column :notifications, :metadata, :jsonb
  end
end
