# frozen_string_literal: true

class AddTypeToPostBrands < ActiveRecord::Migration[5.0]
  def change
    add_column :post_brands, :brandable_type, :string, default: 'Post'
    rename_table :post_brands, :story_brands
    rename_column :story_brands, :post_id, :brandable_id
    add_column :stories, :brands_count, :integer, default: 0
    add_column :stories, :products_count, :integer, default: 0
    add_index :story_brands, %w[brandable_id brandable_type], name: 'fk_brandable_id_type'
  end
end
