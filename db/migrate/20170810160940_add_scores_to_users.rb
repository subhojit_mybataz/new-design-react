# frozen_string_literal: true

class AddScoresToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :brand_score, :integer, default: 0
    add_column :users, :lifestyle_score, :integer, default: 0
  end
end
