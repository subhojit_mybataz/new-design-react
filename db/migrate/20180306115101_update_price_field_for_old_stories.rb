class UpdatePriceFieldForOldStories < ActiveRecord::Migration[5.0]
  def change
    Story.with_deleted.where(price: nil).update_all(price: 0.0)
  end
end
