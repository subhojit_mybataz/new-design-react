# frozen_string_literal: true

class CreateClaps < ActiveRecord::Migration[5.0]
  def change
    create_table :claps do |t|
      t.references :user, foreign_key: true
      t.integer :clapable_id, null: false
      t.string :clapable_type, null: false
      t.integer :count, null: false, default: 1
      t.timestamps
    end
    add_index :claps, %w[clapable_id clapable_type], name: 'fk_clap'
  end
end
