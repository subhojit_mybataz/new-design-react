class CreateEmoticons < ActiveRecord::Migration[5.0]
  def change
    create_table :emoticons do |t|
      t.string :name, null: false
      t.string :url_logo, null: false
      t.string :url, null: false
      t.integer :posts_count, null: false, default: 0
      t.integer :looks_count, null: false, default: 0
      t.datetime :deleted_at, default: nil
      t.timestamps
    end
  end
end
