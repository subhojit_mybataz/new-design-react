# frozen_string_literal: true

class AddTotalLikesAndTotalCommentsToBrands < ActiveRecord::Migration[5.0]
  def up
    add_column :brands, :total_likes, :integer
    add_column :brands, :total_comments, :integer

    Brand.find_each do |brand|
      brand.total_likes = Post.where(brand_id: brand.id).sum(:likers_count)
      brand.total_comments = Post.where(brand_id: brand.id).sum(:comments_count)
      brand.save!
    end
  end

  def down
    remove_column :brands, :total_likes
    remove_column :brands, :total_comments
  end
end
