# frozen_string_literal: true

class CreateNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :notifications do |t|
      t.string :trackable_type
      t.integer :trackable_id
      t.string :owner_type
      t.integer :owner_id
      t.string :activity_type
      t.string :recipient_type
      t.integer :recipient_id
      t.boolean :read

      t.timestamps
    end
  end
end
