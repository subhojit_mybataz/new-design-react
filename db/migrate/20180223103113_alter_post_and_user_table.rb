class AlterPostAndUserTable < ActiveRecord::Migration[5.0]
  def up
    change_column_default(:posts, :deleted, false)
  end

  def down
    change_column_default(:posts, :deleted, nil)
  end
end
