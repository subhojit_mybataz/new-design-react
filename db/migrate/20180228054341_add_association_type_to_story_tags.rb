# frozen_string_literal: true

class AddAssociationTypeToStoryTags < ActiveRecord::Migration[5.0]
  def change
    add_column :story_tags, :association_type, :string, null: false, default: 'Post'
    add_column :story_brands, :association_type, :string, null: false, default: 'Post'
    add_column :story_products, :association_type, :string, null: false, default: 'Post'
    add_column :comments, :association_type, :string, null: false, default: 'Post'
    add_column :brands, :looks_count, :integer, null: false, default: 0
    add_column :products, :posts_count, :integer, null: false, default: 0
    add_column :products, :looks_count, :integer, null: false, default: 0
  end
end
