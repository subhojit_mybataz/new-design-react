# frozen_string_literal: true

class AddIsActiveToPosts < ActiveRecord::Migration[5.0]
  def change
    add_column :posts, :is_active, :boolean, default: true
  end
end
