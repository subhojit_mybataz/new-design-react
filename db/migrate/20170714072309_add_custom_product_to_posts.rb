# frozen_string_literal: true

class AddCustomProductToPosts < ActiveRecord::Migration[5.0]
  def change
    add_column :posts, :custom_product, :string
  end
end
