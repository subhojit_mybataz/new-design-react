# frozen_string_literal: true

class CreatePostProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :post_products do |t|
      t.integer :post_id
      t.integer :product_id
      t.datetime :created_at
    end
  end
end
