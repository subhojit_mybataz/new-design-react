# frozen_string_literal: true

class AddCommentsToPosts < ActiveRecord::Migration[5.0]
  def change
    add_column :posts, :comments_count, :integer, default: 0
  end
end
