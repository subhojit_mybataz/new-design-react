class AddAvenueToStory < ActiveRecord::Migration[5.0]
  def change
    add_column :stories, :avenue_to_buy, :text, default: nil
    add_column :stories, :detail, :text, default: nil
  end
end
