# frozen_string_literal: true

class UpdateBrandsFields < ActiveRecord::Migration[5.0]
  def change
    add_column :brands, :deleted_at, :time, default: nil
    Brand.where('total_likes is NOT NULL').find_in_batches(batch_size: 100).each do |brands|
      brands.each do |brand|
        brand.update(claps_count: brand.total_likes)
      end
    end
    Brand.where('total_comments is NOT NULL').find_in_batches(batch_size: 100).each do |brands|
      brands.each do |brand|
        brand.update(comments_count: brand.total_comments)
      end
    end
    remove_column :brands, :total_likes
    remove_column :brands, :total_comments
  end
end
