class AddFieldsToProductss < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :preferred_for, :string, default: 'all'
    add_column :products, :priority, :integer, default: 1
  end
end
