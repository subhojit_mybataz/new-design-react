# frozen_string_literal: true

class CreatePoints < ActiveRecord::Migration[5.0]
  def change
    create_table :points do |t|
      t.references :brand, foreign_key: true
      t.references :user, foreign_key: true
      t.integer :num
      t.string :custom_brand

      t.timestamps
    end
  end
end
