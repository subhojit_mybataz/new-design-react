# frozen_string_literal: true

class AddCustomBrandToPosts < ActiveRecord::Migration[5.0]
  def change
    add_column :posts, :custom_brand, :string
  end
end
