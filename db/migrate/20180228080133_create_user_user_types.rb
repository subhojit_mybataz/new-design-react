# frozen_string_literal: true

class CreateUserUserTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :user_user_types do |t|
      t.references :user
      t.references :user_type
      t.timestamps
    end
  end
end
