# frozen_string_literal: true

class UpdateBannersFields < ActiveRecord::Migration[5.0]
  def change
    add_column :banners, :bannerable_id, :integer
    add_column :banners, :bannerable_type, :string
    add_column :banners, :deleted_at, :time, default: nil
  end
end
