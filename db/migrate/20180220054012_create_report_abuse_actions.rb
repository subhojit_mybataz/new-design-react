# frozen_string_literal: true

class CreateReportAbuseActions < ActiveRecord::Migration[5.0]
  def change
    create_table :report_abuse_actions do |t|
      t.references :report_abuse, foreign_key: true

      t.timestamps
    end
  end
end
