class CreateStoryEmoticons < ActiveRecord::Migration[5.0]
  def change
    create_table :story_emoticons do |t|
      t.references :story, foreign_key: true
     	t.references :emoticon, foreign_key: true
     	t.datetime :deleted_at,  default: nil
     	t.references :user, foreign_key: true
     	t.integer :count, default: 1, null: false
      t.timestamps
    end
  end
end
