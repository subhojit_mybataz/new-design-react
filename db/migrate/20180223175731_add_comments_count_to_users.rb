# frozen_string_literal: true

class AddCommentsCountToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :comments_count, :integer
  end
end
