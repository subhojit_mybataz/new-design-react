# frozen_string_literal: true

class AddRefererCodeToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :referer_code, :string
  end
end
