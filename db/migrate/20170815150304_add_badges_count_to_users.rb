# frozen_string_literal: true

class AddBadgesCountToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :total_guru_badges, :integer, default: 0
    add_column :users, :total_ambassador_badges, :integer, default: 0
  end
end
