# frozen_string_literal: true

class AddAttachmentToAttachments < ActiveRecord::Migration[5.0]
  def up
    add_attachment :attachments, :media
  end

  def down
    remove_attachment :attachments, :media
  end
end
