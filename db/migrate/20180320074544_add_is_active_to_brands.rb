class AddIsActiveToBrands < ActiveRecord::Migration[5.0]
  def change
    add_column :brands, :is_active, :boolean, default: true
  end
end
