# frozen_string_literal: true

class AddSaveLooksCountsToStories < ActiveRecord::Migration[5.0]
  def change
    add_column :stories, :save_looks_count, :integer, default: 0
  end
end
