# frozen_string_literal: true

class AddBadgesCountToBrand < ActiveRecord::Migration[5.0]
  def change
    add_column :brands, :total_guru_badges, :integer, default: 0
    add_column :brands, :total_ambassador_badges, :integer, default: 0
  end
end
