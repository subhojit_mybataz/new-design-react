# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180330105830) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "abuse_types", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "abuse_type_for"
  end

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace",     limit: 510
    t.text     "body"
    t.string   "resource_type", limit: 510
    t.integer  "resource_id"
    t.string   "author_type",   limit: 510
    t.integer  "author_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "ahoy_events", force: :cascade do |t|
    t.integer  "visit_id"
    t.integer  "user_id"
    t.string   "name",       limit: 510
    t.text     "properties"
    t.datetime "time"
  end

  create_table "attachments", force: :cascade do |t|
    t.integer  "attachable_id"
    t.string   "attachable_type"
    t.datetime "created_at"
    t.time     "deleted_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "video_file_name"
    t.string   "video_content_type"
    t.integer  "video_file_size"
    t.datetime "video_updated_at"
    t.string   "file_type",          default: "image", null: false
    t.index ["attachable_id", "attachable_type"], name: "fk_attachment", using: :btree
  end

  create_table "badges", force: :cascade do |t|
    t.integer  "brand_id"
    t.integer  "user_id"
    t.string   "badge_type", limit: 510
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["brand_id"], name: "badges_brand_id_idx", using: :btree
    t.index ["user_id"], name: "badges_user_id_idx", using: :btree
  end

  create_table "banners", force: :cascade do |t|
    t.string   "img_url",            limit: 510
    t.boolean  "active"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "brand_country_id"
    t.string   "image_file_name",    limit: 510
    t.string   "image_content_type", limit: 510
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "bannerable_id"
    t.string   "bannerable_type"
    t.time     "deleted_at"
    t.index ["brand_country_id"], name: "banners_brand_country_id_idx", using: :btree
  end

  create_table "bataz_events", force: :cascade do |t|
    t.string   "name",               limit: 510
    t.string   "link",               limit: 510
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "image_file_name",    limit: 510
    t.string   "image_content_type", limit: 510
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "bookmark_posts", force: :cascade do |t|
    t.integer  "post_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.time     "deleted_at"
    t.index ["post_id", "user_id"], name: "index_bookmark_posts_on_post_id_and_user_id", unique: true, using: :btree
    t.index ["user_id"], name: "index_bookmark_posts_on_user_id", using: :btree
  end

  create_table "brand_countries", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "brand_id"
    t.integer  "country_id"
    t.boolean  "trending"
    t.integer  "brand_rank"
    t.index ["brand_id"], name: "brand_countries_brand_id_idx", using: :btree
    t.index ["country_id"], name: "brand_countries_country_id_idx", using: :btree
  end

  create_table "brands", force: :cascade do |t|
    t.string   "name",                    limit: 510
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.string   "slug",                    limit: 510
    t.string   "logo_file_name",          limit: 510
    t.string   "logo_content_type",       limit: 510
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.integer  "total_guru_badges",                   default: 0
    t.integer  "total_ambassador_badges",             default: 0
    t.integer  "posts_count",                         default: 0,     null: false
    t.integer  "looks_count",                         default: 0,     null: false
    t.integer  "claps_count",                         default: 0,     null: false
    t.integer  "comments_count",                      default: 0,     null: false
    t.time     "deleted_at"
    t.string   "description"
    t.boolean  "is_active",                           default: true
    t.string   "preferred_for",                       default: "all"
    t.integer  "priority",                            default: 1
    t.index ["slug"], name: "brands_slug_key", unique: true, using: :btree
  end

  create_table "claps", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "clapable_id",               null: false
    t.string   "clapable_type",             null: false
    t.integer  "count",         default: 1, null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.time     "deleted_at"
    t.index ["clapable_id", "clapable_type"], name: "fk_clap", using: :btree
    t.index ["user_id"], name: "index_claps_on_user_id", using: :btree
  end

  create_table "comments", force: :cascade do |t|
    t.string   "title",               limit: 100, default: ""
    t.text     "comment"
    t.string   "commentable_type",    limit: 510
    t.integer  "commentable_id"
    t.integer  "user_id"
    t.string   "role",                limit: 510, default: "comments"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.time     "deleted_at"
    t.string   "association_type",                default: "Post",     null: false
    t.integer  "report_abuses_count",             default: 0
  end

  create_table "countries", force: :cascade do |t|
    t.string   "name",       limit: 510
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "emoticons", force: :cascade do |t|
    t.string   "name",                    null: false
    t.string   "url_logo",                null: false
    t.string   "url",                     null: false
    t.integer  "posts_count", default: 0, null: false
    t.integer  "looks_count", default: 0, null: false
    t.datetime "deleted_at"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "follows", force: :cascade do |t|
    t.string   "follower_type",   limit: 510
    t.integer  "follower_id"
    t.string   "followable_type", limit: 510
    t.integer  "followable_id"
    t.datetime "created_at"
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",           limit: 510, null: false
    t.integer  "sluggable_id",               null: false
    t.string   "sluggable_type", limit: 100
    t.string   "scope",          limit: 510
    t.datetime "created_at"
    t.index ["slug"], name: "friendly_id_slugs_slug_key", unique: true, using: :btree
  end

  create_table "identities", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "provider",     limit: 510
    t.string   "accesstoken",  limit: 1600
    t.string   "refreshtoken", limit: 1600
    t.string   "uid",          limit: 510
    t.string   "name",         limit: 510
    t.string   "email",        limit: 510
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["user_id"], name: "identities_user_id_idx", using: :btree
  end

  create_table "likes", force: :cascade do |t|
    t.string   "liker_type",    limit: 510
    t.integer  "liker_id"
    t.string   "likeable_type", limit: 510
    t.integer  "likeable_id"
    t.datetime "created_at"
  end

  create_table "marketing_messages", force: :cascade do |t|
    t.text     "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notifications", force: :cascade do |t|
    t.string   "trackable_type", limit: 510
    t.integer  "trackable_id"
    t.string   "owner_type",     limit: 510
    t.integer  "owner_id"
    t.string   "activity_type",  limit: 510
    t.string   "recipient_type", limit: 510
    t.integer  "recipient_id"
    t.boolean  "read"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.jsonb    "metadata"
  end

  create_table "oauth_access_grants", force: :cascade do |t|
    t.integer  "resource_owner_id",             null: false
    t.integer  "application_id",                null: false
    t.string   "token",             limit: 510, null: false
    t.integer  "expires_in",                    null: false
    t.text     "redirect_uri",                  null: false
    t.datetime "created_at",                    null: false
    t.datetime "revoked_at"
    t.string   "scopes",            limit: 510
    t.index ["application_id"], name: "oauth_access_grants_application_id_idx", using: :btree
    t.index ["resource_owner_id"], name: "oauth_access_grants_resource_owner_id_idx", using: :btree
    t.index ["token"], name: "oauth_access_grants_token_key", unique: true, using: :btree
  end

  create_table "oauth_access_tokens", force: :cascade do |t|
    t.integer  "resource_owner_id"
    t.integer  "application_id"
    t.string   "token",                  limit: 510,              null: false
    t.string   "refresh_token",          limit: 510
    t.integer  "expires_in"
    t.datetime "revoked_at"
    t.datetime "created_at",                                      null: false
    t.string   "scopes",                 limit: 510
    t.string   "previous_refresh_token", limit: 510, default: "", null: false
    t.index ["application_id"], name: "oauth_access_tokens_application_id_idx", using: :btree
    t.index ["refresh_token"], name: "oauth_access_tokens_refresh_token_key", unique: true, using: :btree
    t.index ["resource_owner_id"], name: "oauth_access_tokens_resource_owner_id_idx", using: :btree
    t.index ["token"], name: "oauth_access_tokens_token_key", unique: true, using: :btree
  end

  create_table "oauth_applications", force: :cascade do |t|
    t.string   "name",         limit: 510,              null: false
    t.string   "uid",          limit: 510,              null: false
    t.string   "secret",       limit: 510,              null: false
    t.text     "redirect_uri",                          null: false
    t.string   "scopes",       limit: 510, default: "", null: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.index ["uid"], name: "oauth_applications_uid_key", unique: true, using: :btree
  end

  create_table "pending_invitees", force: :cascade do |t|
    t.string   "ShoptazId",   limit: 510
    t.string   "InitiatorId", limit: 510
    t.string   "emailId",     limit: 510
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "point_rules", force: :cascade do |t|
    t.string  "action_type"
    t.integer "points"
  end

  create_table "points", force: :cascade do |t|
    t.integer  "brand_id"
    t.integer  "user_id"
    t.integer  "num"
    t.string   "custom_brand",  limit: 510
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "item_id"
    t.string   "item_type"
    t.integer  "points"
    t.integer  "point_rule_id"
    t.string   "action_by"
    t.index ["brand_id"], name: "points_brand_id_idx", using: :btree
    t.index ["user_id"], name: "points_user_id_idx", using: :btree
  end

  create_table "preferences", force: :cascade do |t|
    t.integer  "users_count", default: 0, null: false
    t.string   "preference",              null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "products", force: :cascade do |t|
    t.string   "name",           limit: 510
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.integer  "posts_count",                default: 0,     null: false
    t.integer  "looks_count",                default: 0,     null: false
    t.integer  "claps_count",                default: 0,     null: false
    t.integer  "comments_count",             default: 0,     null: false
    t.string   "preferred_for",              default: "all"
    t.integer  "priority",                   default: 1
  end

  create_table "push_notif_trackers", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.index ["user_id"], name: "push_notif_trackers_user_id_idx", using: :btree
  end

  create_table "report_abuse_actions", force: :cascade do |t|
    t.integer  "report_abuse_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "action"
    t.index ["report_abuse_id"], name: "index_report_abuse_actions_on_report_abuse_id", using: :btree
  end

  create_table "report_abuses", force: :cascade do |t|
    t.string   "reportable_type"
    t.integer  "reportable_id"
    t.integer  "user_id"
    t.integer  "abuse_type_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.boolean  "is_reviewed",     default: false
    t.time     "deleted_at"
    t.index ["abuse_type_id"], name: "index_report_abuses_on_abuse_type_id", using: :btree
    t.index ["reportable_type", "reportable_id"], name: "index_report_abuses_on_reportable_type_and_reportable_id", using: :btree
    t.index ["user_id"], name: "index_report_abuses_on_user_id", using: :btree
  end

  create_table "save_looks", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "look_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.time     "deleted_at"
    t.index ["look_id", "user_id"], name: "index_save_looks_on_look_id_and_user_id", unique: true, using: :btree
    t.index ["user_id"], name: "index_save_looks_on_user_id", using: :btree
  end

  create_table "stories", force: :cascade do |t|
    t.string   "name",                 limit: 510
    t.text     "description"
    t.integer  "user_id"
    t.datetime "created_at",                                                             null: false
    t.datetime "updated_at",                                                             null: false
    t.integer  "likers_count",                     default: 0
    t.integer  "brand_id"
    t.integer  "product_id"
    t.integer  "comments_count",                   default: 0
    t.string   "image_file_name",      limit: 510
    t.string   "image_content_type",   limit: 510
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "custom_brand",         limit: 510
    t.string   "custom_product",       limit: 510
    t.float    "price",                            default: 0.0
    t.boolean  "deleted"
    t.time     "deleted_at"
    t.boolean  "is_active",                        default: true
    t.string   "type",                             default: "post",                      null: false
    t.integer  "brands_count",                     default: 0
    t.integer  "products_count",                   default: 0
    t.integer  "report_abuses_count",              default: 0,                           null: false
    t.integer  "save_looks_count",                 default: 0
    t.integer  "bookmark_posts_count",             default: 0
    t.integer  "claps_count",                      default: 0
    t.jsonb    "metadata",                         default: {},                          null: false
    t.uuid     "uuid",                             default: -> { "uuid_generate_v4()" }, null: false
    t.text     "avenue_to_buy"
    t.text     "detail"
    t.index ["brand_id"], name: "posts_brand_id_idx", using: :btree
    t.index ["product_id"], name: "posts_product_id_idx", using: :btree
    t.index ["user_id"], name: "posts_user_id_idx", using: :btree
    t.index ["uuid"], name: "uuid_index_on_stories", using: :btree
  end

  create_table "story_brands", force: :cascade do |t|
    t.integer  "brandable_id"
    t.integer  "brand_id"
    t.datetime "created_at"
    t.string   "brandable_type",   default: "Post"
    t.time     "deleted_at"
    t.string   "association_type", default: "Post", null: false
    t.index ["brandable_id", "brandable_type"], name: "fk_brandable_id_type", using: :btree
  end

  create_table "story_emoticons", force: :cascade do |t|
    t.integer  "story_id"
    t.integer  "emoticon_id"
    t.datetime "deleted_at"
    t.integer  "user_id"
    t.integer  "count",       default: 1, null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["emoticon_id"], name: "index_story_emoticons_on_emoticon_id", using: :btree
    t.index ["story_id"], name: "index_story_emoticons_on_story_id", using: :btree
    t.index ["user_id"], name: "index_story_emoticons_on_user_id", using: :btree
  end

  create_table "story_products", force: :cascade do |t|
    t.integer  "productable_id"
    t.integer  "product_id"
    t.datetime "created_at"
    t.string   "productable_type", default: "Post", null: false
    t.time     "deleted_at"
    t.string   "association_type", default: "Post", null: false
    t.index ["productable_id", "productable_type"], name: "fk_productable_id_type", using: :btree
  end

  create_table "story_tags", force: :cascade do |t|
    t.string  "name"
    t.string  "tagable_type",                      null: false
    t.integer "tagable_id",                        null: false
    t.time    "deleted_at"
    t.integer "tag_id"
    t.string  "association_type", default: "Post", null: false
    t.index ["tag_id"], name: "index_story_tags_on_tag_id", using: :btree
    t.index ["tagable_type", "tagable_id"], name: "fk_tagable", using: :btree
  end

  create_table "tags", force: :cascade do |t|
    t.string   "name"
    t.integer  "posts_count",  default: 0, null: false
    t.integer  "looks_count",  default: 0, null: false
    t.integer  "brands_count", default: 0, null: false
    t.integer  "user_id"
    t.time     "deleted_at"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["user_id"], name: "index_tags_on_user_id", using: :btree
  end

  create_table "user_types", force: :cascade do |t|
    t.string   "user_type",   default: "user", null: false
    t.integer  "users_count", default: 0,      null: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "user_user_types", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "user_type_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["user_id"], name: "index_user_user_types_on_user_id", using: :btree
    t.index ["user_type_id"], name: "index_user_user_types_on_user_type_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                   limit: 510, default: "", null: false
    t.string   "encrypted_password",      limit: 510, default: "", null: false
    t.string   "reset_password_token",    limit: 510
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                       default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",      limit: 510
    t.string   "last_sign_in_ip",         limit: 510
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.string   "random_auth_token",       limit: 510
    t.text     "firebase_auth_token"
    t.string   "fname",                   limit: 510
    t.string   "lname",                   limit: 510
    t.integer  "followers_count",                     default: 0
    t.integer  "followees_count",                     default: 0
    t.string   "gender",                  limit: 510
    t.string   "city",                    limit: 510
    t.integer  "country_id"
    t.string   "firebasePNId",            limit: 510
    t.string   "image_file_name",         limit: 510
    t.string   "image_content_type",      limit: 510
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "registration_code",       limit: 510
    t.string   "provider",                limit: 510
    t.string   "uid",                     limit: 510
    t.boolean  "admin"
    t.integer  "brand_score",                         default: 0
    t.integer  "lifestyle_score",                     default: 0
    t.date     "dob"
    t.integer  "total_likes",                         default: 0
    t.integer  "looks_count",                         default: 0
    t.integer  "total_guru_badges",                   default: 0
    t.integer  "total_ambassador_badges",             default: 0
    t.string   "app_version",             limit: 510
    t.boolean  "top_user"
    t.boolean  "bataz_icon_user"
    t.boolean  "blacklisted"
    t.string   "referer_code"
    t.time     "deleted_at"
    t.integer  "comments_count"
    t.integer  "report_abuses_count",                 default: 0,  null: false
    t.string   "username"
    t.string   "phone"
    t.text     "bio"
    t.jsonb    "metadata",                            default: {}, null: false
    t.integer  "posts_count",                         default: 0,  null: false
    t.index ["country_id"], name: "users_country_id_idx", using: :btree
    t.index ["email"], name: "users_email_key", unique: true, using: :btree
    t.index ["reset_password_token"], name: "users_reset_password_token_key", unique: true, using: :btree
  end

  create_table "visits", force: :cascade do |t|
    t.string   "visit_token",      limit: 510
    t.string   "visitor_token",    limit: 510
    t.string   "ip",               limit: 510
    t.text     "user_agent"
    t.text     "referrer"
    t.text     "landing_page"
    t.integer  "user_id"
    t.string   "referring_domain", limit: 510
    t.string   "search_keyword",   limit: 510
    t.string   "browser",          limit: 510
    t.string   "os",               limit: 510
    t.string   "device_type",      limit: 510
    t.integer  "screen_height"
    t.integer  "screen_width"
    t.string   "country",          limit: 510
    t.string   "region",           limit: 510
    t.string   "city",             limit: 510
    t.string   "postal_code",      limit: 510
    t.decimal  "latitude",                     precision: 10
    t.decimal  "longitude",                    precision: 10
    t.string   "utm_source",       limit: 510
    t.string   "utm_medium",       limit: 510
    t.string   "utm_term",         limit: 510
    t.string   "utm_content",      limit: 510
    t.string   "utm_campaign",     limit: 510
    t.datetime "started_at"
    t.index ["visit_token"], name: "visits_visit_token_key", unique: true, using: :btree
  end

  add_foreign_key "badges", "brands"
  add_foreign_key "badges", "users"
  add_foreign_key "banners", "brand_countries"
  add_foreign_key "bookmark_posts", "users"
  add_foreign_key "brand_countries", "brands"
  add_foreign_key "brand_countries", "countries"
  add_foreign_key "claps", "users"
  add_foreign_key "identities", "users"
  add_foreign_key "oauth_access_grants", "oauth_applications", column: "application_id"
  add_foreign_key "oauth_access_grants", "users", column: "resource_owner_id"
  add_foreign_key "oauth_access_tokens", "oauth_applications", column: "application_id"
  add_foreign_key "oauth_access_tokens", "users", column: "resource_owner_id"
  add_foreign_key "points", "brands"
  add_foreign_key "points", "users"
  add_foreign_key "push_notif_trackers", "users"
  add_foreign_key "report_abuse_actions", "report_abuses", column: "report_abuse_id"
  add_foreign_key "report_abuses", "abuse_types"
  add_foreign_key "report_abuses", "users"
  add_foreign_key "save_looks", "users"
  add_foreign_key "stories", "brands"
  add_foreign_key "stories", "products"
  add_foreign_key "stories", "users"
  add_foreign_key "story_emoticons", "emoticons"
  add_foreign_key "story_emoticons", "stories"
  add_foreign_key "story_emoticons", "users"
  add_foreign_key "users", "countries"
end
