import ReactOnRails from 'react-on-rails';
import { store } from '../../store'
import { TURBO_INIT } from '../actions/index'

import HelloWorld from '../components/HelloWorld';

// This is how react_on_rails can see the HelloWorld in the browser.
ReactOnRails.register({
  HelloWorld,
});

ReactOnRails.registerStore({ store });

document.addEventListener("turbolinks:before-render", function() {  
  store.dispatch({type: TURBO_INIT});
  if (document.documentElement.hasAttribute("data-turbolinks-preview")) {
    Turbolinks.clearCache();
    return;
  }
})
