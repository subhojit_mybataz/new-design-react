import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { sendInvite } from '../actions/index.js'
import React from 'react';
class SendInvite extends React.Component {

  static propTypes = {
    dispatch: PropTypes.func.isRequred
  }

  constructor(props, _railsContext){
    super(props);
    this.render = this.render.bind(this);
    this.state =({
      error_mesg: "hello"
    });
      clevertap.event.push("track", {"label" : "invite_friend_view"});
      if(ga) {
          ga('send', {
              hitType: 'event',
              eventCategory: 'engagement',
              eventAction: 'invite_friend_viewed',
              eventLabel: ""
          });
      }
  }

  submitInvite = (event) => {
    event.preventDefault();
    let emails = '';
    emails = this.refs.email_ids.value;
    var email = emails.split(",");
    var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var flag = true;
    for(var i = 0;i<email.length; i++)
    {
      var email_id = email[i].trim();
      if( email_id.length!=0 ){
        if(!pattern.test(email_id)){
          flag = false;
          this.refs.email_ids.classList.add("error-input");
          this.setState({error_mesg: email[i]+" is an invalid email id. Please enter the correct one."});
          this.refs.error_message.classList.add("show");
        }
      } else{
        flag = false;
      }
    }
    if(flag)
    {
      var email_sec = this.refs.email_ids;
      var success_mesg_sec = this.refs.success_mesg;
      var btn = this.refs.invite_btn;
      email_sec.classList.remove("error-input");
      this.refs.error_message.classList.remove("show");
      email_sec.value = "";
      email_sec.classList.add("hide");
      success_mesg_sec.classList.add("show");
      btn.classList.add("hide");
      this.props.emailsSubmitted(emails);
      setTimeout(function(){
        $("#invite_popup").modal('hide');
        email_sec.classList.remove("hide");
        success_mesg_sec.classList.remove("show");
        btn.classList.remove("hide");
      },3000);

        clevertap.event.push("Invite_friend", {"invited_email" : emails});
        if(ga) {
            ga('send', {
                hitType: 'event',
                eventCategory: 'engagement',
                eventAction: 'invite_friend',
                eventLabel: emails
            });
        }
    }
  }

  render() {


  return(
      <div className ="modal fade" ref="invite_popup" role="dialog" id="invite_popup">
        <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-body">
            <form onSubmit={this.submitInvite}>
            <div className="row remove_row_margin">
              <div className="email_error remove_decoration" ref="error_message">
                {this.state.error_mesg}
              </div>
              <textarea id="email_ids" name="emails" className = "new-post-description" placeholder="Click here to enter your friend(s) email(s) separated by commas"  ref="email_ids"/>
              <div className="success_mesg" ref="success_mesg">
                <button type="button" className="close" data-dismiss="modal">&times;</button>
                Your invitation was sent sucessfully.
              </div>
            </div>
            <div className="row margin-top-10 remove_row_margin">
              <button ref="invite_btn" type='submit' className="btn button-styling" style={{"outline":"none"},{"float":"right"}}>Send invitation</button>  
            </div>
          </form>
          </div>
        </div>
      </div>
    </div>
    );
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    dispatch,
    emailsSubmitted: (emails) => {
      dispatch(sendInvite(emails));
    }
  }
}

const SendInviteWithRedux = connect(null,mapDispatchToProps)(SendInvite);
export default SendInviteWithRedux;