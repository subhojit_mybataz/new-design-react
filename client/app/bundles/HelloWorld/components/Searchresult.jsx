import PropTypes from 'prop-types';
import React from 'react';
import {Component} from 'react';
import { connect } from "react-redux";
import { fetchSearchResult } from '../actions/index.js'
import PostsFeed from './PostsFeed';
import UsersFeed from './UsersFeed';
import { store } from '../../store'


class SearchResult extends Component {

  static propTypes = {
    dispatch: PropTypes.func.isRequired
  }

  /**
   * @param props - Comes from your rails view.
   * @param _railsContext - Comes from React on Rails
   */
  constructor(props, _railsContext) {
    super(props);
    this.render = this.render.bind(this);
  }

  render() {
    
    if(this.props.empty_search_result==true && store.getState().posts.length == 0 && store.getState().follow_notification_details.length == 0)
      return (<div className="center-align no-search-result"> No search results found</div>);
    else
    return (
        <div>
          <UsersFeed/>
          <div className="post">
            <PostsFeed feedAction={fetchSearchResult}/>
          </div>
        </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    empty_search_result: state.empty_search_result
  }
}

const SearchResultWithRedux = connect(mapStateToProps)(SearchResult);
export default SearchResultWithRedux
