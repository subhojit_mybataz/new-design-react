import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { Provider } from "react-redux";
import { Component } from 'react';
import ReactDOM from 'react-dom';
import React from 'react';

class TrendingUser extends Component{

  static PropTypes = {
    user: PropTypes.object.isRequired
  }
  debugger;

  constructor(props, _railsContext) {
    super(props);
    this.render = this.render.bind(this);
  }

render() {
  var user = this.props.user;
  // console.log(user);
  var uName = user.name;
  if(uName.length > 18){
    var sName = uName.substr(0,14)+"...";
    uName = sName;
  }
  return (
    <div key={user.id} className="user_rows">
        <div className="top_user_image user_info">
          <a href={"/profile/"+user.id}>  <img src={user.profile_pic.thumb} className="circular-image sidebar_user_img_size"/></a>
        </div>
      <div className="width_setter">
        <div style={{"display":"inline-block"}}>
          <a href={"/profile/"+user.id} className="top_user_name sidebar_user_text">{uName}</a>
          <a href={"/profile/"+user.id} className="view_profile_text sidebar_user_text">View profile</a>
        </div>
        <div className="lifestyle_score top_user_score">
         {user.lifestyle_score}
        </div>
      </div>
    </div>
    );
  }

}
const TrendingUserWithRedux = connect()(TrendingUser);
export default TrendingUserWithRedux;