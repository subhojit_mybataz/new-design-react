import PropTypes from 'prop-types';
import React from 'react';
import { Component } from 'react';
import { Provider } from "react-redux";
import { connect } from "react-redux";
import Post from './Post'
import { fetchPosts, sendPost, toggleLike, registerEmbeddedAppAuthHeaders } from '../actions/index.js'
import ReactOnRails from 'react-on-rails';
import Waypoint from 'react-waypoint';
import NewPost from './NewPost';
import TrendingBrand from './TrendingBrand';
import MarketingMessage from './MarketingMessage';


var counter = 0;
var that;

class PostsFeed extends Component {

  static propTypes = {
    posts: PropTypes.array.isRequired,
    isInfiniteLoading: PropTypes.bool.isRequired,
    dispatch: PropTypes.func.isRequired,
    feedAction: PropTypes.func.isRequired
  }

  componentDidMount(nextProps) {
    registerEmbeddedAppAuthHeaders()
    that = this;
    // console.log(this.props.brandid);
    if(this.props.brandid) {
      this.props.dispatch(this.props.feedAction(this.props.brandid))
    }
    else {
      this.props.dispatch(this.props.feedAction())
    }
  }

  componentWillReceiveProps(nextProps) {
    console.log('nextProps = ' + nextProps);
  }

  /**
   * @param props - Comes from your rails view.
   * @param _railsContext - Comes from React on Rails
   */
  constructor(props, _railsContext) {
    super(props);
    this.handleInfiniteLoad = this.handleInfiniteLoad.bind(this);
    this.render = this.render.bind(this);
    // How to set initial state in ES6 class syntax
    // https://facebook.github.io/react/docs/reusable-components.html#es6-classes
    //this.state = { name: this.props.name, isInfiniteLoading: true };
  }

  updateName = (name) => {
    this.setState({ name });
  }

  handleInfiniteLoad = () => {
    this.props.dispatch(this.props.feedAction());
  }

  elementInfiniteLoad() {
    return (
      <div className="infinite-list-item">
         Loading...
      </div>
    );
  }

  render() {
    // console.log(this.props.posts);
    var postsList = '',
      that = this;
    if (this.props.posts && this.props.posts.length != 0) {
      postsList = this.props.posts.map((post, index) => {
        var post_dom ="";
        var advertise="";
        
        // if post is a marketing message type
        /*if(post.type=="marketing_message"){
          post_dom =(<MarketingMessage marketingMesg={post}/>);
        }else if(post.type=="trending_brand")
        {*/
          // get brand.id from active admin or something...
          // fetch brand_name, total_likes, total_comments, images.
          // call trending_brand component with flag=true and brand.id
          // add <TrendingBrand brand.id flag/> to the list of posts.
          /*post_dom = (<TrendingBrand brand_post={post} country_name="India" loc_sidebar={false}/>);
        }else{*/
        //return <Post post={post} key={post.id} />;

        /*if(index == index + 5) {
          // post an ad
          advertise="";
        }*/

        // if(index%5==0 && index!=0) {
        //   //post an ad
        //   advertise=(<div className="advertise-div">Advertisement comes here!</div>);
        // }

        post_dom = (<Post postid={post.id} ref={"post_"+counter++} post={post} />);
      // }
        let item, current_key = counter++;

        if (index == this.props.posts.length - 2 || this.props.posts.length < 2) {
          item = (
            <div key={current_key} id={"post_"+(index+1)}>
              {advertise}
              {post_dom}
              {this.props.all_post_feed_exhausted ? null : <Waypoint onEnter={this.handleInfiniteLoad} />}
            </div>
          );
        } else {
          item = (
            <div key={current_key} id={"post_"+(index+1)}>
              {advertise} 
              { post_dom } 
            </div>
          );
        }
        return item;
      });
      return (
        <div>
          {postsList}
        </div>
      );

    } else {
        return <br/>;
    }
  }

}

function mapStateToProps(state) {
  // console.log(state);
  return {
    posts: state.posts,
    isInfiniteLoading: state.isInfiniteLoading,
    all_post_feed_exhausted: state.all_post_feed_exhausted,
    trending_post: state.trending_post
  }
}

const PostsFeedWithRedux = connect(mapStateToProps)(PostsFeed);
export default PostsFeedWithRedux
