import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { toggleLike, addCommentViaAPI, getNewPostUserDetails, getNewPostDetails, registerEmbeddedAppAuthHeaders, getAutoCompleteDetails } from '../actions/index.js';
import ReactDOM from 'react-dom';
import React from 'react';
window.typeahead_init=false;
class NewPost extends React.Component {

  static propTypes = {
    dispatch: PropTypes.func.isRequired,
  };


  constructor(props, _railsContext) {
    super(props);
    this.render = this.render.bind(this);
    this.isFormValid = true;
    this.state = {
      description: null,
      images_attributes: [],
      brand_ids: [],
      product_ids: [],
      price: '',
      errors: {
        description: false,
        brand_ids: false,
        product_ids: false,
      },
      has_errors: false
    }
  }

  validate = () => {
    let errors = Object.assign({}, this.state.errors);

    //description validation4
    // console.log(this.state.description.value);
    let check_description = this.state.description.value ? false : true;
    // console.log("check_description", check_description);
    errors.description = check_description;

    // image validation
    // console.log(this.state.images_attributes[0].files);
    let check_image = this.state.images_attributes[0].files.length !== 0 ? false : true;
    // console.log("check_image", check_image);
    errors.images_attributes = check_image;

    //brand validation
    // console.log(this.state.brand_ids[0].value);
    let check_brand = this.state.brand_ids[0].value ? false : true;
    // console.log("check_brand", check_brand);
    errors.brand_ids = check_brand;

    //product validation
    // console.log(this.state.product_ids[0].value);
    let check_product = this.state.product_ids[0].value ? false : true;
    // console.log(check_product);
    errors.product_ids = check_product;

    this.setState({errors:errors});
    return errors;
  }

  areThereAnyErrors = () => {
    let has_errors = Object.assign({}, this.state.has_errors);
    const errors = this.validate();
    has_errors = Object.keys(errors).some(x => errors[x]);

    this.setState({has_errors: has_errors});
    return has_errors
  }

  handleSubmit = (event) => {
    event.preventDefault();
    console.log("Submitting")
    let error = this.areThereAnyErrors();
    if(!error){
      let data = {};
      data.description = this.state.description.value || "";
      // console.log(this.state.images_attributes);
      data.images_attributes = this.state.images_attributes[0] || null;
      let brand_string = this.state.brand_ids[0].value;
      let brands_list = this.props.auto_complete_details.brand_json;
      let selected_brand = brands_list.find(function (brand) {
        return brand.brand_name.toLowerCase() == brand_string.toLowerCase();
      });
      if(selected_brand){
        this.state.brand_selected.value = selected_brand.brand_id;
      }

      if(this.state.brand_selected.value){
        data.Brand = this.state.brand_selected.value;
      } else {
        // console.log(this.state.brand_ids);
        data.custom_brand = this.state.brand_ids[0].value;
      }


      let product_string = this.state.product_ids[0].value;
      let product_list = this.props.auto_complete_details.product_json;
      let selected_product = product_list.find(function (product) {
        return product.product_name.toLowerCase() == product_string.toLowerCase();
      });
      if(selected_product){
        this.state.product_selected.value = selected_product.product_id;
      }


      if(this.state.product_selected.value){
        data.Product = this.state.product_selected.value;
      } else {
        data.custom_product = this.state.product_ids[0].value;
      }

      data.Price = this.state.price.value;
      this.props.onFormSubmit(data);
    }
  }


  componentWillMount() {
    registerEmbeddedAppAuthHeaders();
    this.props.dispatch(getNewPostUserDetails())
    this.props.dispatch(getAutoCompleteDetails())
  }

  componentDidUpdate(prevProps, prevState) {

    // Need to keep track of the fact that typeahead is loaded, this is being done here
    // so that we can avoid react rendering the same page
    if (!$.isEmptyObject(this.props.auto_complete_details) && window.typeahead_init==false) {
      window.typeahead_init=true;

      var brands = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('brand_name', 'brand_id'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: this.props.auto_complete_details.brand_json
      });

      var products = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('product_name', 'product_id'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: this.props.auto_complete_details.product_json
      });

      $('#autocomplete_brand.typeahead').typeahead(null, {
        name: 'brands',
        source: brands,
        display: function(item) {
          return item.brand_name
        },
        suggestion: function(data) {
          return '<div>' + data.brand_name + '</div>'
        }
      });

      $('#autocomplete_product.typeahead').typeahead(null, {
        name: 'products',
        source: products,
        display: function(item) {
          return item.product_name
        },
        suggestion: function(data) {
          return '<div>' + data.product_name + '</div>'
        }
      });
    }

    $('#autocomplete_brand.typeahead').on('typeahead:selected', (e, datum) => {
      // console.log(datum);
      $('#brand_value_selected').val(Number(datum.brand_id));
    });

    $('#autocomplete_product.typeahead').on('typeahead:selected', (e, datum) => {
      // console.log(datum);
      $('#product_value_selected').val(Number(datum.product_id));
    });

  }

  render() {
    var post = this.props.new_post;
    var instance_new_post = this;
    var show_loader;

    this.props.new_post_created==false? show_loader="none" :show_loader="block";

    var loading_icon = "";

    if(this.props.new_post_creation_in_progress){
      loading_icon = (<i className="fa fa-refresh fa-spin fa-fw"></i>)
    }

    var error_class =this.state.errors.description ? 'error-input':"";

    return (
      <div className="mock-outer-text">
        <form onSubmit={this.handleSubmit}>
        {/*<div className="row pull-left">
          <div className="nopadding" style={{"display":"inline-block"}}>
            <a href={"/profile/"+post.user_id}>  <img src={post.profile_pic} className="circular-image"/></a>
          </div>
        </div>*/}

          
            <a href={"/profile/"+post.user_id}><img src={post.profile_pic} className="top-circular-image" alt="Avatar" /></a>
            <input id="new_post_desc" type="text" placeholder="Share your brand story.." className="brand-story" ref={(input) => {this.state.description = input}}/>
            {/*<div class="container-fluid content-button-section">
              <div class="col-sm-7 col-xs-3 video-section">
                <button type="submit" class="camera-button">
                  <i class="fas fa-camera"></i>
                  <label class="camera-text"><h4 class="camera-text">Image</h4></label>
                </button>
                <button type="submit" class="video-button">
                  <i class="fas fa-video"></i>
                  <label class="camera-text"><h4 class="camera-text">Video</h4></label>
                </button>
              </div>
              <button class="col-sm-1 col-xs-3 post-button">
                <h4 class="posted-text">Post</h4>
              </button>
            </div>*/}
            {/*<textarea id="new_post_desc" name="description" className='new-post-description' placeholder="Share your brand story..."  ref={(input) => {this.state.description = input}}/>*/}
         

        {/*<div class="content">
          <img src="assets/profile-picture.jpg" class="post-pic" style="width: 50px;" alt="Avatar" />
          <input type="text" placeholder="Share your brand story.." class="brand-story" />
        </div>*/}

          {/*<div class="container-fluid content-button-section">
            <div class="col-sm-7 col-xs-3 video-section">
              <button type="submit" class="camera-button">
                <i class="fas fa-camera"></i>
                <label class="camera-text"><h4 class="camera-text">Image</h4></label>
              </button>
              <button type="submit" class="video-button">
                <i class="fas fa-video"></i>
                <label class="camera-text"><h4 class="camera-text">Video</h4></label>
              </button>
            </div>
            <button class="col-sm-1 col-xs-3 post-button">
              <h4 class="posted-text">Post</h4>
            </button>
          </div>*/}

          {/*<div className={"row description new_post_desc "+error_class}>
            <div className="col-md-12 col-xs-12" style={{"paddingRight":"0px"}}>
              <textarea id="new_post_desc" name="description" className='new-post-description'   placeholder="Share your brand story..."  ref={(input) => {this.state.description = input}}/>
            </div>
          </div>*/}
          {/*<div className="description">
            <div className="row">
              <input id="new_post_image" className={this.state.errors.images_attributes ? "error-input disp_inline": "disp_inline"} type="file" name="image" accept="image/*" ref={(input) => {this.state.images_attributes[0] = input}} />
            </div>
            <div className="row margin-top-10">
              <div className="col-md-2 col-sm-2 col-xs-3 new_post_spacing">
                <input id='autocomplete_brand' className={this.state.errors.brand_ids ? 'typeahead error-input new_post_capsules tt-hint hint_user_info' : 'typeahead new_post_capsules hint_user_info'} type="text" name="brand" placeholder="Brand" ref={(input) => {this.state.brand_ids[0] = input}}/>
                <input id="brand_value_selected" style={{display: "none"}} value="" ref={(input) => {this.state.brand_selected = input}}/>
              </div>
              <div className="col-md-2 col-sm-2 col-xs-3 new_post_spacing">
                <input id='autocomplete_product' className={this.state.errors.product_ids ? 'typeahead error-input new_post_capsules tt-hint hint_user_info' : 'typeahead new_post_capsules hint_user_info'} type="text" name="product" placeholder="Product" ref={(input) => {this.state.product_ids[0] = input}}/>
                <input id="product_value_selected" style={{display: "none"}} value="" ref={(input) => {this.state.product_selected = input}} />
              </div>
              <div className="col-md-2 col-sm-2 col-xs-3 new_post_spacing">
                <input id='autocomplete_price' className={this.state.errors.price ? 'typeahead new_post_capsules tt-hint hint_user_info error-input' : 'typeahead new_post_capsules tt-hint hint_user_info'} type="number" name="price" placeholder="Price" ref={(input) => {this.state.price = input}}/>
              </div>
              <div className="col-md-2 col-sm-2 col-xs-3 new_post_spacing">
                <button type="submit" className="btn button-styling" style={{"outline":"none"}} disabled={this.props.new_post_creation_in_progress}>Share { loading_icon }</button>
              </div>
            </div>
          </div>*/}
          <div className="row">
            { this.state.has_errors ? <div className="error_mesg">* Please provide required information</div> : '' }
          </div>
        </form>
      </div>

    );
  }
}


const mapStateToProps = (state) => {
  return {
    new_post: state.new_post,
    auto_complete_details: state.auto_complete_details,
    new_post_created: state.new_post_created,
    new_post_creation_in_progress: state.new_post_creation_in_progress
  };
}

const mapDispatchToProps = (dispatch, ownProps) => {
  var that = this;
  return {
    dispatch,
    onFormSubmit: (data) => {
      console.log(data);
      dispatch(getNewPostDetails(data));
    }
  };
}

const NewPostWithRedux = connect(mapStateToProps, mapDispatchToProps)(NewPost);
export default NewPostWithRedux