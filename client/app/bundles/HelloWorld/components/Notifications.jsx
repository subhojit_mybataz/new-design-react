import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { getPostDetails, getNotificationDetails, markAllNotificationsRead } from '../actions/index.js'
import React from 'react';
import PostsFeed from './PostsFeed';
import Post from './Post'

class Notifications extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    // notification_details: PropTypes.object.isRequired
    notifications: PropTypes.object.isRequired

  };

  constructor(props, _railsContext) {
    super(props);
    this.render = this.render.bind(this);
    this.mark_all_notifications_read = this.mark_all_notifications_read.bind(this);
  }


  componentWillMount() {
    this.props.dispatch(getNotificationDetails())
  }

  mark_all_notifications_read(data) {
    console.log(data);
    // this.props.dispatch(markAllNotificationsRead(data));
  }

  render() {

    let allReadButton = "";
    let allReadDiv = "";
    var notif_ids = [];
    // var notified = "";

    if(window.access_token) {
      allReadDiv = "phoneAlign";
      allReadButton = "";
    }else 
    {
      allReadDiv ="markAllRead";
      allReadButton = "btn btn-block btn-warning"; 
    }

    console.log(this.props.notifications);
    // if (!$.isEmptyObject(this.props.notification_details)) {
    if (this.props.notifications.notification_details && this.props.notifications.notification_details.length > 0) {

        var notificationsList = this.props.notifications.notification_details.map((notif, index) => {
        var notification_sentence_part="";
        var notification_sentence_part_2="";
        var notification_sentence_part_3="";
        var notification_href="";
        var notification_read_href="";

        console.log(notif);
        var owner_name = "";

        if(notif.user && !notif.read) {

          if(notif.user.name!="") {
            owner_name = notif.user.name;
          }
          else {
            owner_name = "User";
          }

          /*if(notif.metadata && notif.metadata.message) {
            var notification = notif.metadata.message.split(" ");
            owner_name = notification[0];
          }*/

          if(notif.activity_type==commented || notif.activity_type==clapped) {

            var messaged;
            var lastindex;
            var post_type;

            notification_sentence_part='on your';
            console.log(notif.metadata.message);
            console.log(notif.trackable_type);
            if(notif.trackable_type == trackable_type) {

              if(notif.metadata.message) {
                messaged = notif.metadata.message.split(' ');
                lastindex = messaged.length - 1;
              }

              if(messaged[lastindex] == type_post) {
                notification_sentence_part_2='post';
                post_type = type_post + "s";
              }
              else if(messaged[lastindex] == type_look) {
                notification_sentence_part_2='look';
                post_type = type_look + "s";
              }
              else {
                if(messaged[lastindex] == clapped_times) {
                  notification_sentence_part_2 = messaged[lastindex - 2] + " ";
                  notification_sentence_part_3 = messaged[lastindex - 1] + " " + messaged[lastindex];
                  post_type = messaged[lastindex - 2] + "s";
                }
              }
              // console.log(messaged);
            }

            notification_href = '/' + post_type + '/' + notif.trackable_id;
            notification_read_href = '/' + post_type + '/' + notif.trackable_id + '?notif_id=' + notif.id;
            // console.log(notif);
            // console.log(notification_href);
          }
          else if(notif.activity_type==followed) {
            notification_sentence_part_2='you';
            notification_href = '/profile/' + notif.recipient_id;
          }
          else if(notif.activity_type==warnings || notif.activity_type==removed) {
            return;
          }
          else {
            notification_sentence_part='your';
          }

          notif_ids.push(notif.id);

          return (
              <div key={index} className={ notif.read ? "col-md-6 col-md-offset-3 alert notification-read" : "col-md-6 col-md-offset-3 alert notification-unread"}>
                <div className="row">
                  <div className="col-xs-2 text-center"><img className="circular-image" src={notif.user.image_url}/></div>
                  <div className="col-xs-10 col-sm-8 notification-message">
                    <span>
                      <a href={'/profile/' + notif.user.id}> {owner_name} </a>
                       {notif.activity_type} {notification_sentence_part} 
                      <a href={notification_href}> {notification_sentence_part_2}</a> {notification_sentence_part_3}
                    </span>
                    <br/>
                    <span className="notification-timestamp"><i className="fa fa-clock-o" aria-hidden="true"></i> {notif.created_at_string}</span>

                  </div>
                  
                  <div className="col-sm-2 hidden-xs text-center vertical-align-parent" style={{"height":"50px"}}>
                    <div className="vertical-align-child" style={{"width":"90%"}}>
                      <a href={notification_read_href}>
                        <i className="fa fa-chevron-right" aria-hidden="true"></i>
                      </a>
                    </div>
                  </div>
                  
                </div>
              </div>
          );
        }
      });

      return (
        <div className="container">
          <div className={allReadDiv}>
            <button className={allReadButton} onClick={() => this.mark_all_notifications_read(notif_ids)}>Mark all as read </button>
          </div>
          <br />
          <br />
          {notificationsList} 
        </div>
      );
    } else {
        console.log(this.props.notifications);
      if(this.props.notifications_request_completed==false && this.props.notifications.notification_details.length != 0)
          return(<div className="center-align" style={{"display":"block"}}>
                <img id="loading_pic" name="loading_pic" ref='loading_pic' className="load-icon-dimensions" src={loading_pic}/>
              </div>
          );
        else  
          return (<div className="no-notifications">No Notifications</div>);
    }
  }
}

const mapStateToProps = (state) => {
  console.log(state);
  return {
    /*notification_details: state.notification_details,*/
    notifications: state,
    notifications_request_completed: state.notifications_request_completed
  };
}

const NotificationDetailsWithRedux = connect(mapStateToProps, null)(Notifications);
export default NotificationDetailsWithRedux
