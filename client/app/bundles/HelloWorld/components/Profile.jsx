import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { getProfile, fetchPosts, fetchUserPosts, toggleFollowOnProfilePage, registerEmbeddedAppAuthHeaders } from '../actions/index.js'
import React from 'react';
import PostsFeed from './PostsFeed';
import UsersFeed from './UsersFeed';


class Profile extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired
  };

  constructor(props, _railsContext) {
    super(props);
    this.render = this.render.bind(this);
  }

  componentDidMount(nextProps) {
    registerEmbeddedAppAuthHeaders()
    this.props.dispatch(getProfile())
    // this.props.dispatch(getUserFeed())
    $("#profileImage").click(function(e) {
      console.log(e.target.dataset);
      if(e.target.dataset.profile_id==current_user_id)
        $("#imageUpload").click();
    });

    function fasterPreview(uploader) {
      $("#pic_errors").hide();
      if (uploader.files && uploader.files[0]) {
        $('#profileImage').attr('src',
          window.URL.createObjectURL(uploader.files[0]));
        var formData = new FormData();
        formData.append("file", uploader.files[0], uploader.files[0].name);
        formData.append("upload_file", true);
        $.ajax({
          type: "POST",
          url: "/api/v1/user/new_profile_pic",
          success: function(resp) {
            console.log("profile pic updated");
          },
          error: function(error) {},
          async: true,
          data: formData,
          cache: false,
          contentType: false,
          processData: false,
          timeout: 60000
        });
      }
    }

    $("#imageUpload").change(function() {
        console.log("sdsdssd");
        fasterPreview(this);
    });
  }

  render() {
    // console.log(this.props.profile.data);
    if (this.props.profile.data) {
      var profile = this.props.profile.data;
      // console.log(current_user_id);
      // console.log(profile.id);
      // var userfeed = this.props.userfeed.data.posts;
      var following = profile.profile_user_following_current_user ? "Unfollow" : "Follow";
      var follow_visible=profile.id==current_user_id? "hidden" :"visible" ;
      var message_to_change_profile_pic=profile.id==current_user_id? "Click to update picture" : "";
      var mutual_follower=profile.mutual_follower==true? 'Mutual Follower' : '' ;
      var bataz_icon_user = profile.bataz_icon_user==true? 'Bataz Icon' : '';
      var badge_visibility_ambassador = profile.ambassadors>0? "top_badge" : "";
      var badge_visibility_guru = profile.gurus>0? "top_badge" : "";

      return (
      <div className="profile_header">
        <div className="center-align mock-outer padding-left-right-zero ">
        <div className="row remove_row_margin">
          <div className="col-md-5 col-xs-12">
           <img id="profileImage" src={profile.img_url} data-profile_id={profile.id} style={{width:'100%'}}/> 
           <input type="file" id="imageUpload" name="profile_photo" placeholder="Photo" accept="image/*"/>
           {/*<div id="profile_pic_message">{message_to_change_profile_pic}</div>*/}
          </div>
          <div className="col-md-7 col-xs-12">
          <div className="row remove_row_margin" style={{"float":"right"}}>
            <div className="display_bataz_icon country_name">
               {bataz_icon_user}
             </div>
          </div>
          <div className="row" style={{paddingLeft:"15px"}}>
           <div className="title_name left-align"> 
            {profile.name}
          </div>
          <div className="country_name left-align">
            {profile.place}
          </div>
          </div>
          <div className="row left-align margin-top-15">
            <div className="col-md-6 col-xs-5">
              <div className="title_followers align_to_left" style={{"textAlign":"center"}}>
                  {profile.followers}
                  <div className="title_followers_text">
                    Followers
                  </div>            
              </div>
            </div>
            <div className="col-md-6 col-xs-6" style={{"visibility":follow_visible}}>
              <button onClick={this.props.toggledFollow} className="btn button-styling" type="button">{following }</button>
            </div>
          </div>

          </div>
          </div>
          <div className="title_comm_header text-center">
            <span className="title_comm_text am_text"> ACTIVITY METER</span>
          </div>
            <div className="activity_meter_details">
              <div className="row am_profile_width">
                <div className="am_sections am_sections_3">
                  <div className="am_num">
                  {profile.post_count}
                  </div>
                  <div className="am_text">Posts</div>
                </div>
                <div className="am_sections am_sections_3">
                  <div className="am_num">
                  {profile.brand_score}
                  </div>
                  <div className="am_text">Brand Score</div>
                </div>
                <div className="am_sections am_sections_3" style={{"borderRight":"none"}}>
                  <div className="am_num">
                  {profile.lifestyle_score}
                  </div>
                  <div className="am_text">Lifestyle Score</div>
                </div>
              </div>
            </div>

            <div className="title_comm_header text-center">
              <span className="title_comm_text am_text">BRAND BADGES EARNED</span>
            </div>

            <div className="brand_badge_section row text-center" style={{"marginTop":"30px"}}>
              <div className="am_sections brand_badge">
                <div className={"badge_circle guru_badge "+badge_visibility_guru} data-badge={profile.gurus}>
                  <div className="badge_heart margin-top-10"></div>
                </div>
                  <div className="am_text">Brand Guru</div>
              </div>
              <div className="am_sections brand_badge" style={{"borderRight":"none"}}>
                <div className={"badge_circle ambassador_badge "+badge_visibility_ambassador} data-badge={profile.ambassadors}>
                  <div className="badge_star margin-top-10"></div>
                </div>
                <div className="am_text">Brand Ambassador</div>
              </div>
            </div>
          </div>
          <UsersFeed />
          <PostsFeed feedAction={fetchUserPosts} page={'profile_page'}/>
        </div>
      );
    } else {
      return null
    }
  }
}

function mapStateToProps(state) {
  return {
    profile: state.profile
    // userfeed: state.userfeed
  };
}


const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    dispatch,
    toggledFollow: () => {
      dispatch(toggleFollowOnProfilePage());
    }
  };
}

const ProfileWithRedux = connect(mapStateToProps, mapDispatchToProps)(Profile);
export default ProfileWithRedux
