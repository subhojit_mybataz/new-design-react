import PropTypes from 'prop-types';
import React from 'react';
import { Component } from 'react';
import { Provider } from "react-redux";
import { connect } from "react-redux";
import Post from './Post'
import { fetchPosts, sendPost, toggleLike, registerEmbeddedAppAuthHeaders } from '../actions/index.js'
import ReactOnRails from 'react-on-rails';
import FollowUser from './FollowUser'

class UsersFeed extends Component {

  static propTypes = {
    users: PropTypes.array.isRequired,
    dispatch: PropTypes.func.isRequired,
  }

  componentDidMount(nextProps) {
    registerEmbeddedAppAuthHeaders()
  }

  componentWillReceiveProps(nextProps) {
    console.log('nextProps = ' + nextProps);
  }

  constructor(props, _railsContext) {
    super(props);
    this.render = this.render.bind(this);
  }

  render() {
    // console.log(this.props.user_search_results);
    var usersList = '';
    if (this.props.user_search_results && this.props.user_search_results.length != 0) {
      usersList = this.props.user_search_results.map((user, index) => {
          var item = (
            <div> 
              <FollowUser item={user} loc="search"/>
            </div>
          );
        return item;
      });
      return (
        <div>
          <br/>
          {usersList}
        </div>
      );

    } else {
        return <br/>;
    }
  }

}

function mapStateToProps(state) {
  // console.log(state);
  return {
    user_search_results: state.follow_notification_details
  }
}

const UsersFeedWithRedux = connect(mapStateToProps)(UsersFeed);
export default UsersFeedWithRedux
