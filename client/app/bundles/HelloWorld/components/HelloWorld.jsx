import PropTypes from 'prop-types';
import React from 'react';
import { Component } from 'react';
import { Provider } from "react-redux";
import { store } from '../../store'
import ReactOnRails from 'react-on-rails';
import BatazAppWithRedux from './BatazApp';
import Profile from './Profile';
import Brand from './Brand';
import PostDetails from './PostDetails';
import NavHeader from './NavHeader';
import Notifications from './Notifications';
import FollowNotifications from './FollowNotifications';
import SearchResult from './Searchresult';
import SideBar from './SideBar';
import SendInvite from './SendInvite';
import RightSidebar from './RightSidebar'


export default class HelloWorld extends Component {

  constructor(props, _railsContext) {
    super(props);
    //this.state = { name: this.props.name, isInfiniteLoading: true };
  }

  render() {
    var page;
    var header;
    var sideBar =" ";
    var r_sidebar = " ";
    var post_design = "";
    var top_position ="post-header-space";
    var send_invite = "";
    switch (this.props.page) {
      case 'profile':
        page = <Profile/>;
        sideBar = <SideBar />;
        send_invite = <SendInvite />;
        r_sidebar = <RightSidebar />;
        post_design = "post_with_trending";
        top_position = "";
        header=<NavHeader page="profile" incSidebar={true}/>;
        break;
      case 'post_details':
        page = <PostDetails/>;
        post_design="post";
        header=<NavHeader page="profile" incSidebar={false}/>;
        break;
      case 'brand':
        page = <Brand/>;
        sideBar = <SideBar />;
        send_invite = <SendInvite />;
        r_sidebar = <RightSidebar />;
        post_design = "post_with_trending";
        top_position = "";
        header=<NavHeader page="brand" incSidebar={true}/>;
        break;
      case 'notification':
        page = <Notifications/>;
        header=<NavHeader page="notifications" incSidebar={false}/>;
        break;

      case 'feed':
        page = <BatazAppWithRedux/>;
        sideBar = <SideBar />;
        send_invite = <SendInvite />;
        r_sidebar = <RightSidebar />;
        post_design = "post_with_trending outer-card";
        top_position = "";
        header= <NavHeader page="home_page" incSidebar={true}/>;
        break;
      case 'follow_notification':
        page=<FollowNotifications/>;
        header=<NavHeader page="follow_notifications" incSidebar={false}/>;
        break;
      case 'search':
        page=<SearchResult/>;
        header=<NavHeader page="search" incSidebar={false}/>;
        break;
    }

    return (
      <Provider store={store}>
        <div>
        {header}
        <br/>
        {send_invite}
        <div className={top_position}>
          <div className={post_design}>
            {page}
         </div>
            
        </div>
        </div>
      </Provider>
    );


  }
}

// {sideBar}
// {r_sidebar}
