import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { getNotificationCount,getSearchResults, fecthUserProfilePic } from '../actions/index.js'
import SendInvite from './SendInvite';
import React from 'react';

class NavHeader extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    notification_count: PropTypes.object.isRequired,
    incSidebar: PropTypes.bool.isRequired
  };

  constructor(props, _railsContext) {
    super(props);
    this.render = this.render.bind(this);
    this.show_sidebar = this.show_sidebar.bind(this);
    this.show_invite =this.show_invite.bind(this);
  }


  componentWillMount() {
    if(!window.access_token)
      this.props.dispatch(getNotificationCount())
  }

  componentDidMount() {
    this.props.dispatch(fecthUserProfilePic());
  }
  componentDidUpdate(prevProps, prevState) {
    $('#search_box').keypress(function(e){
      if(e.keyCode==13)
       $('#search_submit_button').click();
    });
  }

  show_sidebar() {
    var sidebar_obj = document.getElementById("sideMenu");
    sidebar_obj.classList.toggle("show");
    if(sidebar_obj.classList.contains("show")){
      $('#carousel_brand').carousel({
        interval: 10000
        });
      $('#carousel_user').carousel({
        interval: 15000
        });
    }else{
      $('#carousel_brand').carousel('pause');
      $('#carousel_user').carousel('pause');
    }
  }

  show_invite(){
    //display the popup
    $("#invite_popup").modal("show");
  }

  render() {

    var nav_element ='';
    var page=this.props.page;
    var instance_nav_bar = this;
    var search_value=""; 
    var url_values=window.location.href.split('/');
    var hamburger_menu = "";
    var phone_menu="";
    var search_div_classes = "";
    var adjustment = window.access_token? "": "nopadding";
    var settingsicon = "";
    var before_search_invite = (
      <div className={"col-xs-1 col-sm-1 before_search_invite vertical-align-parent desktop-mobile"+ adjustment}>
        <div className = "send_invite_section navbar_vertical_align_child add_padding_5">
          <div className="bell-button" style={{"backgroundColor":"white"}} onClick={this.show_invite}>
            <div className="follow_icon invite_icon"></div>
          </div>
        </div>
      </div>
      );
    var after_search_invite = (
      <div className="col-md-2 col-sm-2 after_search_invite vertical-align-parent desktop-mobile">
        <div className = "send_invite_section navbar_vertical_align_child">
          <div className="si_capsule" onClick={this.show_invite}>
            <div className="follow_icon invite_icon"></div>
            <div className="capsule-text send_invite_text">
              Invite a Friend
            </div>
          </div>
        </div>
      </div>
      );
    if($.inArray("search", url_values)!=-1){
      search_value=url_values[4];
      $("#search_box").val(search_value);
    }
  if (userLoggedIn)
       search_div_classes="col-md-3 col-xs-5 col-sm-5 desktop-mobile";
    if(!window.access_token){
      if(this.props.incSidebar == false){
        search_div_classes="col-xs-9 col-xs-offset-0 extra_margin";
        before_search_invite=""
      }
      else{
        search_div_classes="col-xs-9 col-xs-offset-0 phone_search_align";
        phone_menu = "phone_menu_align";
      }
    }

    // Logic for displying hamburger menu
    
    if(this.props.incSidebar == true)
    {
      hamburger_menu = (
        <div className="col-md-1 col-xs-1 navbar-blocks vertical-align-parent ham_menu">
          <div className={phone_menu+" navbar_vertical_align_child"} onClick={this.show_sidebar}>
            <div className="hamburger-logo"></div>
          </div>
        </div>
        );
    }

    var searchicon = (
        <div className="col-md-1 col-xs-1 navbar-blocks vertical-align-parent search_icon_header mobile_margin">
          <div className={phone_menu+" navbar_vertical_align_child"} onClick={this.show_sidebar}>
            <div className="search-icon"></div>
          </div>
        </div>
      );

    var icons=(
        <div className="col-md-1 col-xs-1 col-sm-2 vertical-align-parent nopadding desktop-mobile">
          <div className="row logos">
            <div className="col-sm-6 col-xs-6">
              <a href={'/'} className="logo_bg navbar_vertical_align_child add_padding_5"><div className="hamburger-logo"></div></a>
            </div>
            <div className="col-sm-6 col-xs-6">
              <a href={'/'} className="logo_bg navbar_vertical_align_child add_padding_5"><div className="bataz-logo"></div></a>
            </div>
          </div>
        </div>
      );

    var mobile_bataz_logo=(
      <div className="col-xs-2 text-center navbar-blocks vertical-align-parent mobile_notificationicon mobile_margin">
        <a href={'/follownotifications'} style={{"color":"#FF9800"}} className="bell-button navbar_vertical_align_child nav-icon-allignment">
          <div className="mobile-bataz-logo"></div>
        </a>
      </div>
      );
       
    
    var searchbar=(<div id='search_div' className={search_div_classes + " navbar-blocks vertical-align-parent"}>
      <div className="navbar_vertical_align_child">
          <div id="custom-search-input" className="input-group nav_search_box nav_search_style">
            <input id="search_box" ref="search_text" type="text" className="nav_search_text" placeholder="I am looking for.." />
            <span className="input-group-btn">
              <button id="search_submit_button" className="btn nav_search_btn" type="button" onClick={(e)=> this.props.onSearchSubmit(e,instance_nav_bar) } style={{"outline":"none"}}>
                <i className="glyphicon glyphicon-search"></i>
              </button>
            </span>
          </div>
      </div>
    </div>
    );
    

    var notificationicon=(<div className="col-xs-4 col-md-offset-1 text-center navbar-blocks vertical-align-parent notificationicon">
        <a href={'/notifications'} style={{"color":"white"}} className="bell-button navbar_vertical_align_child nav-icon-allignment">
          <div className="follow_icon notification_bell_icon"></div>
          { this.props.notification_count.count > 0 ? <div className="button__badge"></div> : null }
        </a>
      </div>
      );
    var mobile_notificationicon=(<div className="col-xs-2 col-md-offset-1 text-center navbar-blocks vertical-align-parent mobile_notificationicon mobile_margin">
        <a href={'/notifications'} style={{"color":"white"}} className="bell-button navbar_vertical_align_child nav-icon-allignment">
          <div className="follow_icon notification_bell_icon"></div>
          { this.props.notification_count.count > 0 ? <div className="button__badge"></div> : null }
        </a>
      </div>
      );
    // console.log(this.props.profile_pic);
    if(this.props.profile_pic && this.props.profile_pic.hasOwnProperty('thumb')){
      settingsicon=(
        <div className="col-xs-2 col-md-offset-1 text-center navbar-blocks vertical-align-parent">
            <a href={'/users/edit'}>  <img src={this.props.profile_pic.thumb} className="navbar_vertical_align_child circular-image navbar_profile_pic nav-icon-allignment settings-icon"/></a>
        </div>
      );
    }
    else {
      settingsicon=(
        <div className="col-xs-2 col-md-offset-1 text-center navbar-blocks vertical-align-parent">
            <a href={'/users/edit'}>  <img src="/missing.jpg" className="navbar_vertical_align_child circular-image navbar_profile_pic nav-icon-allignment settings-icon"/></a>
        </div>
      );
    }
    
    var followicon=(<div className="col-xs-4 text-center navbar-blocks vertical-align-parent followicon">
        <a href={'/follownotifications'} style={{"color":"#FF9800"}} className="bell-button navbar_vertical_align_child nav-icon-allignment">
          <div className="follow_icon follow_icon_image"></div>
        </a>
      </div>
    );
    var mobile_followicon=(
      <div className="col-xs-2 text-center navbar-blocks vertical-align-parent mobile_followicon mobile_margin">
        <a href={'/follownotifications'} style={{"color":"#FF9800"}} className="bell-button navbar_vertical_align_child nav-icon-allignment">
          <div className="follow_icon follow_icon_image"></div>
        </a>
      </div>
      );

    if(window.access_token) {
      // batazlogo="";
      // notificationicon="";
      // followicon="";
      // settingsicon="";
      // if(page=="follow_notifications" || page=="notifications"){
      //   return (<div></div>);
      // }
    }

    

    if(page=="follow_notifications" || page=="notifications"){
      searchbar=(<div className={search_div_classes}></div>);
      before_search_invite = (<div className="col-xs-0 col-sm-0 col-md-0" > </div>);
      after_search_invite = (<div className="col-md-2 col-sm-2 col-xs-2 navbar-blocks"></div>);
    }
      // console.log(userLoggedIn);
      // {after_search_invite}
    if(userLoggedIn){
      nav_element = (
          <div className="row remove_row_margin">
            {hamburger_menu}
            {icons}
            {before_search_invite}
            {searchbar}
            <div className="row col-xs-11 col-xs-offset-0 col-sm-3 col-md-3 nav-icons mobile_nav">
              {mobile_bataz_logo}
              {searchicon}
              {followicon}
              {mobile_followicon}
              {notificationicon}
              {mobile_notificationicon}
              {settingsicon}   
            </div>
          </div>       
      );
    } else {
      nav_element= (
        <div className="row remove_row_margin">
          {hamburger_menu}
          {icons}
          <div className="col-md-7 col-md-offset-2 col-xs-9">
            {searchicon}
           {searchbar}
          </div>
        </div>
      );
    }
    return (
      <div className="navbar navbar-default navbar-fixed-top nav_styling">
        {nav_element}
      </div>
      );
  }
}


const mapStateToProps = (state) => {
  return {
    notification_count: state.notification_count,
    search_result: state.posts,
    empty_search_result: state.empty_search_result,
    profile_pic: state.profile_pic
  };
}

const mapDispatchToProps = (dispatch, ownProps) => {
  var that = this;
  return {
    dispatch,
    onSearchSubmit: (e,instance_nav_bar) => {
      if(instance_nav_bar.refs.search_text.value.length!=0)
        redirectTo('/search/' + instance_nav_bar.refs.search_text.value);
    }
  };
}


const NavHeaderWithRedux = connect(mapStateToProps, mapDispatchToProps)(NavHeader);
export default NavHeaderWithRedux
