import PropTypes from 'prop-types';
import React from 'react';
import { Provider } from "react-redux";
import { connect } from "react-redux";
import { fetchRightSidebarElements } from '../actions/index.js'
import ReactOnRails from 'react-on-rails';
import RightSidebarElement from './RightSidebarElement'

class RightSidebar extends React.Component{

  static PropTypes={
    dispatch: PropTypes.func.isRequired
  }

  constructor(props, _railsContext) {
    super(props);
    this.render = this.render.bind(this)
    this.handleScroll = this.handleScroll.bind(this);
  }

  componentWillMount(nextProps) {
    this.props.dispatch(fetchRightSidebarElements());
  }

  handleScroll = (e) => {
    const ele = document.getElementById("scrollable_right_sidebar");
    var event = e,
    d = -event.deltaY || -event.detail ;

    ele.scrollTop += ( d < 0 ? 1 : -1 ) * 30;
    e.preventDefault();
  }

  render() {
    var onGoingLink = "";
    var r_elements = this.props.rightSidebarElements.map((element,index) => {
      if(index == 0){
        onGoingLink = element.link;
      }else{
      return (
        <div key={element.link} className="sidebar_component">
          <RightSidebarElement rSbElement={element} />
        </div>
        );
      }
    });

   return ( 
    <div className="sideBar sidebar_common right_sidebar_alignment" id="RightsideMenu">
        <div className="scrollable_inside" id="scrollable_right_sidebar" onWheel = {(e) => {this.handleScroll(e)}}>
          <div className="sidebar_component user_rows">
              <a href={onGoingLink} className = "t_headers padding-top-10" style={{"paddingBottom":"15px"}} target="_blank">
                <div className="gift_image margin-right-dynamic"></div>
                <div className="gift_text">
                  <p className ="trending_heading">See all ongoing <br/><span className ="trending_heading sr_red_font">Contests & Rewards</span></p>
                </div>
              </a>
            </div>
          {r_elements}
        </div>
      </div>
      );
  }
}

function mapStateToProps(state) {
  return {
    rightSidebarElements: state.rightSidebarElements
  }
}

const RightSidebarWithRedux = connect(mapStateToProps)(RightSidebar);
export default RightSidebarWithRedux;