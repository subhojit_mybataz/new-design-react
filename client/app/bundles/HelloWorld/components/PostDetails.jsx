import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { getPostDetails } from '../actions/index.js'
import React from 'react';
import PostsFeed from './PostsFeed';
import Post from './Post'

class PostDetails extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    post_details: PropTypes.object.isRequired

  };

  constructor(props, _railsContext) {
    super(props);
    this.render = this.render.bind(this);
  }

  componentWillMount() {
    this.props.dispatch(getPostDetails())
  }

  render() {
    console.log(this.props.post_details);
    if (!$.isEmptyObject(this.props.post_details)) {
      if(this.props.post_details.deleted_at == null && this.props.post_details.status != 'INVALID_REQUEST'){
      // if(this.props.post_details){
        return (
          <div>
            <Post ref="single_post" post={this.props.post_details}/>  
          </div>
        );
      }
      else {
        return (
          <div className="error_message">
            <h3> This post has been deleted</h3>
          </div>
          );
      }
    } 
      else {
        return <br />;
      }
    }
}

const mapStateToProps = (state) => {
  return {
    post_details: state.post_details
  };
}

const PostDetailsWithRedux = connect(mapStateToProps, null)(PostDetails);
export default PostDetailsWithRedux
