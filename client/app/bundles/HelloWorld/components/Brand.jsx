import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { getBrand, fetchBrandPosts, toggleFollowOnBrandPage, registerEmbeddedAppAuthHeaders } from '../actions/index.js'
import React from 'react';

import PostsFeed from './PostsFeed';
class Brand extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired
  };

  constructor(props, _railsContext) {
    super(props);
    this.render = this.render.bind(this);
  }

  componentDidMount(nextProps) {
    registerEmbeddedAppAuthHeaders()
    this.props.dispatch(getBrand())
  }
  componentDidUpdate() {
    console.log('updated');
      $(".image-slider").not('.slick-initialized').slick({
        dots: true,
        infinite: true,
        slidesToShow: 1,
        autoplay: true,
        autoplaySpeed: 2000
      });
  }


  render() {
    var branding_identity = "";
    // console.log("I am on render!");
    console.log(this.props.brand.brand);
    
    if (!$.isEmptyObject(this.props.brand.brand)) {
      var brand = this.props.brand.brand;
      if(brand.top_contributors && brand.top_contributors.length > 0) {
        var top_contributors = brand.top_contributors.map(function(top_contributer, index) {
        var add_hr = (<hr className="top_con_hr"/>);
        if (index != brand.top_contributors.length - 1) { add_hr = (<hr className="top_con_hr"/>); } else { add_hr = ''; }
          return (
            <div className="row remove_row_margin">
             <div className="col-md-12 margin-top-15" style={{"marginBottom":"7px"}}>
                <div className="col-md-6 nopadding">
                <div className="row">
                  <div className="col-md-3 nopadding">
                    <div>
                     <a href={"/profile/"+top_contributer.id}> <img src={top_contributer.image} className="circular-image sidebar_user_img_size"/></a>
                    </div>
                  </div>
                  <div className="col-md-8 top_user_name top_contributor_user_name">
                   <a href={"/profile/"+top_contributer.id} className="sidebar_user_text" style={{"textDecoration":"none"}}> {top_contributer.name}</a>
                   <a href={"/profile/"+top_contributer.id} className="view_profile_text sidebar_user_text" style={{"marginLeft":"0px"}}>View profile</a>
                  </div>
                </div>
                </div>
                <div className="col-md-6 nopadding">
                  <div className="row text-color top-contributers-score">
                    <div className="col-md-4 col-xs-4 nopadding top_contributor_num">
                      {top_contributer.posts}
                    </div>
                    <div className="col-md-4 col-xs-4 nopadding top_contributor_num">
                      {top_contributer.followers}
                    </div>
                    <div className="col-md-4 col-xs-4 top_contributor_num top_con_likes">
                      {top_contributer.likes}
                    </div>
                  </div>
                  <div className="row margin-top-5 lighter_text hint_user_info">
                    <div className="col-md-4 col-xs-4">
                      Posts
                    </div>
                    <div className="col-md-4 col-xs-4">
                      Followers
                    </div>
                    <div className="col-md-4 col-xs-4 top_con_likes">
                      Claps
                    </div>
                  </div>
                </div>
              </div>
                {add_hr}
            </div>
          )
        });
      }
      if(this.props.brand.current_user_is_following != undefined) {
        brand.current_user_is_following = this.props.brand.current_user_is_following;
        brand.followers_count = this.props.brand.followers_count;
      }
      // console.log(brand.current_user_is_following);
      var following = brand.current_user_is_following ? 'Unfollow' : 'Follow';
      // Content of the banner images
      var banners = "";
      if(brand.banners && brand.banners.length > 0) {
        banners = brand.banners.map((banner, index) => (<div><img src={banner} style={{"width":"100%"}} key={index}/></div>))
      }  

      if(brand.brand_logo.medium) {
        branding_identity = (
          <div className="brand_identity" >
            <img src={brand.brand_logo.medium} className="product-logo"/>
          </div>
          )
      }

      var sponsored_section = "";
      if(banners.length>0){
        sponsored_section = (
          <div className="sponsed_section">
            <div className="title_comm_header text-center">
              <span className="title_comm_text am_text"> SPONSORED CONTENT</span>
            </div>
            <div className="image-slider row padding-left-right-zero remove_row_margin" style={{"marginTop":"30px"}}>
              {banners}
            </div>
          </div>
        );
      }

      var top_contributors_section ="";
      if(top_contributors && top_contributors.length>0){
        top_contributors_section = (
          <div className="top_contributers_section" style={{"marginTop":"45px"}}>
            <div className="title_comm_header text-center" style={{"marginTop":"50px"},{"marginBottom":"20px"}}>
              <span className="title_comm_text am_text"> TOP CONTRIBUTORS</span>
            </div>
            {top_contributors}
          </div>
        )
      }

      console.log(brand);

      return (
        <div style={{"textAlign":"center"}}>
          <div className="mock-outer padding-left-right-zero">
          <div className="entire_header">
            <div className="brand_logo_alignment">
            { branding_identity }
            </div>
            <div className="align_to_left width_calc">
              <div className="row remove_row_margin">
                <div className="title_name align_to_left">
                { brand.name }
                </div>
              </div>
              <div className="row remove_row_margin">
                <div className="country_name align_to_left">
                  { brand.country_name}
                </div>
              </div>
              <div className="row remove_row_margin margin-top-15">
                <div className="title_followers align_to_left">
                  {brand.followers_count}
                  <div className="title_followers_text">
                    Followers
                  </div>
                </div>
                <button id={brand.id} className="btn follow_btn_text button-styling" style={{"outline":"none"}} onClick={() =>{this.props.toggledBrandFollow(brand.id)}}>
                  {following}
                </button>
              </div>
            </div>
            </div>
            <div className="title_comm_header text-center">
              <span className="title_comm_text am_text"> ACTIVITY METER</span>
            </div>
            <div className="activity_meter_details">
              <div className="row" style={{"marginTop":"30px"}}>
                <div className="am_sections">
                  <div className="am_num">
                  {brand.comments_count}
                  </div>
                  <div className="am_text">Comments</div>
                </div>
                <div className="am_sections">
                  <div className="am_num">
                  {brand.claps_count}
                  </div>
                  <div className="am_text">Claps</div>
                </div>
                <div className="am_sections">
                  <div className="am_num">
                  {brand.total_guru_badges}
                  </div>
                  <div className="am_text">gurus</div>
                </div>
                <div className="am_sections" style={{"borderRight":"none"}}>
                  <div className="am_num">
                  {brand.total_ambassador_badges}
                  </div>
                  <div className="am_text">Ambassadors</div>
                </div>
              </div>
            </div>
            {sponsored_section}
            {top_contributors_section}
          </div>
          <PostsFeed brandid={brand.id} feedAction={fetchBrandPosts} page={'brand_page'}/>
        </div>
      );
    } else {
      return null
    }
  }
}

function mapStateToProps(state) {
  console.log(state);
  return {
    brand: state.brand,
  };
}


const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    dispatch,
    toggledBrandFollow: (brandid) => {
      // console.log(e.currentTarget.id);
      dispatch(toggleFollowOnBrandPage(brandid));
    }
  };
}

const BrandWithRedux = connect(mapStateToProps, mapDispatchToProps)(Brand);
export default BrandWithRedux;
