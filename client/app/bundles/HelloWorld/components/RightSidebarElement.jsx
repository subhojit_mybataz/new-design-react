import PropTypes from 'prop-types';
import React from 'react';
import { connect } from "react-redux";
import { Provider } from "react-redux";
import ReactOnRails from 'react-on-rails';

class RightSidebarElement extends React.Component{

  static PropTypes={
    dispatch: PropTypes.func.isRequired,
    rSbElement: PropTypes.object.isRequired
  }

  constructor(props, _railsContext) {
    super(props);
    this.render = this.render.bind(this)
  }

  render() {
   return ( 
    <div className="rSbElement_section">
      <a href={this.props.rSbElement.link} className="rSbElement_name" target="_blank">
        {this.props.rSbElement.name}
      </a>
      <a href={this.props.rSbElement.link} className="image_section" target="_blank">
        <img className="rSbElement_image" src={this.props.rSbElement.image}/>
      </a>
      <div className="know_more_link">
        <a href={this.props.rSbElement.link} className="capsule-text more_text" target="_blank">
          Know more
        </a>
      </div>
    </div>
      );
  }
}

export default RightSidebarElement;