import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  fetchAllComments,
  togglePostsLike,
  toggleLooksLike,
  addPostsCommentViaAPI,
  addLooksCommentViaAPI,
  deleteComment,
  deletePost
} from "../actions/index.js";
import ReactDOM from "react-dom";
import React from "react";

var heights = {};
class Post extends React.Component {
  static propTypes = {
    post: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
  };

  constructor(props, _railsContext) {
    super(props);
    this.state = { showAll: false };
    this.render = this.render.bind(this);
    this.delete_post = this.delete_post.bind(this);
    this.delete_comment = this.delete_comment.bind(this);
  }

  delete_comment(e, data) {
    var comment_detail = e.target.id.split("~");
    if (confirm("Are you sure you want to delete this comment?")) {
      this.props.dispatch(
        deleteComment(comment_detail[1], comment_detail[2], current_user_id)
      );
    }
  }

  show_comment_dropdown(e, data) {
    var c_id = e.target.id.split("~");
    document
      .getElementById("delete~" + c_id[1] + "~" + c_id[2])
      .classList.toggle("show");
  }

  componentDidMount(nextProps) {
    var post_id = this.props.post.id;
    var shared_url =
      location.protocol + "//" + location.host + "/post/" + post_id;
    var id = "share_" + post_id;
    $("div[id=" + id + "]").socialLikes({
      url: shared_url,
      title: "Post"
    });
  }
  componentDidUpdate() {
    this.refs.comment.value = "";
  }

  delete_post() {
    if (confirm("Are you sure you want to delete this post?")) {
      // console.log(this.props.post.id, this.props.post.type);
      this.props.dispatch(deletePost(this.props.post.id, this.props.post.type));
    }
  }

  show_delete_dropdown(e, data) {
    var c_id = e.target.id.split("~");
    document.getElementById("delete_post~" + c_id[1]).classList.toggle("show");
  }

  handleComments(posttype, postid, comments_count) {
    this.state.showAll = !this.state.showAll;
    this.props.dispatch(fetchAllComments(posttype, postid, comments_count));
  }

  render() {
    // console.log(this.props.post);
    // console.log(this.props.all_comments);

    let comment_class = "";
    let post = this.props.post;
    // if (this.props.all_comments.post && (post.type == "Post")) {
    //   var all_comments = this.props.all_comments.post;
    // }
    // if (this.props.all_comments.look && (post.type == "Look")) {
    var all_comments = this.props.all_comments.post;
    // }
    let id = post.id;
    let post_image = "";
    let post_price = "";
    let current_user_pic = "";
    let user_pic = "";
    let delete_comment_button = "";
    let comments = "";
    // let liked = post.liked ? 'liked' : 'unliked';
    let liked = "applicable";
    let products = "";
    let brands = "";
    let comments_show = "";

    if (post.comments_count > 1) {
      comments_show = (
        <div
          className="vertical-align-child comment-portion"
          style={{ width: "100%" }}
          onClick={() => {
            this.handleComments(post.type, post.id, post.comments_count);
          }}
        >
          <div className="comment_icon" />
          <div className="post_info">{post.comments_count} Comments</div>
        </div>
      );
    } else {
      comments_show = (
        <div
          className="vertical-align-child comment-portion"
          style={{ width: "100%" }}
        >
          <div className="comment_icon" />
          <div className="post_info">{post.comments_count} Comments</div>
        </div>
      );
    }

    if (post.current_user_claps_count == 0) {
      liked = "not applicable";
    }

    if (post.products && post.products.length > 0) {
      products = post.products.map((product, index) => {
        return (
          <a key={index} className="product-info1">
            {product.name}
          </a>
        );
      });
    }

    if (post.brands && post.brands.length > 0) {
      brands = post.brands.map((brand, index) => {
        return (
          // <a className="capsule" href={brand.current_user_is_following==false ? `` : `/India/${brand.slug}`} >
          <a key={index} className="capsule" href={`/India/${brand.slug}`}>
            <div key={index} className="capsule-text">
              {brand.name}
            </div>
          </a>
        );
      });
    }

    if (post.comments && post.comments.length > 0) {
      // console.log(all_comments);
      var lastindex = post.comments.length - 1;

      if (
        all_comments &&
        all_comments.comments &&
        all_comments.comments.length > 0 &&
        all_comments.id === post.id &&
        this.state.showAll
      ) {
        comments = all_comments.comments.map((comment, index) => {
          // console.log(comment);
          if (comment.user.id == current_user_id) {
            delete_comment_button = (
              <div className="dropdown">
                <button
                  className="fa fa-ellipsis-v"
                  id={"commentsEdit~" + post.id + "~" + comment.id}
                  onClick={this.show_comment_dropdown}
                />
                <div
                  id={"delete~" + post.id + "~" + comment.id}
                  className="dropdown-content"
                  onClick={this.delete_comment}
                >
                  <p id={"comment_details~" + post.id + "~" + comment.id}>
                    Delete
                  </p>
                </div>
              </div>
            );
          } else {
            delete_comment_button = "";
          }
          if (
            window.show_animation_for_comment_post == post.id &&
            index + 1 == post.comments.length
          ) {
            comment_class = "animated fadeInUp";
            window.show_animation_for_comment_post = -1;
          }
          return (
            <div
              key={index}
              style={{ paddingBottom: "5px" }}
              id="anim_delay"
              className={"row margin-top-10 " + comment_class}
            >
              <div className="cmt_img_width cmt_img_alignment">
                <a href={"/profile/" + comment.user.id}>
                  {" "}
                  <img
                    src={comment.user.profile_pic.fixed}
                    className="circular-image sidebar_user_img_size comment_image"
                  />
                </a>
              </div>
              <div className="cmt_text_width cmt_text_margins">
                <span className="post_comment">
                  <a
                    href={"/profile/" + comment.user.id}
                    style={{ textDecoration: "none" }}
                    className="post_comment_user"
                  >
                    {comment.user.name}
                  </a>&nbsp;{" " + comment.comment}
                </span>
                <div className="post-creation-time">
                  {comment.created_at_string}
                </div>
              </div>
              <div className="cmt_menu_width cmt_btn_alignment">
                {delete_comment_button}
              </div>
            </div>
          );
        });
      } else {
        console.log(post.comments);
        comments = post.comments.map((comment, index) => {
          // console.log(comment);
          if (index == lastindex) {
            if (comment.user.id == current_user_id) {
              delete_comment_button = (
                <div className="dropdown">
                  <button
                    className="fa fa-ellipsis-v"
                    id={"commentsEdit~" + post.id + "~" + comment.id}
                    onClick={this.show_comment_dropdown}
                  />
                  <div
                    id={"delete~" + post.id + "~" + comment.id}
                    className="dropdown-content"
                    onClick={this.delete_comment}
                  >
                    <p id={"comment_details~" + post.id + "~" + comment.id}>
                      Delete
                    </p>
                  </div>
                </div>
              );
            } else {
              delete_comment_button = "";
            }
            if (
              window.show_animation_for_comment_post == post.id &&
              index + 1 == post.comments.length
            ) {
              comment_class = "animated fadeInUp";
              window.show_animation_for_comment_post = -1;
            }
            return (
              <div
                key={index}
                style={{ paddingBottom: "5px" }}
                id="anim_delay"
                className={"row margin-top-10 " + comment_class}
              >
                <div className="cmt_img_width cmt_img_alignment">
                  <a href={"/profile/" + comment.user.id}>
                    {" "}
                    <img
                      src={comment.user.profile_pic.fixed}
                      className="circular-image sidebar_user_img_size comment_image"
                    />
                  </a>
                </div>
                <div className="cmt_text_width cmt_text_margins">
                  <span className="post_comment">
                    <a
                      href={"/profile/" + comment.user.id}
                      style={{ textDecoration: "none" }}
                      className="post_comment_user"
                    >
                      {comment.user.name}
                    </a>&nbsp;{" " + comment.comment}
                  </span>
                  <div className="post-creation-time">
                    {comment.created_at_string}
                  </div>
                </div>
                <div className="cmt_menu_width cmt_btn_alignment">
                  {delete_comment_button}
                </div>
              </div>
            );
          }
        });
      }
    }
    let heart_icon = "";
    let delete_post_button = "";
    heart_icon =
      liked == "applicable"
        ? "active_clap_icon " + "cursor"
        : "deactive_clap_icon " + "cursor";
        
    // if(window.show_animation_for_like_post == post.id){
      // console.log(post);
      // console.log(this.props.clapped_postid);

    if (this.props.clapped_postid &&
      (this.props.clapped_postid==post.id) &&
      (post.current_user_claps_count < 5) &&
      (post.current_user_claps_count != 0)
    ) {
      heart_icon = heart_icon + " animated bounceIn";
      window.show_animation_for_like_post = -1;
    }

    let textbox = "";
    let comment_button = "";
    let HeartIcon = "";
    /*if(post.type == "Post") {
      
    }
    if(post.type == "Look") {
      
    }*/
    // console.log(post);

    if (userLoggedIn) {
      if (
        post.author.profile_pic &&
        post.author.profile_pic.hasOwnProperty("thumb")
      ) {
        user_pic = (
          <a href={"/profile/" + post.author_id}>
            {" "}
            <img
              src={post.author.profile_pic.thumb}
              className="circular-image"
            />
          </a>
        );
      } else {
        user_pic = (
          <a href={"/profile/" + post.author_id}>
            {" "}
            <img src="/missing.jpg" className="circular-image" />
          </a>
        );
      }

      if (
        this.props.profile_pic &&
        this.props.profile_pic.hasOwnProperty("thumb")
      ) {
        current_user_pic = (
          <a href={"/profile/" + window.current_user_id}>
            {" "}
            <img
              src={this.props.profile_pic.thumb}
              className="circular-image navbar_profile_pic comment_image"
            />
          </a>
        );
      } else {
        current_user_pic = (
          <a href={"/profile/" + window.current_user_id}>
            {" "}
            <img
              src="/missing.jpg"
              className="circular-image navbar_profile_pic comment_image"
            />
          </a>
        );
      }
      textbox = (
        <div
          className="col-md-11 col-sm-11 col-xs-11"
          style={{ paddingRight: "0px" }}
        >
          <textarea
            ref="comment"
            name="body"
            className="comment-box"
            placeholder="Write a comment"
          />
        </div>
      );
      if (post.type == "Look") {
        comment_button = (
          <div className="col-md-1 col-xs-1 cmt_btn_alignment">
            <button
              className="btn comment_btn"
              style={{ outline: "none" }}
              onClick={() => {
                this.props.addLooksComment(post.id, this.refs.comment.value);
              }}
            />
          </div>
        );

        HeartIcon = (
          <span className={heart_icon} onClick={this.props.onLooksLiked} />
        );
      }

      if (post.type == "Post") {
        comment_button = (
          <div className="col-md-1 col-xs-1 cmt_btn_alignment">
            <button
              className="btn comment_btn"
              style={{ outline: "none" }}
              onClick={() => {
                this.props.addPostsComment(post.id, this.refs.comment.value);
              }}
            />
          </div>
        );

        HeartIcon = (
          <span className={heart_icon} onClick={this.props.onPostsLiked} />
        );
      }
    } else {
      current_user_pic = "";
      textbox = (
        <div
          className="col-md-12 col-xs-12"
          style={{ backgroundColor: "#ffffff" }}
        >
          <span>
            <a href="/users/sign_in" style={{ "text-decoration": "none" }}>
              Log in
            </a>{" "}
            to like or comment.
          </span>
        </div>
      );
    }
    // console.log(window.current_user_id);
    // console.log(this.props.post.author_id);
    if (window.current_user_id == this.props.post.author_id) {
      delete_post_button = (
        <div className="dropdown">
          <button
            className="fa fa-ellipsis-v delete_ellipse"
            id={"delete_post_ellipses~" + post.id}
            onClick={this.show_delete_dropdown}
          />
          <div
            id={"delete_post~" + post.id}
            className="dropdown-content"
            onClick={this.delete_post}
          >
            <p id={"delete_post_option~" + post.id}>Delete</p>
          </div>
        </div>
      );
    }

    if (post.attachments.length > 0) {
      post_image = (
        <div className="row post_image">
          <img style={{ width: "100%" }} src={post.attachments[0].fixed} />
          <br />
        </div>
      );
    }

    if (post.price) {
      post_price = (
        <div className="product-info" style={{ width: "100%" }}>
          <a className="post-price">Rs. {post.price}</a>
        </div>
      );
    }

    var post_type;
    if (post.type == "Look") {
      post_type = "looks";
    } else {
      post_type = "posts";
    }
    // console.log(post);
    return (
      <div className="mock-outer text-center">
        <div className="row" style={{ textAlign: "left" }}>
          <div className="post-nopadding" style={{ display: "inline-block" }}>
            {user_pic}
          </div>
          <div className="post_user_name_section">
            <a className="post_user_name" href={"/profile/" + post.author_id}>
              {post.author.name}
            </a>
            <div className="post-creation-time">{post.created_at_string}</div>
          </div>
          <div className="delete_post_btn">{delete_post_button}</div>
        </div>
        <div className="row description">
          {post.description}
          <br />
        </div>
        <a href={"/" + post_type + "/" + post.id}>{post_image}</a>

        <br />
        <div className="row">
          <div
            key={this.props.postid}
            className="post-nopadding text-left align_to_left"
          >
            {brands}
          </div>
          <div
            className="post-nopadding vertical-align-parent text-center align_to_right bottom-space"
            style={{ height: "32px" }}
          >
            {products}
            {post_price}
          </div>
        </div>
        {/*<br/>*/}
        <hr className="post_hr" />
        <div className="row" style={{ paddingTop: "8px" }}>
          <div style={({ paddingTop: "10px" }, { paddingBottom: "10px" })}>
            <div className="col-xs-4 post-nopadding text-weight-style vertical-align-parent text-left">
              <div className="vertical-align-child" style={{ width: "100%" }}>
                {HeartIcon}
                <span className="post_claps">
                  &nbsp;{post.claps_count}&nbsp;Claps
                </span>
              </div>
            </div>
            <div className="col-xs-4 post-nopadding vertical-align-parent">
              {comments_show}
            </div>
            <div className="col-xs-4 post-nopadding text-weight-style vertical-align-parent text-right">
              <div className="vertical-align-child" style={{ width: "100%" }}>
                <div
                  id={"share_" + id}
                  className="social-likes social-likes_single"
                  data-counters="no"
                >
                  <div className="facebook" title="Share link on Facebook">
                    Facebook
                  </div>
                  <div className="twitter" title="Share link on Twitter">
                    Twitter
                  </div>
                  <div className="plusone" title="Share link on Google+">
                    Google+
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr className="post_hr" />
        <div className="row">
          <div style={{ textAlign: "left" }}>{comments}</div>
          <div className="row margin-top-15">
            <div className=" comment_current_user_image cmt_img_alignment">
              {current_user_pic}
            </div>
            <div className="row comment_area" style={{ marginLeft: "6px" }}>
              {textbox}
              {comment_button}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  // console.log(state);
  return {
    profile_pic: state.profile_pic,
    all_comments: state.all_comments,
    clapped_postid: state.clapped_postid
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  var that = this;
  // console.log(ownProps);
  return {
    dispatch,
    onPostsLiked: () => {
      dispatch(togglePostsLike(ownProps.post.id));
    },
    onLooksLiked: () => {
      dispatch(toggleLooksLike(ownProps.post.id));
    },
    addLooksComment: (postid, comment) => {
      if (comment.trim().length > 0) {
        dispatch(addLooksCommentViaAPI(postid, comment));
      }
    },
    addPostsComment: (postid, comment) => {
      if (comment.trim().length > 0) {
        dispatch(addPostsCommentViaAPI(postid, comment));
      }
    }
  };
};

const PostWithRedux = connect(mapStateToProps, mapDispatchToProps)(Post);
export default PostWithRedux;
