import PropTypes from 'prop-types';
import React from 'react';
import { fetchSidebar, fetchTopUsers } from '../actions/index.js'
import { Component } from 'react';
import { Provider } from "react-redux";
import { connect } from "react-redux";
import ReactOnRails from 'react-on-rails';
import TrendingBrand from './TrendingBrand';
import TrendingUser from './TrendingUser';

class SideBar extends Component{

  static PropTypes = {
    dispatch: PropTypes.func.isRequired
  }

  constructor(props, _railsContext) {
    super(props);
    this.render = this.render.bind(this);
    this.handleScroll = this.handleScroll.bind(this);
  }

  componentWillMount(nextProps) {
    this.props.dispatch(fetchSidebar());
    this.props.dispatch(fetchTopUsers());
  }

  handleScroll = (e) => {
    const ele = document.getElementById("inside_sidebar");
    var event = e,
    d = -event.deltaY || -event.detail ;

    ele.scrollTop += ( d < 0 ? 1 : -1 ) * 30;
    e.preventDefault();
  }

  componentDidUpdate() {
    if($(window).width()<768){
      if(this.refs.sideMenu.classList.contains("show")){
        $('#carousel_brand').carousel();
        $('#carousel_user').carousel();
      }
    }else if($(window).width()>=768){
      $('#carousel_brand').carousel({
        interval: 10000
        });
      $('#carousel_user').carousel({
        interval: 15000
        });
    }

  }

  fetchSlides(limit, arr_list){
    var grouped_list =[];
    var trending_elems = [];
    // checking for limit 4 since it is for trending users where we accept odd number of users
    var length = limit==4 || arr_list.length%2==0 ? arr_list.length : arr_list.length-1;
    for(var i=0; i<length; i=i+limit)
    {
      var item = [];
      for(var j=i;j<(i+limit)&& j<length; j++)
      {
        item.push(arr_list[j]);
      }
      grouped_list.push(item);
    }

    for(var k=0;k<grouped_list.length; k++){
      var sliders = "";
      if(k==0){
        sliders= (
          <div className="item active">
            {grouped_list[k]}
          </div>
          );
      }else {
        sliders = (
          <div className="item">
            {grouped_list[k]}
          </div>
          )
      }
      trending_elems.push(sliders);
    }
    return trending_elems;

  }

  render() {
    var brands = "";
    var users = "";
    var sidebar = this.props.sidebar.sidebar;
    // console.log(this.props.sidebar.sidebar);
    var user_slider="";
    var user_items=[];
    var brand_slider ="";
    var brand_items = [];
    /*if(this.props.sidebar.sidebar.trendingBrand === 'undefined'){
      brands = (<br />);
    }
    else if(typeof this.props.sidebar.sidebar.trendingUser == 'undefined') {
      users = (<br />);
    }
    else{*/
      if(this.props.sidebar.sidebar.trendingBrand && this.props.sidebar.sidebar.trendingBrand.length > 0) {
        brands = this.props.sidebar.sidebar.trendingBrand.map((post,index) =>{
          // console.log(post);
          return (<TrendingBrand key={index} brand_post={post} index={index} loc_sidebar={true}/>);
        });
      }
      if(this.props.sidebar.sidebar.trendingUsers && this.props.sidebar.sidebar.trendingUsers.length > 0) {
        users = this.props.sidebar.sidebar.trendingUsers.map((user, index) => {
          // console.log(user);
          return (<TrendingUser key={index} user={user} />);
        });
      }
    /*}*/

    user_items = this.fetchSlides(4,users);
    brand_items = this.fetchSlides(2,brands);

    user_slider = (
    <div id="carousel_user" className="carousel slide">
      <div className="carousel-inner" role="listbox">
        {user_items}
      </div>
    </div>
      );

    brand_slider =(
    <div id="carousel_brand" className="carousel slide">
      <div className="carousel-inner" role="listbox">
        {brand_items}
      </div>
    </div>
      );
    

    return (
      <div className="sideBar sidebar_common" id="sideMenu" ref="sideMenu">
        <div className="scrollable_inside" id="inside_sidebar" onWheel = {(e) => {this.handleScroll(e)}}>
          <div className="sidebar_component brand_rows_height">
            <div className = "t_headers padding-top-10">
              <div className="fire_icon_trending"></div>
              <p className ="trending_heading">Trending Brands</p>
            </div>
            <hr className="tb_line trending_line"/>
            <div className="trendindBrands">
              {brand_slider}
            </div>
          </div>
          <div className ="sidebar_component user_rows_height">
            <div className = "t_headers padding-top-10">
              <div className="fire_icon_trending"></div>
              <p className ="trending_heading">Top Users</p>
            </div>
            <hr className="tb_line trending_line"/>
            <div className="trending_users">
            {user_slider}
            </div>
          </div>
        </div>
      </div>
    );

  }

}

function mapStateToProps(state) {
  // console.log(state);
  return {
    sidebar: state
  };
}


const SideBarWithRedux = connect(mapStateToProps)(SideBar);
export default SideBarWithRedux
// Query top brands from active admin and 
// fetch brand_name, total_likes, total_comments, images.
// Query top userd from active admin
//Trending brands
// for(brand.id and country.id from brand_country table)
// <trendingBrand brand id flag (country_id)>
// end
// Trending users
// for(user_id)
// <TrendingUSers user_id>
// end
// make sure of the size of screen of size of this sidebar in css.
// ensure that it is closed when x is clicked.