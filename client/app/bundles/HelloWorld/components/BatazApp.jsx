import PropTypes from 'prop-types';
import React from 'react';
import {Component} from 'react';
import { connect } from "react-redux";
import { fetchPosts} from '../actions/index.js'
import PostsFeed from './PostsFeed';
import NewPost from './NewPost';
import UsersFeed from './UsersFeed';


class BatazApp extends Component {

  static propTypes = {
    dispatch: PropTypes.func.isRequired
  }

  /**
   * @param props - Comes from your rails view.
   * @param _railsContext - Comes from React on Rails
   */
  constructor(props, _railsContext) {
    super(props);
    this.render = this.render.bind(this);
    // How to set initial state in ES6 class syntax
    // https://facebook.github.io/react/docs/reusable-components.html#es6-classes
    //this.state = { name: this.props.name, isInfiniteLoading: true };
  }

  componentWillUnmount() {
    // Update the flag as main react component is being rendered more than once
    window.typeahead_init=false;
  }

  render() {
    return (
        <div>
          <NewPost />
          <PostsFeed feedAction={fetchPosts}/>
        </div>
    );
  }
}

const BatazAppWithRedux = connect()(BatazApp);
export default BatazAppWithRedux
