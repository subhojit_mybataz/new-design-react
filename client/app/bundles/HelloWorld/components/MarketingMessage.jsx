import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { Provider } from "react-redux";
import { Component } from 'react';
import ReactDOM from 'react-dom';
import React from 'react';
import Autolinker from 'autolinker';

class MarketingMessage extends React.Component {
  static propTypes = {
    marketingMesg: PropTypes.object.isRequired
  }

  constructor(props, _railsContext) {
    super(props);
    this.render = this.render.bind(this);
    this.convertToUrl = this.convertToUrl.bind(this);
  }

  convertToUrl(){
    var autolinker = new Autolinker();
    return (autolinker.link(this.props.marketingMesg.message));
  }

  render() {
    var text = this.convertToUrl();
    return (
      <div className="mMessage">
        <img src={bataz_login_logo} className="mm_image vary_size_image"/>
        <div className="mock-outer mark_message">
          <div className="vary_allignment" dangerouslySetInnerHTML={{__html: text}} />
        </div>
      </div>
      );
    }
}

const MarketingMessageWithRedux = connect()(MarketingMessage);
export default MarketingMessage;