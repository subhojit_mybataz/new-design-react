import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { getPostDetails, getFollowNotificationDetails, toggledFollowNotificationPage } from '../actions/index.js'
import React from 'react';
import PostsFeed from './PostsFeed';
import Post from './Post'
import FollowUser from './FollowUser'


class FollowNotifications extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    follow_notification_details: PropTypes.object.isRequired

  };

  constructor(props, _railsContext) {
    super(props);
    this.render = this.render.bind(this);
  }

  componentWillMount() {
    this.props.dispatch(getFollowNotificationDetails())
  }


  render() {

    if (!$.isEmptyObject(this.props.follow_notification_details)) {

      var notificationsList = this.props.follow_notification_details.map((follow_notif, index) => {
        return (
              <FollowUser item={follow_notif} notification_sentence_part="you" />
          );
      });

      return (
        <div className="container">
           {notificationsList} 
        </div>
      );
    } else {
        if(this.props.follow_notifications_request_completed==false)
          return(<div className="center-align" style={{"display":"block"}}>
                  <img id="loading_pic" name="loading_pic" ref='loading_pic' className="load-icon-dimensions" src={loading_pic}/>
                </div>
          );
        else  
          return (<div className="no-notifications">No Notifications</div>)
    }
  }
}


const mapStateToProps = (state) => {
  return {
    follow_notification_details: state.follow_notification_details,
    follow_notifications_request_completed: state.follow_notifications_request_completed
  };
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    dispatch,
    toggledFollow: (id) => {
      dispatch(toggledFollowNotificationPage(id));
    }
  };
}


const FollowNotificationDetailsWithRedux = connect(mapStateToProps, mapDispatchToProps)(FollowNotifications);
export default FollowNotificationDetailsWithRedux
