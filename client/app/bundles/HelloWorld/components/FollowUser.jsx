import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { getPostDetails, getFollowNotificationDetails, toggledFollowNotificationPage } from '../actions/index.js'
import React from 'react';
import PostsFeed from './PostsFeed';
import Post from './Post'

class FollowUser extends React.Component {

  constructor(props, _railsContext) {
    super(props);
    this.render = this.render.bind(this);
  }

  render() {

    var item=this.props.item;
    console.log(item);
    var following = item.current_user_is_following ? 'Unfollow' : 'Follow';
    var notification_sentence_part = this.props.notification_sentence_part;
    var timestamp_component = "";
    var notif_container = this.props.loc == "search"? "post" : "col-md-6 col-md-offset-3";
    
    if (item.created_at_string) {
      timestamp_component = (
        <span className="notification-timestamp"><i className="fa fa-clock-o" aria-hidden="true"></i> {item.created_at_string}</span>
      )
    }

    return (

      <div className="row remove_row_margin">     
      <div className={notif_container+" alert notification-unread"}>
        <div className="row" style={{"padding":"10px 0px"}}>
          <div className="col-xs-3 col-sm-2  text-center custom-follow-sm">
            <img className="circular-image" src={item.author.profile_pic.fixed}/>
          </div>
          <div className="col-xs-9 col-sm-6  notification-message custom-follow-sm">
            <a href={'/profile/' + item.author_id}>{item.author.name}</a>&nbsp;{item.type} {notification_sentence_part} <br/>
            { timestamp_component }<br/>
          </div>
          <div className="col-xs-12 col-sm-4">
            <button className="btn btn-block btn-warning" onClick={() => this.props.toggledFollow(item.owner_id)}> {following} </button>
          </div>
          
        </div>
      </div>
    </div>
      

    );
  }
}



function mapStateToProps(state) {
  return {
    follow_notification_details: state.follow_notification_details
  };
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    dispatch,
    toggledFollow: (id) => {
      dispatch(toggledFollowNotificationPage(id));
    }
  };
}

const FollowUserWithRedux = connect(mapStateToProps,mapDispatchToProps)(FollowUser);
export default FollowUserWithRedux
