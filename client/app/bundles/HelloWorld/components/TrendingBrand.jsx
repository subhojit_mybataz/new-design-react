// send flag and brand id and (country_id)

// write the variables in the way as to organise the content according to 2 / 4 images.

import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { Provider } from "react-redux";
import { Component } from 'react';
import ReactDOM from 'react-dom';
import React from 'react';

class TrendingBrand extends React.Component {
  static propTypes = {
    brand_post: PropTypes.object.isRequired,
    loc_sidebar: PropTypes.bool.isRequired
  }

  constructor(props, _railsContext) {
    super(props);
    this.render = this.render.bind(this);
  }

  render() {
    var brand_row = this.props.brand_post;
    console.log(brand_row);
    var images = "";
    var line = "";
    var brand_info = "";
    var rearrange_style=""
    if(this.props.loc_sidebar)
    {
      brand_info = "brand_info"
      rearrange_style = "rearrange_style"
    }else
    {
      brand_info ="always_inline_post";
    }

    var brand_header = (
      <div key={brand_row.id} className={"paddingsTB "+rearrange_style} style={{"paddingTop":"0px"}}>
          <a className={"capsule"} style={{"marginRight":"10px"}} href={`/India/${brand_row.slug}`}>
            <div className="capsule-text">{brand_row.name}</div>
          </a>
          <div className={brand_info}>
            <p className="trending_brand_info" >{brand_row.comments_count} &nbsp;<i className="fa fa-pencil-square-o icon-b-heart" aria-hidden="true"></i></p>
            <p className="trending_brand_info" >{brand_row.claps_count}&nbsp;<i className="far fa-heart icon-b-heart" aria-hidden="true"></i></p>
          </div>
      </div>
    );

    images = brand_row.stories.map((story, index) => {
      if(story.attachments && story.attachments.length > 0) {
        if(story.type == "Look") {
          var imgs = (
          <div key={story.id} className="col-md-5 col-xs-5 tb_brand_rows">
            <a href={`/looks/${story.id}`}><img src={story.attachments[0].fixed} className="tb_brand brand-image-size"/></a>
          </div>);
          return imgs;
        }
        if(story.type == "Post") {
          var imgs = (
          <div key={story.id} className="col-md-5 col-xs-5 tb_brand_rows">
            <a href={`/posts/${story.id}`}><img src={story.attachments[0].fixed} className="tb_brand brand-image-size"/></a>
          </div>);
          return imgs;
        }
      }
    });

    if(this.props.loc_sidebar){
    if(this.props.index%2 == 0){
      line = (<hr className="tb_line"/>);
    }
    // console.log(this.props.loc_sidebar);
    return (
      <div key={this.props.index} className="brand_rows">
        {brand_header}
      <div key={brand_row.id} className="row paddingsLR">
        {images}
      </div>
      {line}
      </div>
      );
    }else
    {
    return (
      <div key={brand_row.id} className="trending_brand_post mock-outer" style={{"textAlign":"left"}}>
        <p className="trending_heading trendb_heading"> <div className="fire_icon_trending"></div>Trending</p>
          {brand_header}
        <div key={this.props.index} className="row">
          {images[0]}
          {images[1]}
        </div>
        <br/>
        <div key={this.props.index} className="row">
          {images[2]}
          {images[3]}
        </div>
      </div>
      );
    }
  }
}

const TrendingBrandWithRedux = connect()(TrendingBrand);
export default TrendingBrandWithRedux