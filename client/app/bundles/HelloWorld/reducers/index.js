import { combineReducers } from "redux";
import { RECIEVE_RSB_ELEMENTS, RECEIVE_ALL_COMMENTS, PROFILE_PIC, RECIEVE_SIDEBAR, RECIEVE_TOP_USERS, CREATE_POST_PROGRESS, RECEIVE_POSTS,RECEIVE_USERS,RECEIVE_BRAND, SEND_POSTS, TOGGLE_LIKE, RECEIVE_PROFILE, RECEIVE_POST_DETAILS, TOGGLE_FOLLOW_USER,TOGGLE_FOLLOW_USER_NOTIFICATION ,TOGGLE_FOLLOW_BRAND, COMMENT_ADDED, TURBO_INIT, NEW_POST_USER, NEW_POST_CREATE, AUTO_COMPLETE, NOTIFICATION_COUNT,NOTIFICATION_DETAILS,FOLLOW_NOTIFICATION_DETAILS, SEARCH_RESULTS,RESET_NEW_POST_FLAG,RECEIVE_EMPTY_SEARCH,COMMENT_DELETED, DELETED_POST, MARK_ALL_NOTIFICATION_AS_READ } from '../actions/index'
let defaultState = { rightSidebarElements: [], count_notification:0, brandid: '', postslength:0,  all_comments:{}, profile_pic: '',posts: [], sidebar: {}, isInfiniteLoading: false, profile: {}, post_details: {}, new_post: {}, notification_count: {}, notification_details: [],follow_notification_details: [] ,auto_complete_details: {}, userLoggedIn: userLoggedIn, brand: {}, offset: 0, page: 1, no_follow: false,follow_feed_exhausted: false,new_post_created:false,feed_exhausted_at: -1,user_search_results: [],empty_search_result: false,follow_notifications_request_completed: false,notifications_request_completed: false};

function dummyReducer(state = defaultState, action) {
  // console.log(state);
  // console.log(action);
  switch (action.type) {
    case CREATE_POST_PROGRESS:
      var newState = Object.assign({}, state);
      newState.new_post_creation_in_progress = action.progress;
      return newState;
    case RECEIVE_POSTS:
      // console.log(state.posts);
      // console.log(action);
      var newState = Object.assign({}, state);
      // console.log(newState);
      // console.log(action);
      newState.posts = state.posts.concat(action.posts);
      // newState.offset = newState.offset + 1;
      if(action.posts[0] && action.posts[0].brands[0]) {
        newState.brandid = action.posts[0].brands[0].id;
        // console.log(newState);
      }
      // newState.postslength = newState.postslength + action.posts.length;
      if(newState.postslength <= action.all_post_feed_exhausted) {
        newState.page = newState.page + 1;
        newState.postslength = newState.postslength + action.posts.length;
        return newState;
      }
      if(action.no_posts==true)
        newState.no_follow=true;
      if(action.follow_feed_exhausted==true){
          newState.follow_feed_exhausted=true;
          newState.feed_exhausted_at=newState.page;
      }
      newState.all_post_feed_exhausted = action.all_post_feed_exhausted;
      // console.log(newState);
      // return newState;
    case RECEIVE_USERS:
      var newState = Object.assign({}, state);
      newState.follow_notification_details = state.follow_notification_details.concat(action.user_search_results);
      return newState;
    case RECEIVE_EMPTY_SEARCH:
      var newState = Object.assign({}, state);
      newState.empty_search_result = action.empty_search_result;
      return newState;
    case RECIEVE_SIDEBAR:
      // console.log(state);
      // console.log(action);
      var newState = Object.assign({}, state);
      var sidebar = Object.assign({}, state.sidebar);
      sidebar.trendingBrand = action.sidebar.brands;
      // sidebar.trendingUser = action.sidebar.user_row;
      newState.sidebar = sidebar;
      return newState;
    case RECIEVE_TOP_USERS:
      // console.log(state);
      // console.log(action);
      var newState = Object.assign({}, state);
      var sidebar = Object.assign({}, state.sidebar);
      // sidebar.trendingBrand = action.sidebar.brands;
      sidebar.trendingUsers = action.users.users;
      newState.sidebar = sidebar;
      return newState;
    case RECEIVE_ALL_COMMENTS:
      // console.log(state);
      // console.log(action);
      var newState = Object.assign({}, state);
      var all_comments = Object.assign({}, state.all_comments);
      all_comments.post = action.allcomments;
      newState.all_comments = all_comments;
      return newState;
    case RECIEVE_RSB_ELEMENTS:
      var newState = Object.assign({}, state);
      var bataz_elements = Object.assign({}, state.rightSidebarElements);
      bataz_elements = action.elements;
      newState.rightSidebarElements = bataz_elements;
      return newState;
    case DELETED_POST:
      var newState = Object.assign({}, state);
      var newPosts = state.posts.filter(post => {
        if (post.id == action.id) {
          return false;
        }
        return true;
      });
      newState.posts = newPosts;
      return newState;
    case TOGGLE_LIKE:
      // console.log(state);
      var newState = Object.assign({}, state);
      // console.log(action);
      newState.clapped_postid = action.id;
      if(state.post_details.id && state.post_details.current_user_claps_count) {
        // console.log(state);
        var newPostDetails = Object.assign({}, state.post_details);
        // newPostDetails.comments.push(comment[0]);
        // console.log(newPostDetails);
        if(newPostDetails.current_user_claps_count < 5) {
          newPostDetails.claps_count+=1;
          newPostDetails.current_user_claps_count+=1;
        }
        newState.post_details = newPostDetails;
        return newState;
      }
      else {
        if (state.posts.length != 0) {
          var newPosts = state.posts.map(post => {
          // console.log(post);
          // console.log(action);
          if (post.id == action.id) {
            // post.liked = !post.liked;
            // if(post.claps_count < 5)
            if(post.current_user_claps_count < 5) {
              post.claps_count+=1;
              post.current_user_claps_count+=1;
            }
          }
          return post
        });
        newState.posts = newPosts;
        return newState;
      } else {
        var newState = Object.assign({}, state);
        var newPostDetails = Object.assign({}, state.post_details);
        newPostDetails.liked = !state.post_details.liked
        if(newPostDetails.liked)
          newPostDetails.no_of_likes+=1;
        else
          newPostDetails.no_of_likes-=1;
        newState.post_details = newPostDetails
        return newState;
      }
    }
    case PROFILE_PIC:
      var newState = Object.assign({},state);
      newState.profile_pic = action.profile_pic;
      return newState;
    case RECEIVE_PROFILE:
      var newState = Object.assign({}, state);
      newState.profile = action.profile;
      return newState;
    case RESET_NEW_POST_FLAG:
      var newState = Object.assign({}, state);
      newState.new_post_created = !state.new_post_created;
      return newState;
    case SEARCH_RESULTS:
      var newState = Object.assign({}, state);
      newState.posts = action.search_result.posts;
      return newState;
    case RECEIVE_POST_DETAILS:
      var newState = Object.assign({}, state);
      console.log(action);
      // var newState = Object.assign({}, state);
      // console.log(action.post_details.look);
      if(action.post_details.post) {
        newState.posts = [action.post_details.post];
        newState.post_details = action.post_details.post;
      }
      if(action.post_details.look) {
        newState.posts = [action.post_details.look];
        newState.post_details = action.post_details.look;
      }
      if(action.post_details.status) {
        newState.post_details = action.post_details;
      }
      // console.log(newState.posts);
      return newState;
    case NEW_POST_USER:
      var newState = Object.assign({}, state);
      newState.new_post = action.new_post_user.post;
      return newState;
    case NOTIFICATION_COUNT:
      var newState = Object.assign({}, state);
      newState.notification_count = action.notification_count;
      return newState;
    case NOTIFICATION_DETAILS:
      // console.log(state);
      // console.log(action);
      var newState = Object.assign({}, state);
      var newStateNotify = Object.assign([], state.notification_details);

      /*var newState = Object.assign({}, state);
      // console.log(newState);
      console.log(action);
      newState.posts = state.posts.concat(action.posts);
      newState.offset = newState.offset + 1;
      // newState.postslength = newState.postslength + action.posts.length;
      if(newState.postslength < action.all_post_feed_exhausted) {
        newState.page = newState.page + 1;
        newState.postslength = newState.postslength + action.posts.length;
        return newState;
      }*/
      console.log(action.notification_details.notifications.length);

      if(action.notification_details.notifications.length == 0) {
        newState.notifications_request_completed=true;
      }
      else {
        newState.notifications_request_completed=false;
      }

      if(newState.count_notification == 0) {
        newState.count_notification = action.notification_details.notifications.length;
      }
      else {
        newState.count_notification+= action.notification_details.notifications.length;
      }

      // if(action.notification_details.size < action.notification_details.total) {
        newState.page = newState.page + 1;
        newStateNotify=action.notification_details.notifications.map(a => Object.assign({}, a));
        console.log(newStateNotify);
        newState.notification_details=newStateNotify;
        // console.log(action.notification_details.notifications.length);
        // if(action.notification_details.notifications.length == 0) {
        //   newState.notifications_request_completed=true;
        // }
        // else {
        //   newState.notifications_request_completed=false;
        // }
      // }
      return newState;
    case FOLLOW_NOTIFICATION_DETAILS:
      var newState = Object.assign({}, state);
      console.log(action);
      console.log(newState);
      newState.notification_details=action.notification_details.notifications;
      newState.follow_notifications_request_completed=true;
      return newState;
    case AUTO_COMPLETE:
      var newState = Object.assign({}, state);
      var new_auto_complete_details = newState.auto_complete_details;
      var auto_complete_json = {};
      auto_complete_json['brand_json'] = action.auto_complete_details.brand;
      auto_complete_json['product_json'] = action.auto_complete_details.product;
      new_auto_complete_details = auto_complete_json;
      newState.auto_complete_details = new_auto_complete_details;
      return newState;
    case NEW_POST_CREATE:
      var newState = Object.assign({}, state);
      // console.log(newState);
      // console.log(action);
      var existingPosts = [];
      existingPosts = Object.assign([], state.posts);
      existingPosts.unshift(action.new_post_create.data.post);
      newState.posts = existingPosts;
      newState.new_post_created=true
      return newState;
    case RECEIVE_BRAND:
      // console.log(state);
      // console.log(action);
      var newState = Object.assign({}, state);
      newState.brand = action.brand;
      return newState;
    case TOGGLE_FOLLOW_USER:
      var newState = Object.assign({}, state);
      var newProfile = Object.assign({}, state.profile);
      newProfile.data.profile_user_following_current_user = !newProfile.data.profile_user_following_current_user;
      if(newProfile.data.profile_user_following_current_user == true) {
        newProfile.data.followers+=1;
      }
      else {
        newProfile.data.followers-=1;
      }
      // newProfile.following = !newProfile.following
      newState.profile = newProfile;
      return newState;
    case TOGGLE_FOLLOW_USER_NOTIFICATION:
      var newState = Object.assign({}, state);
      var newFollowNotificationDetails = state.follow_notification_details.map(notif => {
            if(notif.owner_id==action.id)
              notif.following=!notif.following;
            return notif;
      });
      newState.follow_notification_details = newFollowNotificationDetails;
      return newState;
    case MARK_ALL_NOTIFICATION_AS_READ:
      var newState = Object.assign({},state);
      var newNotifications=state.notification_details.map(notif =>{
              notif.read = true;
            return notif;
      });
      newState.notification_count = 0;
      newState.notification_details = newNotifications;
      return newState;
    case TOGGLE_FOLLOW_BRAND:
      var newState = Object.assign({}, state);
      var newBrand = Object.assign({}, state.brand);
      // console.log(action);
      // console.log(newBrand);
      // console.log(newState);
      newBrand.current_user_is_following = !newState.brand.brand.current_user_is_following;
      if(newBrand.current_user_is_following) {
        newBrand.followers_count = newState.brand.brand.followers_count + 1;
      }
      else {
        newBrand.followers_count = newState.brand.brand.followers_count - 1;
      }
      newState.brand = newBrand;
      // console.log(newState);
      return newState;
    case COMMENT_ADDED:
      // console.log(state.posts.length);
      if (state.posts.length != 0) {
        var comment = action.comment;
        var postid = action.postid;
        // var comment_id = action
        // console.log(state);
        var newPosts = state.posts.map(post => {
          // console.log(post);
          // console.log(action);
          var j = comment.length - 1;
          if (post.id == postid) {
            post.comments.push(comment[j]);
            post.comments_count = post.comments_count + 1;
          }
          return post;
        });
        var newState = Object.assign({}, state);
        // console.log(newState);
        // console.log(state);
        // console.log(action);
        if(state.post_details.comments && action.comment.length) {
          var newPostDetails = Object.assign({}, state.post_details);
          newPostDetails.comments.push(comment[0]);
          newState.post_details = newPostDetails;
        }
        newState.posts = newPosts;
        return newState;
      } else {
        var comment = action.comment;
        var newState = Object.assign({}, state);
        var newPostDetails = Object.assign({}, state.post_details);
        newPostDetails.comments.push(comment)
        newState.post_details = newPostDetails
        return newState;
      }
     case COMMENT_DELETED:
      if (state.posts.length != 0) {
        //this is for comments in the feed
        var newPosts = state.posts.map(post => {
          if (post.id == action.post_id) {
            var comments = post.comments.filter(comment=>{
              return checkComment(comment.id,action.comment_id);
            });
            post.comments = comments;
          }
          return post;
        });
        var newState = Object.assign({}, state);
        newState.posts = newPosts;
        return newState;
      } else {
        //this is for the individual post page
        var newState = Object.assign({}, state);
        var newPostDetails = Object.assign({}, state.post_details);
        var comments = newPostDetails.comments.filter(comment=>{
             return checkComment(comment.id,action.comment_id);
            });
        newPostDetails.comments = comments;
        newState.post_details = newPostDetails;
        return newState;
      } 
    case TURBO_INIT:
      return defaultState;
    default:
      return state;
  }
}

function checkComment(comment_id, action_id) {
  if(comment_id == action_id)
    return false;
  else
    return true;
}
/*
main reducers no need to combine right now as there is only a single reducer
export const reducers = combineReducers({
  dummyReducer
});
*/
export const reducers = dummyReducer;
