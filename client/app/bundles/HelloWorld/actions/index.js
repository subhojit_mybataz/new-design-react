import { store } from "../../store";

export const RECEIVE_POSTS = "RECEIVE_POSTS";
export const RECEIVE_PROFILE = "RECEIVE_PROFILE";
// export const USER_FEED = 'USER_FEED'
export const RECEIVE_POST_DETAILS = "RECEIVE_POST_DETAILS";
export const RECEIVE_BRAND = "RECEIVE_BRAND";
export const TOGGLE_LIKE = "TOGGLE_LIKE";
export const TOGGLE_FOLLOW_USER = "TOGGLE_FOLLOW_USER";
export const TOGGLE_FOLLOW_USER_NOTIFICATION = "TOGGLE_FOLLOW_USER_NOTIFICATION";
export const TOGGLE_FOLLOW_BRAND = "TOGGLE_FOLLOW_BRAND";
export const GET_PROFILE = "GET_PROFILE";
export const ADD_COMMENT = "ADD_COMMENT";
export const COMMENT_ADDED = "COMMENT_ADDED";
export const TURBO_INIT = "TURBO_INIT";
export const NEW_POST_USER = "NEW_POST_USER";
export const NEW_POST_CREATE = "NEW_POST_CREATE";
export const AUTO_COMPLETE = "AUTO_COMPLETE";
export const NOTIFICATION_COUNT = "NOTIFICATION_COUNT";
export const NOTIFICATION_DETAILS = "NOTIFICATION_DETAILS";
export const FOLLOW_NOTIFICATION_DETAILS = "FOLLOW_NOTIFICATION_DETAILS";
export const RESET_NEW_POST_FLAG = "RESET_NEW_POST_FLAG";
export const SEARCH_RESULTS = "SEARCH_RESULTS";
export const RECEIVE_USERS = "RECEIVE_USERS";
export const RECEIVE_EMPTY_SEARCH = "RECEIVE_EMPTY_SEARCH";
export const DELETED_POST = "DELETED_POST";
export const COMMENT_DELETED = "COMMENT_DELETED";
export const CREATE_POST_PROGRESS = "CREATE_POST_PROGRESS";
export const MARK_ALL_NOTIFICATION_AS_READ = "MARK_ALL_NOTIFICATION_AS_READ";
export const RECIEVE_SIDEBAR = "RECIEVE_SIDEBAR";
export const RECIEVE_TOP_USERS = "RECIEVE_TOP_USERS";
export const RECEIVE_ALL_COMMENTS = "RECEIVE_ALL_COMMENTS";
export const PROFILE_PIC = "PROFILE_PIC";
export const RECIEVE_RSB_ELEMENTS = "RECIEVE_RSB_ELEMENTS";

function receivePosts(posts, no_posts, follow_feed_exhausted, all_post_feed_exhausted) {
  let resp = {
    type: RECEIVE_POSTS,
    posts: posts
  };
  if (no_posts) {
    resp.no_posts = true;
  }
  if (follow_feed_exhausted) {
    resp.follow_feed_exhausted = true;
  }
  resp.all_post_feed_exhausted = all_post_feed_exhausted;
  return resp;
}

const receiveUsers = user_search_results => {
  return {
    type: RECEIVE_USERS,
    user_search_results
  };
};

const receiveAllComments = allcomments => {
  return {
    type: RECEIVE_ALL_COMMENTS,
    allcomments
  };
};

const receiveEmptySearch = empty_search_result => {
  return {
    type: RECEIVE_EMPTY_SEARCH,
    empty_search_result
  };
};

const receiveProfile = profile => {
  return {
    type: RECEIVE_PROFILE,
    profile
  };
};

/*const receiveUserFeed = (userfeed) => {
  return {
    type: USER_FEED,
    userfeed
  };
}*/

const resetNewPostFlag = () => {
  return {
    type: RESET_NEW_POST_FLAG
  };
};

const receiveSearchResults = search_result => {
  return {
    type: SEARCH_RESULTS,
    search_result
  };
};

const createPostProgress = progress => {
  return {
    type: CREATE_POST_PROGRESS,
    progress
  };
};

const receiveNewPostUserDetails = new_post_user => {
  return {
    type: NEW_POST_USER,
    new_post_user
  };
};

const receiveNotificationCount = notification_count => {
  return {
    type: NOTIFICATION_COUNT,
    notification_count
  };
};

const receiveNotificationDetails = notification_details => {
  return {
    type: NOTIFICATION_DETAILS,
    notification_details
  };
};

const receiveFollowNotificationDetails = follow_notification_details => {
  return {
    type: FOLLOW_NOTIFICATION_DETAILS,
    follow_notification_details
  };
};

const receiveAutoCompleteDetails = auto_complete_details => {
  return {
    type: AUTO_COMPLETE,
    auto_complete_details
  };
};

const receiveNewPostDetails = new_post_create => {
  return {
    type: NEW_POST_CREATE,
    new_post_create
  };
};

const receivePostDetails = post_details => {
  return {
    type: RECEIVE_POST_DETAILS,
    post_details
  };
};

const receiveBrand = brand => {
  return {
    type: RECEIVE_BRAND,
    brand
  };
};

const recieveSidebar = sidebar => {
  // console.log(sidebar);
  return {
    type: RECIEVE_SIDEBAR,
    sidebar
  };
};

const recieveTopUsers = users => {
  // console.log(users);
  return {
    type: RECIEVE_TOP_USERS,
    users
  };
};

const recieveRSBElements = elements => {
  return {
    type: RECIEVE_RSB_ELEMENTS,
    elements
  };
};

const setProfilePic = profile_pic => {
  return {
    type: PROFILE_PIC,
    profile_pic
  };
};

export function fecthUserProfilePic() {
  registerEmbeddedAppAuthHeaders();
  return function(dispatch) {
    $.get(`${mainurl}/api/v1/users/${current_user_id}`, function(resp, status) {
      // console.log(resp);
      dispatch(setProfilePic(resp.data.user.profile_pic));
    });
  };
}

export function fetchRightSidebarElements() {
  return function(dispatch) {
    $.post(`${mainurl}/api/v1/user/getRightSidebarElements`, function(
      resp,
      status
    ) {
      dispatch(recieveRSBElements(resp.elements));
    });
  };
}

export function fetchSearchResult() {
  return function(dispatch) {
    let offset = store.getState().offset;
    $.get(
      `${mainurl}/api/v1/post/search/`,
      { offset: offset, input: getSearchTermFromURL },
      function(resp, status) {
        if (resp.posts.length == 0 && resp.users.length == 0)
          dispatch(receiveEmptySearch(true));
        else {
          dispatch(
            receivePosts(
              resp.posts,
              null,
              null,
              resp.brand_feed_posts_exhausted
            )
          );
          dispatch(receiveUsers(resp.users));
        }
      }
    );
  };
}

export function fetchBrandPosts(brand_id) {
  checkAuthHeaders();
  // console.log(brand_id);
  var brandid = brand_id;
  // if(brandid == '') {
  //   brandid = brand_id;
  // }

  if(!brand_id) {
    brandid = store.getState().brandid;
  }
  var currentStore = store;
  var no_follow = currentStore.getState().no_follow;
  var follow_feed_exhausted = currentStore.getState().follow_feed_exhausted;
  // var offset = currentStore.getState().offset;
  var page = currentStore.getState().page;
  var all_post_feed_exhausted = currentStore.getState().all_post_feed_exhausted;
  // var no_calls = currentStore.getState().no_calls;
  // console.log(no_calls);
  // if(!no_calls) {
    // console.log(brand_id);
    // console.log(brandid);
    return function(dispatch) {
      $.get(`${mainurl}/api/v1/posts`, { query: getBrandId(brandid), sort: "relevance", order: "desc", page: page },function(resp, status) {
          // console.log(resp);
          if (resp.data.posts && (resp.data.posts.length > 0) && (page <= currentStore.getState().page)) {
            // console.log(resp);
            dispatch(
              receivePosts(
                resp.data.posts,
                no_follow,
                follow_feed_exhausted,
                resp.data.total
              )
            );
          } else {
            console.log("skipping already added posts");
          }
        }
      );
    };
  // }
}

export function fetchUserPosts() {
  checkAuthHeaders();
  return function(dispatch) {
    var currentStore = store;
    // var offset = store.getState().offset;
    var page = store.getState().page;
    var no_follow = store.getState().no_follow;
    var follow_feed_exhausted = store.getState().follow_feed_exhausted;
    var all_post_feed_exhausted = store.getState().all_post_feed_exhausted;
    $.get(
      `${mainurl}/api/v1/posts`,
      { page: page, limit: 10, query: getUserId(), sort: "relevance" },
      function(resp, status) {
        if (resp.data.posts && (resp.data.posts.length > 0) && (page <= currentStore.getState().page)) {
          // if (offset <= currentStore.getState().offset && resp.data.page >= page){
          // dispatch(receivePosts(resp.data.posts,null,null,all_post_feed_exhausted));
          dispatch(receivePosts(resp.data.posts, no_follow, follow_feed_exhausted, resp.data.total));
          // console.log(resp);
        } else {
          console.log("skipping already added posts");
        }
      }
    );
  };
}

export function fetchAllComments(post_type, post_id, comments_count) {
  var post_type = post_type;
  var postid = post_id;
  var posttype = "";
  var comments_count = comments_count;
  if (post_type == "Post") {
    posttype = "posts";
  }
  if (post_type == "Look") {
    posttype = "looks";
  }
  var currentStore = store;
  var offset = store.getState().offset;
  checkAuthHeaders();
  return function(dispatch) {
    $.get(
      `${mainurl}/api/v1/${posttype}/${post_id}`,
      { comments_offset: 1, comments_limit: comments_count },
      function(resp, status) {
        if (offset <= currentStore.getState().offset) {
          if (post_type == "Post") {
            dispatch(receiveAllComments(resp.data.post));
          }
          if (post_type == "Look") {
            dispatch(receiveAllComments(resp.data.look));
          }
        }
      }
    );
  };
}

export function fetchPosts() {
  checkAuthHeaders();
  return function(dispatch) {
    /*console.log(mainurl);
    var currentStore = store;
    let offset = store.getState().offset;
    let no_follow= store.getState().no_follow;
    let follow_feed_exhausted=store.getState().follow_feed_exhausted;
    let all_post_feed_exhausted = store.getState().all_post_feed_exhausted;
    let feed_exhausted_at=store.getState().feed_exhausted_at;
    $.get('/api/v1/posts/homefeed', { "offset": offset,"no_follow": no_follow,"follow_feed_exhausted": follow_feed_exhausted,"feed_exhausted_at": feed_exhausted_at, "all_post_feed_exhausted": all_post_feed_exhausted}, function(resp, status) {
      if (offset >= currentStore.getState().offset){
        dispatch(receivePosts(resp.posts,resp.state_no_follow,resp.state_follow_feed_exhausted,resp.all_post_feed_exhausted));
     } else
        console.log('skipping already added posts');
    });*/

    var currentStore = store;
    // var offset = store.getState().offset;
    var page = store.getState().page;
    var no_follow = store.getState().no_follow;
    var follow_feed_exhausted = store.getState().follow_feed_exhausted;
    var all_post_feed_exhausted = store.getState().all_post_feed_exhausted;
    var feed_exhausted_at = store.getState().feed_exhausted_at;
    var size = 10;

    // var page = (offset / size) + 1;
    // console.log("offset", offset);
    // console.log("size", size);
    // console.log("page", page);
    //page >= currentStore.getState().page //previous condition

    // $.get(`https://api.myjson.com/bins/ruebt`, function(resp, status) {
    $.get(
      `${mainurl}/api/v1/posts`,
      { page: page, size: size, sort: "relevance" },
      function(resp, status) {
        // console.log("currentStore.getState().offset", currentStore.getState().offset);
        // if (offset <= currentStore.getState().offset && resp.data.page >= page){
        // if (offset <= currentStore.getState().offset && resp.data.page >= page){
        // console.log(page);
        // console.log(currentStore.getState().page);
        if (resp.data.posts && (resp.data.posts.length > 0) && (page <= currentStore.getState().page)) {
          // console.log(resp);
          dispatch(
            receivePosts(
              resp.data.posts,
              no_follow,
              follow_feed_exhausted,
              resp.data.total
            )
          );
        } else {
          console.log("skipping already added posts");
        }
      }
    );
  };
}

/*export const getUserFeed = () => {
  checkIfUserIsLoggedIn()
  var currentStore = store;
  var offset = store.getState().offset;
  return function(dispatch) {
    $.get(`${mainurl}/api/v1/posts`, { "offset": offset, "limit": 10, "query": getUserId() }, function(resp, status) {
      console.log(resp);
      if(offset >= currentStore.getState().offset) {
        dispatch(receiveUserFeed(resp));
      }
      else {
        console.log('skipping already added posts');
      }
    });
  };
}*/

export function sendInvite(email) {
  return function(dispatch) {
    $.post(`${mainurl}/api/v1/user/sendInvite`, { email: email }, function(
      resp,
      status
    ) {
      console.log("sent Invite Email/s");
    });
  };
}
export const toggledLike = id => {
  return {
    type: TOGGLE_LIKE,
    id
  };
};

export const deletedPost = id => {
  return {
    type: DELETED_POST,
    id
  };
};

export const deletedComment = (post_id, comment_id, user_id) => {
  return {
    type: COMMENT_DELETED,
    post_id,
    comment_id,
    user_id
  };
};

export const addedComment = comment => {
  return {
    type: ADD_COMMENT,
    comment
  };
};

export const toggledFollowUser = id => {
  return {
    type: TOGGLE_FOLLOW_USER,
    id
  };
};

export const toggledFollowUserNotification = id => {
  return {
    type: TOGGLE_FOLLOW_USER_NOTIFICATION,
    id
  };
};

export const markAllNotificationsReadResponse = id => {
  return {
    type: MARK_ALL_NOTIFICATION_AS_READ,
    id
  };
};

export const toggledFollowBrand = id => {
  return {
    type: TOGGLE_FOLLOW_BRAND,
    id
  };
};

export const addedCommentToPost = (postid, comment) => {
  return {
    type: COMMENT_ADDED,
    postid,
    comment
  };
};

export const deletePost = (id, post_type) => {
  var post_id = id;
  
  if(post_type == "Post") {
    var posttype = "posts";
  }
  else {
    var posttype = "looks";
  }

  return function(dispatch) {
    $.ajax({
      type: "DELETE",
      url: `${mainurl}/api/v1/${posttype}/${post_id}`,
      headers: checkAuthHeaders(),
      data: SendPostId(post_id, posttype),
      success: function(resp, status) {
        dispatch(deletedPost(post_id));
      }
    });
  };
};

export const deleteComment = (post_id, comment_id, user_id) => {
  checkAuthHeaders();
  return function(dispatch) {
    $.post(
      `${mainurl}/api/v1/post/delete_comment`,
      { post_id: post_id, comment_id: comment_id, user_id: user_id },
      function(resp, status) {
        dispatch(deletedComment(resp.post_id, resp.comment_id, resp.user_id));
      }
    );
  };
};

export const addPostsCommentViaAPI = (post_id, comment) => {
  var postid = post_id;
  var commented = comment;
  return function(dispatch) {
    $.ajax({
      type: "PUT",
      url: `${mainurl}/api/v1/posts/${postid}?action_type=comments_comment`,
      headers: checkAuthHeaders(),
      data: sendPostsCommentData(postid, commented),
      success: function(resp, status) {
        console.log(resp);
        if (comment) {
          window.show_animation_for_comment_post = post_id;
          dispatch(addedCommentToPost(postid, resp.data.post.comments));
        }
      }
    });
  };
  /*checkAppAuthHeaders()
  return function(dispatch) {
    $.put(`${mainurl}/api/v1/posts/${post_id}`, function(resp, status) {
      if(comment){
        window.show_animation_for_comment_post = post_id;
        dispatch(addedCommentToPost(resp.comment))
      }
    });
  }*/
};

export const addLooksCommentViaAPI = (post_id, comment) => {
  var postid = post_id;
  var commented = comment;
  return function(dispatch) {
    $.ajax({
      type: "PUT",
      url: `${mainurl}/api/v1/looks/${postid}?action_type=comments_comment`,
      headers: checkAuthHeaders(),
      data: sendLooksCommentData(postid, commented),
      success: function(resp, status) {
        console.log(resp);
        if (comment) {
          window.show_animation_for_comment_post = post_id;
          dispatch(addedCommentToPost(postid, resp.data.look.comments));
        }
      }
    });
  };
  /*checkAppAuthHeaders()
  return function(dispatch) {
    $.put(`${mainurl}/api/v1/looks/${post_id}`, function(resp, status) {
      if(comment){
        window.show_animation_for_comment_post = post_id;
        dispatch(addedCommentToPost(resp.comment))
      }
    });
  }*/
};

export const toggleFollowOnProfilePage = id => {
  checkIfUserIsLoggedIn();
  return function(dispatch) {
    $.post(
      `${mainurl}/api/v1/user/follow_user`,
      { id: getProfileIdFromPage() },
      function(resp, status) {
        // console.log(resp);
        dispatch(toggledFollowUser(resp.id));
      }
    );
  };
};

export const toggledFollowNotificationPage = id => {
  checkIfUserIsLoggedIn();
  return function(dispatch) {
    $.post(`${mainurl}/api/v1/user/follow_user`, { id: id }, function(
      resp,
      status
    ) {
      dispatch(toggledFollowUserNotification(resp.id));
    });
  };
};

export const getProfile = () => {
  return function(dispatch) {
    $.get(
      `${mainurl}/api/v1/user/get_profile`,
      { id: getProfileIdFromPage() },
      function(resp, status) {
        dispatch(receiveProfile(resp));
      }
    );
  };
};

export const getNewPostUserDetails = () => {
  return function(dispatch) {
    $.get(`${mainurl}/api/v1/posts/newpost`, {}, function(resp, status) {
      dispatch(receiveNewPostUserDetails(resp));
    });
  };
};

export const getAutoCompleteDetails = () => {
  return function(dispatch) {
    $.get(`${mainurl}/api/v1/brand/get_all_brand_products/`, function(
      resp,
      status
    ) {
      dispatch(receiveAutoCompleteDetails(resp));
    });
  };
};

/*export const createPost = (data) => {
  return function(dispatch) {
    $.get(`${mainurl}/api/v1/posts`, function(resp, status) {
      if()
      dispatch(createPostProgress(true));
    });
  };
}*/

export const getNewPostDetails = (data) => {
  // console.log(data);
  var data_value = data;
  return function(dispatch) {
    var formData = new FormData();

    // add assoc key values, this will be posts values
    // setting form data
    console.log(data_value);

    //append description
    formData.append("description", data_value.description);

    //append custom brands
    if (data_value.Brand) {
      formData.append("brand_ids[]", data_value.Brand);
    }
    else {
      formData.append("brand_ids[]", data_value.custom_brand);
    }

    //append products
    if (data_value.Product) {
      formData.append("product_ids[]", data_value.Product);
    }
    else {
      formData.append("product_ids[]", data_value.custom_product);
    }

    //append image
    if (data_value.images_attributes) {
      formData.append("images_attributes[]", $("input[type=file]")[0].files[0]);
    }

    //append video
    formData.append("video_attributes[]", null);

    //For Price & Description
    formData.append("price", data_value.Price);

    for (let pair of formData.entries()) {
      // console.log(pair[0] + ': ' + pair[1]); 
    }

    // Content-Type:multipart/form-data; boundary=----WebKitFormBoundaryqfm4Xg8b3EZVrbNg

    $.ajax({
      type: "POST",
      url: `${mainurl}/api/v1/posts`,
      data: formData,
      beforeSend: function() {
        $("#new_post_desc").prop("disabled", true);
        $("#autocomplete_brand").prop("disabled", true);
        $("#autocomplete_product").prop("disabled", true);
        $("#autocomplete_price").prop("disabled", true);
        dispatch(createPostProgress(true));
      },
      success: function(resp) {
        $("#new_post_desc").val("");
        $("#new_post_image").val("");
        $("#autocomplete_brand").val("");
        $("#autocomplete_product").val("");
        $("#autocomplete_price").val("");
        $("#brand_value_selected").val("");
        $("#product_value_selected").val("");
        $("#new_post_desc").prop("disabled", false);
        $("#autocomplete_brand").prop("disabled", false);
        $("#autocomplete_product").prop("disabled", false);
        $("#autocomplete_price").prop("disabled", false);

        dispatch(receiveNewPostDetails(resp));
        dispatch(resetNewPostFlag());
        dispatch(createPostProgress(false));
      },
      error: function(error) {
        dispatch(createPostProgress(false));
      },
      async: true,
      cache: false,
      contentType: false,
      processData: false,
      timeout: 60000
    });
  };
};

export function fetchTopUsers() {
  // checkAuthHeaders();
  // return function(dispatch) {
  //   $.get(
  //     `${mainurl}/api/v1/users`,
  //     { query: "trending:true", sort: "brand_rank", page: 1, size: 2 },
  //     function(resp, status) {
  //       // $.get(`https://api.myjson.com/bins/j2u5f`, function(resp, status) {
  //       // console.log(resp.data);
  //       dispatch(recieveTopUsers(resp.data));
  //     }
  //   );
  // };

  return function(dispatch) {
    $.ajax({
      type: "GET",
      url: `${mainurl}/api/v1/users?query=trending:true&sort=brand_rank&page=1&size=2`,
      headers: checkAuthHeaders(),
      success: function(resp, status) {
        // console.log(resp.data);
        dispatch(recieveTopUsers(resp.data));
      }
    });
  }
}

export function fetchSidebar() {
  // checkAuthHeaders();
  // return function(dispatch) {
  //   $.get(
  //     `${mainurl}/api/v1/brands`,
  //     {
  //       query: "trending:true",
  //       sort: "brand_rank",
  //       order: "desc",
  //       trending_images_count: 4
  //     },
  //     function(resp, status) {
  //     // $.get(`https://api.myjson.com/bins/wis7v`, function(resp, status) {
  //       console.log(resp.data);
  //       dispatch(recieveSidebar(resp.data));
  //     }
  //   );
  // };

  return function(dispatch) {
    $.ajax({
      type: "GET",
      url: `${mainurl}/api/v1/brands?query=trending:true&sort=brand_rank&order=desc&trending_images_count=4`,
      headers: checkAuthHeaders(),
      success: function(resp, status) {
        // console.log(resp.data);
        dispatch(recieveSidebar(resp.data));
      }
    });
  }
}

export const togglePostsLike = id => {
  // checkAppAuthHeaders()
  var like_id = id;
  return function(dispatch) {
    $.ajax({
      type: "PUT",
      url: `${mainurl}/api/v1/posts/${id}`,
      headers: checkAuthHeaders(),
      data: sendPostsLikeData(like_id),
      success: function(resp, status) {
        // console.log(resp);
        window.show_animation_for_like_post = resp.id;
        dispatch(toggledLike(resp.data.post.id));
      }
    });
  };
  // $.put(`${mainurl}/api/v1/posts/${id}`, function(resp, status) {
  //   window.show_animation_for_like_post = resp.id;
  //   dispatch(toggledLike(resp.id));
  // });
};

export const toggleLooksLike = id => {
  var like_id = id;
  return function(dispatch) {
    $.ajax({
      type: "PUT",
      url: `${mainurl}/api/v1/looks/${id}`,
      headers: checkAuthHeaders(),
      data: sendLooksLikeData(like_id),
      success: function(resp, status) {
        // console.log(resp);
        window.show_animation_for_like_post = resp.id;
        dispatch(toggledLike(resp.data.look.id));
      }
    });
  };
  /*checkAppAuthHeaders()
  return function(dispatch) {
    $.put(`${mainurl}/api/v1/looks/${id}`, function(resp, status) {
      window.show_animation_for_like_post = resp.id;
      dispatch(toggledLike(resp.id));
    });
  }*/
};

export const getPostDetails = () => {
  let urlArray = window.location.href.split("/");
  // console.log(url[4]);
  var post_type = urlArray[3];
  var post_id = urlArray[4];
  // console.log(post_type, post_id);
  // checkAuthHeaders();
  // return function(dispatch) {
  //   $.get(`${mainurl}/api/v1/${post_type}/${post_id}`, function(resp, status) {
  //     // console.log(resp);
  //     dispatch(receivePostDetails(resp.data));
  //   });
  // };

  return function(dispatch) {
    $.ajax({
      type: "GET",
      url: `${mainurl}/api/v1/${post_type}/${post_id}`,
      headers: checkAuthHeaders(),
      success: function(resp, status) {
        // console.log(resp);
        dispatch(receivePostDetails(resp.data));
      },
      error: function(error) {
        console.log(error);
        dispatch(receivePostDetails(error.responseJSON));
      }
    });
  }
};

/*export const getNotificationDetails = () => {
  registerEmbeddedAppAuthHeaders()
  return function(dispatch) {
    $.get(`${mainurl}/api/v1/posts/notificationdetails`, {}, function(resp, status) {
      dispatch(receiveNotificationDetails(resp.data));
    });
  };
}*/

export const getFollowNotificationDetails = () => {
  return function(dispatch) {
    $.get(`${mainurl}/api/v1/user/follownotificationdetails`, {}, function(
      resp,
      status
    ) {
      dispatch(receiveFollowNotificationDetails(resp));
    });
  };
};

export const getNotificationCount = () => {
  return function(dispatch) {
    $.get(`${mainurl}/api/v1/posts/notificatoncount`, {}, function(
      resp,
      status
    ) {
      dispatch(receiveNotificationCount(resp));
    });
  };
};

export const toggleFollowOnBrandPage = (id) => {
  var follow_id = id;
  // console.log(follow_id);
  /*checkAppAuthHeaders();
  return function(dispatch) {
    $.post(
      `${mainurl}/api/v1/brand/follow_brand`,
      { id: getBrandIdFromPage() },
      function(resp, status) {
        console.log(resp);
        dispatch(toggledFollowBrand(resp.id));
      }
    );
  };*/

  return function(dispatch) {
    $.ajax({
      type: "POST",
      url: `${mainurl}/api/v1/brand/follow_brand`,
      headers: checkAuthHeaders(),
      data: SendFollowId(follow_id),
      success: function(resp, status) {
        // console.log(resp);
        dispatch(toggledFollowBrand(resp.id));
      }
    });
  }
};

export const getNotificationDetails = () => {
  // checkAuthHeaders();
  // return function(dispatch) {
  //   $.get(`${mainurl}/api/v1/notifications`, function(resp, status) {
  //   // $.get(`https://api.myjson.com/bins/9zzy3`, function(resp, status) {
  //     // console.log(resp);
  //     dispatch(receiveNotificationDetails(resp.data));
  //   });
  // };

  var count_notification = store.getState().count_notification;
  var page = store.getState().page;

  return function(dispatch) {
    $.ajax({
      type: "GET",
      url: `${mainurl}/api/v1/notifications?page=${page}&size=10&sort=relevance`,
      headers: checkAuthHeaders(),
      success: function(resp, status) {
        console.log(resp);
        console.log(count_notification);
        console.log(resp.data.total);
        if(count_notification < resp.data.total) {
          
          dispatch(receiveNotificationDetails(resp.data));
        }
      }
    });
  }
};

export const markAllNotificationsRead = ids => {
  // return function(dispatch) {
  //   $.get(`${mainurl}/api/v1/notifications/read`, function(resp, status) {
  //     console.log(resp);
  //     dispatch(markAllNotificationsReadResponse(resp.data.notifications));
  //   });
  // };

  var notif_ids = ids;

  console.log(notif_ids);

  return function(dispatch) {
    $.ajax({
      type: "GET",
      url: `${mainurl}/api/v1/notifications/read`,
      headers: checkAuthHeaders(),
      data: getData(notif_ids),
      success: function(resp, status) {
        console.log(resp);
        dispatch(markAllNotificationsReadResponse(resp.data.notifications));
      }
    });
  }
};

export const getBrand = () => {
  return function(dispatch) {
    $.get(`${mainurl}/api/v1/brands/${getBrandIdFromPage()}`, function(
      resp,
      status
    ) {
      // console.log(resp);
      dispatch(receiveBrand(resp.data));
    });
  };
};

const getPostIdFromPage = () => {
  let urlArray = window.location.href.split("/");
  return urlArray[4];
};

const getSearchTermFromURL = () => {
  let urlArray = window.location.href.split("/");
  return urlArray[4];
};

const getProfileIdFromPage = () => {
  let urlArray = window.location.href.split("/");
  return urlArray[4];
};

export const getUserId = () => {
  let urlArray = window.location.href.split("/");
  // console.log(urlArray);
  let id = "user_id:" + urlArray[4];
  // console.log(id);
  return id;
};

const getBrandIdFromPage = () => {
  let urlArray = window.location.href.split("/");
  return urlArray[4];
};

const getBrandId = brandid => {
  // let urlArray = window.location.href.split("/");
  let id = "brand_id:" + brandid;
  // console.log(id);
  return id;
};

const getBrandCountryFromPage = () => {
  let urlArray = window.location.href.split("/");
  return urlArray[3];
};

const checkIfUserIsLoggedIn = () => {
  if (window.access_token) {
    return;
  }
  if (!userLoggedIn) window.location = "/users/sign_in";
};

export const registerEmbeddedAppAuthHeaders = () => {
  if (window.access_token) {
    $.ajaxSetup({
      headers: { Authorization: "Bearer " + window.access_token }
    });
  }
};

/*export const checkAppAuthHeaders = () => {
  if (window.access_token) {
    $.ajaxSetup({
      headers: {
        Authorization: "Bearer " + window.access_token,
        "Content-Type": "application/json"
      }
    });
  }
};*/

export const checkAuthHeaders = () => {
  // console.log(window.access_token);
  // var headers = { "Authorization": "", "Content-Type": "application/json" };
  var headers = {};
  if (window.access_token) {
    let token = "Bearer " + window.access_token;
    var headers = { Authorization: token, "Content-Type": "application/json" };
    // console.log(headers);
  }
  return headers;
};

export const checkPostAuthHeaders = () => {
  // console.log(window.access_token);
  // var headers = { "Authorization": "", "Content-Type": "application/json" };
  var headers = {};
  if (window.access_token) {
    let token = "Bearer " + window.access_token;
    var headers = {
      Authorization: token,
      "Content-Type": "application/x-www-form-urlencoded"
    };
    // console.log(headers);
  }
  return headers;
};

export const sendLooksLikeData = like_id => {
  var posts_id = like_id;

  let clapsdata = {
    look: {
      id: posts_id,
      clap_attribute: {
        clap: true
      }
    }
  };

  let clap_data = JSON.stringify(clapsdata);

  let claps = clap_data;

  return claps;
};

export const sendPostsLikeData = like_id => {
  var posts_id = like_id;

  let clapsdata = {
    post: {
      id: posts_id,
      clap_attribute: {
        clap: true
      }
    }
  };

  let clap_data = JSON.stringify(clapsdata);

  let claps = clap_data;

  return claps;
};

export const sendLooksCommentData = (post_id, comment) => {
  var posts_id = post_id;
  var obj = { comment: comment };
  var comments = [];

  comments.push(obj);

  let commented_data = {
    look: {
      id: posts_id,
      comments_attributes: comments
    }
  };

  let comment_data = JSON.stringify(commented_data);

  let commented = comment_data;

  // console.log(commented);
  return commented;
};

export const sendPostsCommentData = (post_id, comment) => {
  var posts_id = post_id;
  var obj = { comment: comment };
  var comments = [];

  comments.push(obj);

  let commented_data = {
    post: {
      id: posts_id,
      comments_attributes: comments
    }
  };

  let comment_data = JSON.stringify(commented_data);

  let commented = comment_data;

  // console.log(commented);
  return commented;
};

export const SendPostId = (post_id, posttype) => {

  if(posttype == "looks") {
    var delete_data = {
      look: {
        id: post_id
      }
    }
  }
  else {
    var delete_data = {
      post: {
        id: post_id
      }
    }
  }

  let deleted = JSON.stringify(delete_data);

  return deleted;
}

export const SendFollowId = follow_id => {

  var follow_data = {
    id: follow_id
  }

  let followed = JSON.stringify(follow_data);

  return followed;
}

export const getData = notif_ids => {

console.log(notif_ids);
  var notif_data = {
    notification: {
      ids:notif_ids,
      read: true
    }
  }

  let notified_data = JSON.stringify(notif_data);

  return notified_data;

}
