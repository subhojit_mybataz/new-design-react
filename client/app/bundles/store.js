import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import createLogger from 'redux-logger';
import { reducers } from "./HelloWorld/reducers/index";

// apply the middleware
//let middleware = applyMiddleware(thunk);

// add the middlewares
let middlewares = [];

// add the router middleware
middlewares.push(thunk);

const logger = createLogger({ });

middlewares.push(logger);

let middleware = applyMiddleware(...middlewares);

// create the store
export const store = createStore(reducers, middleware);

// export
//export default store;
