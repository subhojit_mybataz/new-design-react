# Installation Steps:

1. git clone git@bitbucket.org:mybataz/rails-application.git
2. cd rails-application
3. bundle install
   If this fails with error libv8, the run this command:
   gem install libv8 -v 5.3.332.38.5 -- --with-system-v8
4. rake db:create
5. rake db:migrate
6. rake db:seed
7. npm install
8. npm install bower
9. rake bower:install


# TO run this app
1. rails s
2. sh -c 'rm app/assets/webpack/* || true && cd client && bundle exec rake react_on_rails:locale && yarn run build:development'
