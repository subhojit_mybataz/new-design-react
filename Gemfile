# frozen_string_literal: true

source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

gem 'rails', '~> 5.0.2'

gem 'aws-sdk'
gem 'acts_as_commentable'
gem 'activeadmin', github: 'activeadmin'
gem 'acts_as_paranoid', '~> 0.5.0'
gem 'access-granted', '~> 1.1.0'
gem 'active_model_serializers', '~> 0.10.0'
gem 'bootstrap-sass'
gem 'bower-rails', '~> 0.11.0'
gem 'coffee-rails', '~> 4.2'
gem 'devise'
gem 'doorkeeper'
gem 'exception_notification'
gem 'friendly_id', '~> 5.2.1'
gem 'font-awesome-sass'
gem 'fcm'
gem 'jquery-rails'
gem 'jbuilder', '~> 2.5'
gem 'jwt'
gem 'kaminari'
gem 'listen', '~> 3.0.5'
gem 'link_thumbnailer'
gem 'mini_racer', platforms: :ruby
gem 'omniauth-facebook'
gem 'omniauth-google-oauth2'
gem 'omniauth-instagram'
gem 'ahoy_matey', '~> 1.6.1'
gem 'pg', '~> 0.18'
gem 'puma', '~> 3.0'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'paperclip', '~> 5.0.0'
gem 'paperclip-compression'
gem 'paperclip-ffmpeg', '~> 1.0.1'
gem 'premailer-rails'
gem 'react_on_rails', '~> 7'
gem 'rack-cors', :require => 'rack/cors'
gem 'slack-notifier'
gem 'socialization', :git => 'https://github.com/ajays1991/socialization.git'
gem 'tzinfo'
gem 'turbolinks', '~> 5'
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]

group :development do
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'capistrano', '~> 3.4.0'
  gem 'capistrano-bundler'
  gem 'capistrano-npm'
  gem 'capistrano-nvm'
  gem 'capistrano-passenger'
  gem 'capistrano-rails'
  gem 'capistrano-rvm'
  gem 'capistrano-yarn'
  gem 'guard'
  gem 'guard-livereload'
  gem 'guard-rails'
  gem 'meta_request'
  gem 'rb-readline'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
end

group :development, :test do
  gem 'byebug', platform: :mri
  gem 'mocha'
  gem 'pry-rails'
  gem 'rubocop', require: false
  gem 'rubocop-checkstyle_formatter', require: false
  gem 'rspec-rails', '~> 3.6'
  gem 'rspec-activemodel-mocks'
  gem 'rspec-mocks', '~> 3.6.0'
  gem 'factory_girl_rails', '~> 4.7.0'
end

group :test do
  gem 'brakeman'
  gem 'rspec_junit_formatter'
  gem 'rspec-collection_matchers'
  gem 'shoulda-matchers', '~> 3.1'
  gem 'simplecov'
end