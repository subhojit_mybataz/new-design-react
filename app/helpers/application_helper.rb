# frozen_string_literal: true

module ApplicationHelper
  include ActionView::Helpers::AssetUrlHelper

  OLD_APP_VERSION = 1.1
  PN_LIMIT = 10

  def getPostFeed(current_post)
    postJSON = current_post.getFeedJSON(current_user_from_base)
    postJSON[:comments] = current_post.comments.recent.reverse.map(&:getFeedJSON)
    postJSON
  end

  def get_brand_country
    brand_country = BrandCountry.all.map do |n|
      brand_name = Brand.find(n.brand_id).name
      country_name = Country.find(n.country_id).name
      ["#{brand_name} | #{country_name}", n.id]
    end
    brand_country
  end

  def get_abuse_type_for_list
    %w[Post Look User Comment]
  end

  def get_user_profile_pic(user_id, size)
    User.find(user_id).image.url(size)
  end

  # # Use this function to get the current_user while accessing from both the web and app
  def current_user_from_base
    if request.headers['Authorization'].present?
      if !@current_user.nil?
        @current_user
      else
        @current_user = User.find(doorkeeper_token[:resource_owner_id])
        @current_user
      end
    else
      current_user
    end
  end

  def facebook_auth_helper(fb_data_hash)
    email = fb_data_hash[:email]
    @user = User.find_by_email(email)
    @identity = Identity.find_by_email(email)
    if @user.nil?
      @first_login = true
      @user = User.create(email: email, fname: fb_data_hash[:first_name] || '', lname: fb_data_hash[:last_name] || '', provider: fb_data_hash[:provider], uid: fb_data_hash[:uid], gender: fb_data_hash[:gender] || '', bataz_icon_user: fb_data_hash[:bataz_icon_user] || false, password: Devise.friendly_token[0, 20])
      token = SecureRandom.hex(9).to_s.upcase
      @user.random_auth_token = token
      @user.firebase_auth_token = createFirebaseToken(email)
      @user.save
    else
      @first_login = false
    end
    @identity.update_attribute(:user_id, @user.id)
    @first_login
  end

  def auth_helper(data_hash)
    email = data_hash[:email]
    @user = User.find_by_email(email)
    @identity = Identity.find_by_email(email)
    if @user.nil?
      @first_login = true
      @user = User.create(email: email, fname: data_hash[:first_name] || '', lname: data_hash[:last_name] || '', provider: data_hash[:provider], uid: data_hash[:uid], gender: data_hash[:gender] || '', bataz_icon_user: data_hash[:bataz_icon_user] || false, password: Devise.friendly_token[0, 20])
    else
      @first_login = false
    end
    @identity.update_attribute(:user_id, @user.id)
    @first_login
  end

  def checkAppVersion(user)
    unless user.app_version.nil?
      return true if user.app_version.to_f > OLD_APP_VERSION
    end
    false
  end

  def check_pn_limit(user)
    counter = PushNotifTracker.where('user_id = ? and created_at > ? and created_at < ?', user.id, 1.day.ago, Time.now)
    return true if counter.size < PN_LIMIT
    false
  end

  def get_trending_brand(trend_brand_limit, sidebar_presence)
    brands = []
    trending_brand = {}
    if sidebar_presence == true
      img_limit = 2
      trending_brand = BrandCountry.where(trending: true).order(:brand_rank)
    else
      img_limit = 4
      trending_brand = BrandCountry.where(trending: true).order(:brand_rank).limit(1).offset(trend_brand_limit - 1)
    end
    brands = trending_brand.map do |trending|
      brand_row = {}
      brand = trending.brand
      posts = Post.where('brand_id=? and deleted=? and image_file_name IS NOT NULL', brand.id, false).order(likers_count: :desc).limit(img_limit)
      images = []
      images = posts.each.map do |post|
        temp = {}
        temp[:post_id] = post.id
        temp[:url] = post.image.url(:sidebar_dimension) if post.image.present?
        temp
      end
      brand_row[:country] = trending.country.name
      brand_row[:image_urls] = images
      brand_row[:id] = brand.id
      brand_row[:slug] = brand.slug
      # brand_row[:total_likes] = brand.total_likes
      # brand_row[:total_comments] = brand.total_comments
      brand_row[:brand_name] = brand.name
      brand_row[:type] = 'trending_brand'
      brand_row
    end

    brands
  end
end
