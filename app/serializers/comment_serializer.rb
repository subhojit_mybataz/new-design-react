# frozen_string_literal: true

class CommentSerializer < ActiveModel::Serializer
  include ActionView::Helpers::DateHelper
  attribute :id
  attribute :comment
  attribute :created_at
  attribute :created_at_string
  attribute :user

  def user
    UserSerializer.new(object.user).attributes
  end

  def created_at_string
    time_ago_in_words(object.created_at)
  end
end
