# frozen_string_literal: true

class EmoticonSerializer < ActiveModel::Serializer
  attribute :id
  attribute :name
  attribute :url
  attribute :url_logo
end
