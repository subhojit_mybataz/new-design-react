# frozen_string_literal: true

class ClapSerializer < ActiveModel::Serializer
  attribute :id
  attribute :count
  attribute :user

  def user
    UserSerializer.new(object.user).attributes
  end
end
