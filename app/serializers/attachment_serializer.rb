# frozen_string_literal: true

class AttachmentSerializer < ActiveModel::Serializer
  attributes :id, :content_type, :file_type, :fixed, :app_compressed, :app, :sidebar, :original

  def content_type
    object.image_content_type || object.video_content_type
  end

  def fixed
    object.image.url(:fixed_dimension)
  end

  def original
    object.image.url(:original)
  end

  def app_compressed
    object.image.url(:app_compressed_dimension)
  end

  def app
    object.image.url(:app_dimension)
  end

  def sidebar
    object.image.url(:fixed_compressed_dimension)
  end
end
