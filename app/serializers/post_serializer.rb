# frozen_string_literal: true

class PostSerializer < StorySerializer
  attribute :bookmark_posts_count
  has_many :bookmark_posts, if: :show?
end
