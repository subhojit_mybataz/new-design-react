# frozen_string_literal: true

class BrandSerializer < ActiveModel::Serializer
  attributes :id, :name, :followers_count, :slug, :brand_logo, :total_guru_badges, :total_ambassador_badges, :posts_count,
             :looks_count, :comments_count, :claps_count, :current_user_is_following, :is_active
  has_many :banners
  attribute :stories
  attribute :top_contributors, if: :is_brand?
  attribute :description

  def is_brand?
    @instance_options[:controller] == 'brand'
  end

  def current_user_is_following
    User.current.follows?(object)
  end

  def followers_count
    object.followers(User).count
  end

  def brand_logo
    {
      fixed: object.logo.url(:fixed_dimension),
      medium: object.logo.url(:medium),
      thumb: object.logo.url(:thumb)
    }
  end

  def stories
    return [] if @instance_options[:trending_images_count].nil?
    ActiveModelSerializers::SerializableResource.new(object.stories.order(claps_count: :desc).limit(@instance_options[:trending_images_count]), @instance_options)
  end
end