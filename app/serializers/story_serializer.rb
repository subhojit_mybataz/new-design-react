# frozen_string_literal: true

class StorySerializer < ActiveModel::Serializer
  include ActionView::Helpers::DateHelper
  attributes :id
  has_many :attachments, if: :is_comments_comment_not?
  has_many :claps, if: :is_comments_comment_not?
  has_one :author, if: :is_comments_comment_not?
  has_one :likers_count, if: :is_comments_comment_not?
  has_many :comments
  has_many :products, if: :not_brands?
  has_many :brands, if: :not_brands?

  def attributes(*args)
    if is_comments_comment_not?
      super.merge({ description: object.description, author_id: self.author_id, created_at: object.created_at,
                    comments_count: object.comments_count, price: object.price, created_at_string: self.created_at_string,
                    brands_count: object.brands_count, products_count: object.products_count,
                    report_abuses_count: object.report_abuses_count, claps_count: object.claps_count,
                    current_user_claps_count: self.current_user_claps_count, share_link: self.share_link,
                    metadata: object.metadata, type: object.type, emoticons_count: object.emoticon_aggregate, deleted_at: object.deleted_at})
    else
      super
    end
  end

  def is_comments_comment_not?
    @instance_options[:action_type] != 'comments_comment'
  end

  def not_brands?
    @instance_options[:controller] != 'brand' && @instance_options[:action_type] != 'comments_comment'
  end

  def show?
    @instance_options[:method] != 'index'
  end

  def share_link
    host = Rails.application.config.action_mailer.default_url_options[:host]
    host += ":#{Rails.application.config.action_mailer.default_url_options[:port]}" if Rails.application.config.action_mailer.default_url_options[:port].present?
    "#{host}/posts/#{object.uuid}" 
  end

  def current_user_claps_count
    user_clap = object.claps.where(user: User.current).first
    user_clap.nil? ? 0 : user_clap.count
  end

  def author_id
    return nil if object.user.nil?
    object.user.id
  end

  def author
    object.user
  end

  def claps
    return [] if @instance_options[:method] == 'index' || @instance_options[:claps_offset].nil?
    offset = @instance_options[:claps_offset] || 0
    limit = @instance_options[:claps_limit] || 5
    offset = (offset - 1) * limit
    object.claps.offset(offset).limit(limit)
  end

  def comments
    return [] if object.last_comment.nil?
    if @instance_options[:action_type] == 'comment'      
      return object.comments.last(2)
    elsif @instance_options[:action_type] == 'comments_comment' || @instance_options[:method] == 'index' || @instance_options[:comments_offset].nil?
      return [object.comments.last]
    else
      offset = @instance_options[:comments_offset].to_i || 1
      limit = @instance_options[:comments_limit].to_i || 5
      offset = (offset - 1) * limit
      return object.comments.order(created_at: :asc).offset(offset).limit(limit)
    end
  end

  def created_at_string
    "#{time_ago_in_words(object.created_at)} ago"
  end
end
