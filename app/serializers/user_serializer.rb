# frozen_string_literal: true

class UserSerializer < ActiveModel::Serializer
  attributes :id, :name, :admin, :followers_count, :followees_count, :username, :profile_pic, :current_user_is_following, :posts_count, :looks_count
  has_many :user_types, if: :current_user?
  has_many :followers
  has_many :followees

  def attributes(*args)
    if current_user? && user_controller?
      super.merge({ email: object.email, city: object.city, dob: object.dob, referer_code: object.referer_code, phone: object.phone, bio: object.bio, gender: object.gender, metadata: object.metadata })
    else
      super
    end
  end

  def current_user_is_following
    User.current.follows?(object)
  end

  def name
    object.full_name
  end

  def followers
    return [] if @instance_options[:method] == 'index' || @instance_options[:followers_offset].nil?
    offset = @instance_options[:followers_offset].present? ? @instance_options[:followers_offset] : 0
    limit = @instance_options[:followers_limit].present? ? @instance_options[:followers_limit] : 5
    object.followers(User, {offset: offset, limit: limit})
  end

  def followees
    return [] if @instance_options[:method] == 'index' || @instance_options[:followees_offset].nil?
    offset = @instance_options[:followees_offset].present? ? @instance_options[:followees_offset] : 0
    limit = @instance_options[:followees_limit].present? ? @instance_options[:followees_limit] : 5
    object.followees(User, {offset: offset, limit: limit})
  end

  def current_user?
    User.current.id == object.id
  end

  def user_controller?
    @instance_options[:controller] == 'user'
  end
end
