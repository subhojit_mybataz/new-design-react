# frozen_string_literal: true

class AbuseTypeSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :abuse_type_for
end