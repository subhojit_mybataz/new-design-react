# frozen_string_literal: true

class ProductSerializer < ActiveModel::Serializer
  attributes :id, :name, :posts_count, :looks_count, :comments_count, :claps_count
end
