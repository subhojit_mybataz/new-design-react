# frozen_string_literal: true

class LookSerializer < StorySerializer
  attribute :save_looks_count, if: :is_comments_comment_not?
  attribute :avenue_to_buy, if: :is_comments_comment_not?
  attribute :detail, if: :is_comments_comment_not?
  has_many :save_looks, if: Proc.new { self.show? && self.is_comments_comment_not? }
end
