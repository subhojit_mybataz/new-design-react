# frozen_string_literal: true

class NotificationSerializer < ActiveModel::Serializer
  include ActionView::Helpers::DateHelper
  attributes :id, :trackable_type, :trackable_id, :owner_type, :owner_id, :activity_type, :recipient_type, :recipient_id,
             :read, :created_at, :updated_at, :metadata
  attribute :user
  attribute :created_at_string

  def created_at_string
    "#{time_ago_in_words(object.created_at)} ago"
  end

  def user
    User.where(id: object.owner_id).first.as_json(notification: true)
  end
end
