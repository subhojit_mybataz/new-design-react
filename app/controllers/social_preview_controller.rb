# frozen_string_literal: true

class SocialPreviewController < ApplicationController
  def index
    @post = Post.find(params[:id])
  end

  def brand_preview
    @brand = Brand.find_by_name(params[:brandname])
  end
end
