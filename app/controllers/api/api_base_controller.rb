# frozen_string_literal: true

class Api::ApiBaseController < ApplicationController
  protect_from_forgery with: :null_session

  before_action :doorkeeper_authorize!
  before_filter :set_current_user

  rescue_from Exception, with: :throw_dry_exception
  respond_to :json

  include ActionView::Helpers::DateHelper
  include ApplicationHelper
  include ActionController::Serialization

  serialization_scope :serializer_options

  def index
    page = params[:page] || 1
    size = params[:size] || 10
    resources, resources_count = Search::SearchManager.new(resource_name, params, page, size, index_preloads).search
    render_json_response({message: 'SUCCESS', status: 200, data: {page: page.to_i, size: size.to_i, total: resources_count.to_i, resource_name.to_s.pluralize.to_sym => ActiveModelSerializers::SerializableResource.new(resources, index_serializer_options)}}, :crud)
  end

  def create
    resource = resource_name.to_s.split('_').map(&:capitalize).join('').constantize.new(resource_params)
    authorize! :create, resource
    resource.current_user = current_user_from_base
    resource.save!
    render_json_response({message: 'SUCCESS', status: 200, data: {resource_name.to_sym => ActiveModel::SerializableResource.new(resource.reload, serializer_options).serializable_hash}}, :create)
  end

  def update
    resource = resource_name.to_s.split('_').map(&:capitalize).join('').constantize.find_by_id(resource_params[:id])
    authorize! :update, resource
    if resource_params[:name].present?
      split_name = resource_params[:name].split(' ', 2)
      resource.fname = split_name[0]
      resource.lname = split_name[1]
    end
    resource.current_user = current_user_from_base
    resource.update(resource_params)
    resource.save!
    resource = resource_name.to_s.split('_').map(&:capitalize).join('').constantize.where(id: resource_params[:id]).eager_load(show_preloads).first
    render_json_response({message: 'SUCCESS', status: 200, data: {resource_name.to_sym => ActiveModel::SerializableResource.new(resource, serializer_options).serializable_hash}}, :create)
  end

  def show
    resource = resource_name.to_s.split('_').map(&:capitalize).join('').constantize.eager_load(show_preloads).find(params[:id])
    authorize! :show, resource
    render_json_response({message: 'SUCCESS', status: 200, data: {resource_name.to_sym => ActiveModel::SerializableResource.new(resource, serializer_options).serializable_hash}}, :show)
  end

  def destroy
    resource = resource_name.to_s.split('_').map(&:capitalize).join('').constantize.find(params[:id])
    authorize! :destroy, resource
    if (resource_name == :post || resource_name == :look)
      if resource.user_id != current_user_from_base.id && current_user_from_base.admin && params[resource_name][:admin_destroy]
        report_abuse = ReportAbuse.create(
          reportable_type: resource_name.to_s.capitalize,
          reportable_id: resource.id,
          user_id: current_user_from_base.id,
          abuse_type_id: params[resource_name][:abuse_type_id],
          is_reviewed: true
        )
        report_abuse.send_removal_notification(resource, abuse_type_id = params[resource_name][:abuse_type_id])
      end
    end
    resource.destroy
    render_json_response({message: 'SUCCESS', status: 204, data: nil}, :delete)
  end

  def current_user
    current_user_from_base
  end

  def render_json_response(response, action_type)
    app_status = response[:status].to_i
    http_status = case action_type
                    when :create then
                      if app_status == 200
                        :created
                      elsif app_status == 600
                        :ok
                      else
                        :unprocessable_entity
                      end
                    when :read, :update then
                      if app_status == 200
                        :ok
                      elsif app_status == 300
                        :bad_request
                      elsif app_status == 400
                        :unauthorized
                      elsif app_status == 500
                        :not_found
                      else
                        :unprocessable_entity
                      end
                    when :delete then
                      app_status
                    when :crud then
                      if app_status == 200
                        :ok
                      elsif app_status == 300
                        :bad_request
                      end
                  end

    render json: ActiveSupport::JSON.encode(status: CONFIG['status_code'][app_status], message: response[:message], data: response[:data]), status: http_status
  end

  private

  def resource_params
    if ['post', 'look'].include?(serializer_options[:controller]) && params[:action] == 'create'
      return params.permit(permitted_attributes)
    end
    params.require(resource_name).permit(permitted_attributes)
  end

  def throw_dry_exception(exception)
    if Rails.env.production? || Rails.env.staging?
      begin
        notifier = Slack::Notifier.new Rails.application.config.SLACK_ERROR_WEBHOOK_URL
        text = "URL: #{request.params} \n\n\n\ User: #{current_user.email} \n\n Message: \n\n\n" + exception.message + "\nBacktrace: \n\n\n" + exception.backtrace.first(5).join("\n")
        res = notifier.post text: text
      rescue StandardError => error
        puts 'slack api failed'
      end
    end
    json_exception(exception)
  end

  def json_exception(exception)
    render_json_response({message: exception.message, status: 300, data: exception.backtrace}, :crud)
  end

  def current_user_from_base
    User.find(doorkeeper_token.resource_owner_id) if doorkeeper_token
  end

  def init_push_notification_for_mss(eachUser, data, message, uid)
    data[:initiator] = uid
    send_PN(data, eachUser, message)
  end

  def init_push_notification_for_platformLC(message, eachUser, post_link)
    data = {}
    data[:activity] = 'homeFeed'
    data[:post_link] = post_link
    send_PN(data, eachUser, message)
  end

  def send_PN(data, eachUser, message)
    data[:title] = 'Bataz'
    data[:body] = message
    options = {}
    options[:priority] = 'high'
    options[:collapse_key] = 'updated_score'
    options[:data] = data
    fcm = FCM.new(Rails.application.secrets.firebase_server_key, timeout: 5)
    Rails.logger.info "FCM=====================#{fcm}=========================="
    registration_ids = [eachUser.firebasePNId]
    response = fcm.send(registration_ids, options)
    Rails.logger.info "RESPONSE=====================#{response}=========================="
  end
end