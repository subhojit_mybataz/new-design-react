# frozen_string_literal: true

class Api::V1::UsersController < Api::ApiBaseController
  METHOD_TO_SKIP = %i[register authentication social_register getRightSidebarElements social_authentication confirm_auth_callback getProfile get_country].freeze

  skip_before_action :doorkeeper_authorize!, only: METHOD_TO_SKIP
  skip_before_action :set_current_user, only: METHOD_TO_SKIP
  require 'fcm'
  require 'jwt'
  require 'open-uri'

  def resource_name
    :user
  end

  def index_preloads; end

  def show_preloads; end

  def serializer_options
    {
      method: 'show',
      controller: 'user'
    }
  end

  def permitted_attributes
    user_types_attributes = %i[user_type id]
    [:id, :email, :fname, :lname, :name, :password, :password_confirmation, :gender, :country_id, :city, :dob, :phone, :username, :bio, user_type_ids: [], metadata: [preferences: []]]
  end

  def index_serializer_options
    {
      method: 'index',
      controller: 'user'
    }
  end

  def register
    response = User.register(params[:user])
    binding.pry
    if response[:status] == 200
      user = User.find(response[:data][:id])
      sign_in(:user, user)
      user.follow_mybataz
    end
    render_json_response(response, :create)
  end

  def social_register
    render_json_response(User.social_register(params), :create)
  end

  def authentication
    response = User.login(params[:user])

    if response[:status] == 200
      sign_in(:user, User.find(response[:data][:id]))
    end
    render_json_response(response, :update)
  end

  def social_authentication
    render_json_response(User.social_login(params), :update)
  end

  def confirm_auth_callback
    resp = {}
    auth_code = params[:code]
    resp[:code] = auth_code
    registered_user_id = Doorkeeper::AccessGrant.find_by_token auth_code
    user = User.find(registered_user_id.resource_owner_id)
    resp[:fb_t] = user.firebase_auth_token
    resp[:uid] = user.id
    resp[:status] = 'success'
    render json: resp.to_json
  end

  # def getFollowers
  #   resp = {}
  #   resp = current_user_from_base.followers(User).map do |f|
  #     temp = {}
  #     temp[:id] = f.id
  #     temp[:name] = f.full_name
  #     temp[:fname] = f.fname || " "
  #     temp[:lname] = f.lname || " "
  #     temp[:email] = f.email
  #     temp
  #   end
  #   render json: resp.to_json
  # end

  def get_follow_notification_details
    response = {}
    follow_notifications = []
    response = {}
    hash_response = {}
    all_notifications = Notification.where(['recipient_id=? and trackable_type=?', current_user_from_base.id, Notification::TRACKABLE_TYPE_USER]).order('created_at DESC').limit(20)
    all_notifications.each do |notif|
      hash_response = {}
      hash_response[:trackable_type] = notif.trackable_type
      hash_response[:trackable_id] = notif.trackable_id
      hash_response[:owner_type] = notif.owner_type
      hash_response[:owner_id] = notif.owner_id
      hash_response[:owner_name] = User.find(notif.owner_id).fname
      hash_response[:owner_pic] = get_user_profile_pic(notif.owner_id, 'thumb')
      hash_response[:activity_type] = notif.activity_type
      hash_response[:read] = notif.read
      hash_response[:notification_id] = notif.id
      hash_response[:created_at] = time_ago_in_words(notif.created_at) + ' ago'
      hash_response[:following] = current_user_from_base.follows?(User.find(notif.owner_id))
      follow_notifications.push(hash_response)
    end
    response[:follow_notifications] = follow_notifications
    render json: response
  end

  def getProfile
    resp = {}
    user = User.find params[:id]
    resp[:id] = user.id
    resp[:name] = user.full_name
    resp[:email] = user.email
    resp[:following] = !current_user_from_base.nil? ? current_user_from_base.follows?(user) : false
    resp[:profile_user_following_current_user] = !user.nil? && !current_user_from_base.nil? ? user.follows?(current_user_from_base) : false
    resp[:mutual_follower] = (resp[:following] == true) && (resp[:profile_user_following_current_user] == true) ? true : false
    resp[:followers] = user.followers(User).count
    resp[:post_count] = Post.where(user_id: user.id).count
    resp[:img_url] = get_user_profile_pic(user.id, 'fixed_dimension')
    resp[:brand_score] = user.brand_score
    resp[:lifestyle_score] = user.lifestyle_score
    resp[:place] = user.get_location
    resp[:gurus] = user.total_guru_badges
    resp[:ambassadors] = user.total_ambassador_badges
    resp[:bataz_icon_user] = user.bataz_icon_user
    render_json_response({ status: 200, message: 'SUCCESS', data: JSON.parse(resp.to_json) }, :read)
  end

  def new_profile_pic
    user = current_user_from_base
    user.image = params[:file]
    user.save
  end

  def follow_user
    unless params[:id].present?
      render_json_response({ message: 'id is missing', status: 500, data: nil }, :update)
      return
    end
    user_to_follow = User.find params[:id]
    current_user_from_base.toggle_follow!(user_to_follow)
    render_json_response({ message: 'SUCCESS', status: 200, data: { user: ActiveModel::SerializableResource.new(user_to_follow, method: 'index').serializable_hash } }, :show)
  end

  def sendEmail
    resp = {}
    emails = []
    emails = JSON.parse(params[:email])
    for email in emails
      Mailer.invite_shopping_spree(email).deliver_later
      PendingInvitee.create(emailId: email, ShoptazId: params[:sspKey], InitiatorId: params['initiator'])
    end
    resp[:success] = true
    resp[:email] = email
    render json: resp.to_json
  end

  def send_push_notification
    data = {}
    data[:activity] = 'MyShoppingSpree'
    data[:sspKey] = params[:sspKey]
    data[:groupName] = params[:groupName]
    data[:members] = params[:members]
    data[:createdAt] = params[:createdAt]
    if !params[:email].nil?
      uid = params[:initiator]
      initiator = User.find(uid).full_name
      iEmail = User.find(uid).email
      emails = []
      emails = JSON.parse(params[:email])
      message = 'Your friend ' + initiator + ' has invited you to help them shop'
      data[:body] = message
      data[:initiator] = uid
      for email in emails
        if email != iEmail
          eachUser = User.find_by_email(email)
          init_push_notification_for_mss(eachUser, data, message, uid)
        end
      end
    else
      uid = params[:uid]
      sentBy = params[:sender]
      message = params[:message]
      sender = User.find(sentBy).full_name
      if message == 'liked'
        message = sender + ' has liked you image'
      elsif message == 'disliked'
        message = sender + ' has disliked your image'
      elsif message == 'commented'
        message = sender + ' has commented on your image'
      end
      eachUser = User.find(uid)
      init_push_notification_for_mss(eachUser, data, message, uid)
    end

    render json: @response.to_json
  end

  def get_country
    counties = []
    countries = Country.all.map do |country|
      temp = {}
      temp[:name] = country.name
      temp[:id] = country.id
      temp
    end
    render json: countries.to_json
  end

  def store_app_version
    current_user_from_base.app_version = params[:app_version]
    current_user_from_base.save
    resp = {}
    resp[:status] = 'success'
    render json: resp.to_json
  end

  def store_firebase_push_notif_id
    current_user_from_base.firebasePNId = params[:pnId]
    current_user_from_base.save
    resp = {}
    resp[:status] = 'success'
    render json: resp.to_json
  end

  def getProfilePic
    resp = {}
    resp[:profile_pic] = current_user_from_base.nil? ? '' : get_user_profile_pic(current_user_from_base.id, 'thumb')
    resp[:status] = 'success'
    render json: resp.to_json
  end

  def send_invite
    resp = {}
    emails = []
    emails = params[:email].split(',')
    for email in emails
      Mailer.send_invite_mail(email).deliver_later
    end
    resp[:success] = true
    resp[:email] = email
    render json: resp.to_json
  end

  def getRightSidebarElements
    resp = {}
    # change name of BatazEvent here
    element_list = BatazEvent.all
    elements = []
    elements = element_list.map(&:get_element)
    resp[:elements] = elements
    resp[:status] = 'success'
    render json: resp.to_json
  end

  def referer_link
    render_json_response({ message: 'SUCCESS', status: 200, data: { referer_link: current_user_from_base.referer_code } }, :crud)
  end

  def send_referer_link
    unless params[:email].present?
      render_json_response({ message: 'email is missing', status: 500, data: nil }, :update)
      return
    end
    current_user_from_base.send_referer_link(params[:email])
    render_json_response({ message: 'SUCCESS', status: 200, data: nil }, :crud)
  end

  # def show
  #   user = current_user_from_base
  #   render_json_response({status: 200, message: nil, data: user.as_json}, :read)
  # end

  def update_profile_pic
    user = current_user_from_base
    user.image = params[:files]
    if user.save
      render_json_response({ status: 200, message: I18n.t('user.updated'), data: { id: user.id, image: user.image.url } }, :create)
    else
      render_json_response({ status: 300, message: I18n.t('user.error'), data: nil }, :create)
    end
  end

  def signout
    user = current_user_from_base
    user.update(random_auth_token: nil, firebase_auth_token: nil, firebasePNId: nil)
    sign_out(user)
    render_json_response({ status: 200, message: I18n.t('user.signout'), data: nil }, :update)
  end

  def change_password
    user = current_user_from_base
    if user.present?
      if user.valid_password?(params[:data][:password])
        user.update(password: params[:data][:new_password], password_confirmation: params[:data][:confirm_password],
                    random_auth_token: nil, firebase_auth_token: nil, firebasePNId: nil)
        render_json_response({ status: 200, message: 'Password changed successfully.', data: nil }, :update)
      else
        render_json_response({ status: 300, message: 'Password is incorrect.', data: nil }, :update)
      end
    else
      render_json_response({ status: 300, message: 'User account does not exist.', data: nil }, :update)
    end
  end
end
