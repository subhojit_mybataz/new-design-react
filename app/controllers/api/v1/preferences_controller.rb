# frozen_string_literal: true

class Api::V1::PreferencesController < Api::ApiBaseController
  def resource_name
    :preference
  end

  def permitted_attributes
    %i[id preference]
  end

  def index_preloads
    []
  end

  def show_preloads
    []
  end

  def serializer_options
    {
      method: 'show',
      controller: 'preference'
    }
  end

  def index_serializer_options
    {
      method: 'index',
      controller: 'preference'
    }
  end
end
