# frozen_string_literal: true

class Api::V1::PostsController < Api::V1::StoriesController
  def resource_name
    :post
  end

  def new_post
    response = {}
    posts = []
    hash_response = {}
    hash_response[:user_name] = current_user_from_base.full_name
    hash_response[:user_id] = current_user_from_base.id
    hash_response[:profile_pic] = get_user_profile_pic(current_user_from_base.id, "thumb")
    response[:post] = hash_response
    render json: response
  end

  def serializer_options
    super.merge({controller: 'post'})
  end

  def index_serializer_options
    super.merge({controller: 'post'})
  end
end