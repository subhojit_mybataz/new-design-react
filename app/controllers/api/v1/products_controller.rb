class Api::V1::ProductsController < Api::ApiBaseController
  def resource_name
    :product
  end

  def index_preloads
    []
  end

  def index_serializer_options
    {
      method: 'index',
      controller: 'product'
    }
  end 

  def serializer_options
    {
      method: 'show',
      controller: 'product'
    }
  end
end
