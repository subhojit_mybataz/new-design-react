# frozen_string_literal: true

class Api::V1::EmoticonsController < Api::ApiBaseController
  def resource_name
    :emoticon
  end

  def permitted_attributes
    %i[id name url]
  end

  def index_preloads
    []
  end

  def show_preloads
    []
  end

  def serializer_options
    {
      method: 'show',
      controller: 'emoticon'
    }
  end

  def index_serializer_options
    {
      method: 'index',
      controller: 'emoticon'
    }
  end
end
