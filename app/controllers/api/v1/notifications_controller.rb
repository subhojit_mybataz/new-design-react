# frozen_string_literal: true

class Api::V1::NotificationsController < Api::ApiBaseController

  def read
    notifications = Notification.where(id: resource_params[:ids]).update_all(read: true)
    render_json_response({ message: 'SUCCESS', status: 200, data: { :notifications => resource_params[:ids] } }, :update)
  end

  def resource_name
    :notification
  end

  def permitted_attributes
    [:read, ids:[]]
  end

  def index_preloads
    []
  end

  def show_preloads
    []
  end

  def serializer_options
    {
      method: 'show',
      controller: 'notification'
    }
  end

  def index_serializer_options
    {
      method: 'index',
      controller: 'notification'
    }
  end
end
