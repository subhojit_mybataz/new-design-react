# frozen_string_literal: true

class Api::V1::BrandsController < Api::ApiBaseController
  skip_before_filter :doorkeeper_authorize!, only: [:get_trending_info]

  def resource_name
    :brand
  end

  def index_preloads
    []
  end

  def show_preloads;
  end

  def index_serializer_options
    {
      method: 'index',
      controller: 'brand',
      trending_images_count: params[:trending_images_count]
    }
  end

  def serializer_options
    {
      method: 'show',
      controller: 'brand'
    }
  end

  def show
    resource = Brand.eager_load(show_preloads).friendly.find(params[:id].downcase)
    authorize! :show, resource
    render_json_response({message: 'SUCCESS', status: 200, data: {resource_name.to_sym => ActiveModel::SerializableResource.new(resource, serializer_options).serializable_hash}}, :show)
  end

  def followBrand
    resp = {}

    brand_to_follow = Brand.friendly.find(params[:id])
    current_user_from_base.toggle_follow!(brand_to_follow)

    resp[:id] = params[:id]
    resp[:success] = true

    render json: resp.to_json
  end

  def getAllBrandProducts
    resp = {}
    brands = []
    products = []
    brands = Brand.all.map do |brand|
      temp = {}
      temp['brand_id'] = brand.id
      temp['brand_name'] = brand.name
      temp
    end
    products = Product.all.map do |product|
      temp = {}
      temp['product_id'] = product.id
      temp['product_name'] = product.name
      temp
    end
    resp[:brand] = brands
    resp[:product] = products
    render json: resp.to_json
  end
end
