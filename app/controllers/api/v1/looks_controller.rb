# frozen_string_literal: true

class Api::V1::LooksController < Api::V1::StoriesController
  def resource_name
    :look
  end

  def serializer_options
  	super.merge({controller: 'look'})
  end

  def index_serializer_options
    super.merge({controller: 'look'})
  end
end
