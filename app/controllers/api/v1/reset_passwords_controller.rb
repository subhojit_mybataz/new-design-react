# frozen_string_literal: true

class Api::V1::ResetPasswordsController < Api::ApiBaseController
  skip_before_action :doorkeeper_authorize!, only: [:index]

  def index
    user = User.find_by_email(user_params)
    if user.present?
      user.send_reset_password_instructions
      render_json_response({ status: 200, message: I18n.t('reset_password.success'), data: { id: user.id } }, :read)
    else
      render_json_response({ status: 300, message: I18n.t('reset_password.error'), data: nil }, :read)
    end
  end

  private

  def user_params
    params.require(:email)
  end
end
