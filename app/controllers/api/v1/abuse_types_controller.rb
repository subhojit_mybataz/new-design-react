# frozen_string_literal: true

class Api::V1::AbuseTypesController < Api::ApiBaseController
  skip_before_action :doorkeeper_authorize!, only: [:index]

  def resource_name
    :abuse_type
  end

  def index_preloads
    []
  end

  def index_serializer_options
    {
      method: 'index',
      controller: 'abuse_type',
    }
  end
end
