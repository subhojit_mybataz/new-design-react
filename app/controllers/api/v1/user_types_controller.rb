# frozen_string_literal: true

class Api::V1::UserTypesController < Api::ApiBaseController
  def resource_name
    :user_type
  end

  def permitted_attributes
    %i[id user_type]
  end

  def index_preloads
    []
  end

  def show_preloads
    []
  end

  def serializer_options
    {
      method: 'show',
      controller: 'user_type'
    }
  end

  def index_serializer_options
    {
      method: 'index',
      controller: 'user_type'
    }
  end
end
