# frozen_string_literal: true

class Api::V1::StoriesController < Api::ApiBaseController
  def permitted_attributes
    attachments_attributes = %i[media_file_name media_content_type media_content_size]
    comment_attributes = %i[id comment _destroy]
    report_abuses_attributes = [:abuse_type_id]
    bookmark_posts_attributes = %i[id user_id _destroy]
    save_looks_attributes = %i[id user_id _destroy]
    clap_attribute = [:clap]
    fixed_attributes = [:name, :description, :price, :avenue_to_buy, :detail, brand_ids: [], product_ids: [], images_attributes: [], videos_attributes: []]
    variable_attributes = [:id, comments_attributes: comment_attributes, bookmark_posts_attributes: bookmark_posts_attributes, report_abuses_attributes: report_abuses_attributes, clap_attribute: clap_attribute, save_looks_attributes: save_looks_attributes, emoticon_ids: []]
    if request.method == 'POST' || current_user.story_owner?(params[:id].to_i)
      return fixed_attributes + variable_attributes
    else
      return variable_attributes
    end
  end

  def index_preloads
    [{comments: [:user]}, :claps, :attachments, :emoticons, :brands, :products, {user: [:claps]}]
  end

  def show_preloads
    [{comments: [:user]}, {claps: [:user]}, :attachments, :emoticons, :brands, :products, {user: [:claps]}]
  end

  def serializer_options
    {
      method: 'show',
      action_type: params[:action_type],
      comments_offset: params[:comments_offset],
      comments_limit: params[:comments_limit],
      brands_offset: params[:brands_offset],
      brands_limit: params[:brands_limit],
      products_offset: params[:products_offset],
      products_limit: params[:products_limit],
      claps_offset: params[:claps_offset],
      claps_limit: params[:claps_limit]
    }
  end

  def index_serializer_options
    {
      method: 'index'
    }
  end
end
