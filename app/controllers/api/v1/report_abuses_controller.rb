# frozen_string_literal: true

class Api::V1::ReportAbusesController < Api::ApiBaseController
  respond_to :json

  def create
    report_abuse = ReportAbuse.new(report_abuse_params)

    if report_abuse.save
      response = { status: 200, message: I18n.t('report_abuse.created'), data: { id: report_abuse.id } }
    else
      response = { status: 300, message: I18n.t('report_abuse.error'), data: report_abuse.errors.full_messages }
    end
    render_json_response(response, :create)
  end

  private

  def report_abuse_params
    params.require(:data).permit(:user_id, :abuse_type_id, :reportable_type, :reportable_id)
  end
end
