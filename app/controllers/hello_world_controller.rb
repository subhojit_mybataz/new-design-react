# frozen_string_literal: true

class HelloWorldController < ApplicationController
  def index
    if params[:embedded].nil?
      authenticate_user!
      @hello_world_props = { page: 'feed' }
    elsif params[:embedded] == 'true'
      @hello_world_props = { page: 'feed' }
    end
  end

  def profile; end

  def search; end

  def post_details
    notification_id = params[:notif_id]
    unless notification_id.nil?
      notification = Notification.find(notification_id)
      notification.read = true
      notification.save
    end
  end

  def brand
    request_params = request.url.split('/')
    country = request_params[3]
    brand = request_params[4]
    country_name = Country.find_by_name(country)
    brand_name = Brand.friendly.find(brand)
    # binding.pry
    if country_name.nil? || brand_name.nil?
      redirect_to '/404'
    elsif BrandCountry.where(['brand_id=? and country_id=?', brand_name.id, country_name.id]).empty?
      redirect_to '/404'
    end
  end
end
