# frozen_string_literal: true

class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def facebook
    auth = env['omniauth.auth']
    bataz_icon_user = request.env['omniauth.params']['bataz_icon_user'].nil? ? false : request.env['omniauth.params']['bataz_icon_user']
    @identity = Identity.find_for_oauth env['omniauth.auth']
    @user = @identity.user
    values = {}
    if @user.nil?
      values[:email] = auth.info.email
      values[:id] = auth.uid
      values[:provider] = auth.provider
      values[:first_name] = auth.extra.raw_info.first_name
      values[:last_name] = auth.extra.raw_info.last_name
      values[:bataz_icon_user] = bataz_icon_user
      facebook_auth_helper(values)
    end

    #
    # cookies[:name] = {
    #   :value => {"access_token" => auth.credentials.token, "user_id" => @user.id},
    #   :expires => 1.year.from_now,
    #   :domain => 'http://localhost:3000'
    # }

    # require 'net/http'
    #
    # url = URI.parse("http://localhost:3000/oauth/authorize?client_id=d56970206fcf4eb84a90b6e45e9909a41affd1fe8e9dc30e25215c03d6485e96&redirect_uri=http://localhost:3000/api/v1/user/auth_code_callback&e=#{@user.email}&t=#{@user.random_auth_token}&response_type=code")
    # req = Net::HTTP::Get.new(url.to_s)
    # res = Net::HTTP.start(url.host, url.port) {|http|
    #   http.request(req)
    # }
    # puts res.body
    #
    # binding.pry
    #
    # params = {
    #   "client_id" => "d56970206fcf4eb84a90b6e45e9909a41affd1fe8e9dc30e25215c03d6485e96",
    #   "client_secret" => "5523bfc10035536d3a65327045a96004e90f3fbe508c8fc3db999392d23296ae",
    #   "grant_type" => "authorization_code",
    #   "redirect_uri" => "http://localhost:3000/api/v1/user/auth_code_callback",
    #   "code" => "d6a9de4977bbeffe9601e7445e0a8907153098f0e055503fd8a630c8fd284a9d"
    # }
    # x = Net::HTTP.post_form(URI.parse('http://localhost:3000/oauth/token'), params.to_json)
    # puts x.body
    #

    sign_in_and_redirect @user, event: :authentication
  end

  def failure
    redirect_to root_path
  end

  def google_oauth2
    auth = env['omniauth.auth']
    bataz_icon_user = request.env['omniauth.params']['bataz_icon_user'].nil? ? false : request.env['omniauth.params']['bataz_icon_user']
    @identity = Identity.find_for_oauth env['omniauth.auth']
    @user = @identity.user
    values = {}
    if @user.nil?
      values[:email] = auth.info.email
      values[:id] = auth.uid
      values[:provider] = auth.provider
      values[:first_name] = auth.info.first_name
      values[:last_name] = auth.info.last_name
      values[:bataz_icon_user] = bataz_icon_user
      auth_helper(values)
    end
    sign_in_and_redirect @user, event: :authentication
  end

  def instagram
    auth = env['omniauth.auth']
    bataz_icon_user = request.env['omniauth.params']['bataz_icon_user'].nil? ? false : request.env['omniauth.params']['bataz_icon_user']
    @identity = Identity.find_for_oauth env['omniauth.auth']
    @user = @identity.user
    values = {}
    if @user.nil?
      values[:email] = auth.info.email
      values[:id] = auth.uid
      values[:provider] = auth.provider
      values[:first_name] = auth.info.first_name
      values[:last_name] = auth.info.last_name
      values[:bataz_icon_user] = bataz_icon_user
      auth_helper(values)
    end
    sign_in_and_redirect @user, event: :authentication
  end
end
