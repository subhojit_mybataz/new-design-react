# frozen_string_literal: true

class Users::RegistrationsController < Devise::RegistrationsController
  # before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  # def new
  #   super
  # end

  # POST /resource
  # def create
  #   super
  # end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   super
  # end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_up_params
  #   devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute])
  # end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end

  def after_sign_up_path_for(_resource)
    root_path + '?from_registration=true'
  end

  def create
    build_resource(registration_params)
    resName = resource.fname
    splitNames = []
    splitNames = resName.split(' ', 2)
    resource.fname = splitNames[0]
    resource.lname = splitNames[1]
    resource.bataz_icon_user = params[:user][:bataz_icon_user].nil? ? false : params[:user][:bataz_icon_user]

    if resource.save
      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up if is_navigational_format?
        sign_up(resource_name, resource)
        respond_with resource, location: after_sign_up_path_for(resource)
      else
        set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_navigational_format?
        respond_with resource, location: after_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      respond_with resource
    end
  end

  def bataz_icon_sign_up
    render 'devise/registrations/bataz_icon_reg'
  end

  def update
    user = current_user_from_base

    splitNames = []
    rName = registration_params[:fname]
    splitNames = rName.split(' ', 2)

    user.fname = splitNames[0]
    user.lname = splitNames[1]
    user.gender = registration_params[:gender]
    user.country_id = registration_params[:country_id]
    user.city = registration_params[:city]
    user.dob = registration_params[:dob]
    user.save

    redirect_to root_path
  end

  private

  def registration_params
    params.require(:user).permit(:email, :fname, :lname, :password, :password_confirmation, :gender, :country_id, :city, :dob)
  end
end
