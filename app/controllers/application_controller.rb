# frozen_string_literal: true

class ApplicationController < ActionController::Base
  # before_filter :prepare_exception_notifier

  include ApplicationHelper

  protect_from_forgery with: :exception

  def authenticate_admin_user!
    redirect_to root_url unless (current_user.nil? != true) && (current_user.admin == true)
  end

  def set_current_user
    User.current = current_user_from_base
  end

  private

  def prepare_exception_notifier
    request.env['exception_notifier.exception_data'] = {
      current_user: current_user
    }
  end

  def authenticate_with_doorkeeper_or_devise!
    if request.headers['Authorization'].present?
      doorkeeper_authorize!
      @current_user = User.find(doorkeeper_token[:resource_owner_id])
    else
      authenticate_user!
    end
  end
end
