# frozen_string_literal: true

ActiveAdmin.register ReportAbuse do
  actions :all, except: %i[edit destroy]
  member_action :remove do
    resource = ReportAbuse.find(params[:id])
    resource.current_user = current_user
    resource.remove_resource
    resource.update(is_reviewed: true)
    redirect_to collection_path, notice: 'The resource have been removed.'
  end

  batch_action :remove do |ids|
    batch_action_collection.find(ids).each do |resource|
      resource.current_user = current_user
      resource.remove_resource
      resource.update(is_reviewed: true)
    end
    redirect_to collection_path, alert: 'The resources have been removed.'
  end

  member_action :send_warning do
    resource = ReportAbuse.find(params[:id])
    resource.current_user = current_user
    resource.send_warning_to_resource
    redirect_to collection_path, notice: 'The resource have been warned.'
  end

  batch_action :send_warning do |ids|
    batch_action_collection.find(ids).each do |resource|
      resource.current_user = current_user
      resource.send_warning_to_resource
    end
    redirect_to collection_path, alert: 'The resources have been warned.'
  end

  index do
    selectable_column
    column :id
    column :reportable_type
    column 'Reported For', :reportable
    column 'Reported By', :user
    column :abuse_type
    column 'Action Taken' do |report_abuse|
      actions = report_abuse.report_abuse_actions
      actions.blank? ? '-' : actions.pluck(:action).join(' -> ')
    end
    column :created_at
    column :is_reviewed
    actions do |report_abuse|
      (link_to 'Warning', send_warning_admin_report_abus_path(report_abuse)) + ' ' +
        (link_to "Remove #{report_abuse.reportable_type}", remove_admin_report_abus_path(report_abuse))
    end
  end

  show do
    attributes_table do
      row :id
      row :reportable_type
      row :reportable
      row :user
      row :abuse_type
      row :created_at
      row :updated_at
      row :is_reviewed
    end

    panel 'Action Taken' do
      table_for report_abuse.report_abuse_actions do
        column 'Action', :action
        column 'Taken On', :created_at
      end
    end
  end
end
