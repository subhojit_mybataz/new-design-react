# frozen_string_literal: true

ActiveAdmin.register Country do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  remove_filter :created_at, :updated_at, :brand_countries, :brands

  permit_params :name
  show do
    attributes_table :name
    active_admin_comments
  end

  index do
    selectable_column
    column :id
    column :name
    actions
  end
end
