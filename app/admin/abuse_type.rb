# frozen_string_literal: true

ActiveAdmin.register AbuseType do
  permit_params :name, :description, :abuse_type_for
  actions :all, except: [:destroy]

  form do |f|
    f.inputs do
      f.input :name
      f.input :description
      f.input :abuse_type_for, label: 'Abuse Type For', as: :select, collection: get_abuse_type_for_list, include_blank: false
    end
    f.actions
  end
end
