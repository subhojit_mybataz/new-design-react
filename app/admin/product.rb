# frozen_string_literal: true

ActiveAdmin.register Product do
  remove_filter :created_at, :updated_at
  permit_params :name, :preferred_for, :priority

  show do
    attributes_table do
      row :name
      row :preferred_for do |b|
        b.preferred_for == 'm' ? 'Male' : (b.preferred_for == 'f' ? 'Female' : 'All')
      end
      row :priority
      row :posts_count
      row :looks_count
      row :claps_count
      row :comments_count
    end
  end

  index do
    selectable_column
    column :id
    column :name
    column :preferred_for do |b|
      b.preferred_for == 'm' ? 'Male' : (b.preferred_for == 'f' ? 'Female' : 'All')
    end
    column :priority
    column :claps_count
    column :comments_count
    column :posts_count
    column :looks_count
    actions
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs do
      f.input :name
      f.input :preferred_for, :as => :select, :collection => [['Male', 'm'], ['Female', 'f'], ['All', 'all']]
      f.input :priority, :as => :select, :collection => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
    end
    f.actions
  end
end
