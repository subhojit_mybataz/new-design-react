# frozen_string_literal: true

ActiveAdmin.register MarketingMessage do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  actions :all, except: %i[destroy new]

  permit_params :message

  index do
    selectable_column
    column :id
    column :message
    actions
  end

  show do
    attributes_table do
      row :message
    end
  end
end
