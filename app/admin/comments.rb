# frozen_string_literal: true

ActiveAdmin.register Comment do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  actions :all, except: %i[new edit]

  index do
    selectable_column
    column 'Post ID', :commentable_id
    column :comment
    column :user_id
    column :created_at
    column :updated_at
    actions
  end
end
