# frozen_string_literal: true

ActiveAdmin.register_page 'Dashboard' do
  menu priority: 1, label: proc { I18n.t('active_admin.dashboard') }

  content title: proc { I18n.t('active_admin.dashboard') } do
    analytics = []
    analytics_with_no_range = []

    daily = Time.zone.now.beginning_of_day..Time.zone.now.end_of_day
    weekly = 1.week.ago.beginning_of_day..Time.zone.now.end_of_day
    monthly = 1.month.ago.beginning_of_day..Time.zone.now.end_of_day
    quarterly = 3.month.ago.beginning_of_day..Time.zone.now.end_of_day
    yearly = 1.year.ago.beginning_of_day..Time.zone.now.end_of_day
    all_users_id = User.pluck(:id).freeze

    def getAverage(array)
      if array.present?
        array.sum / array.count
      else
        0
      end
    end

    # TOTAL USERS FROM TIME RANGE (ignoring guest users)
    total_users = {}
    total_users[:metric] = 'Total users'
    total_users[:daily] = Visit.select(:user_id).distinct(:user_id).where(started_at: daily).count
    total_users[:weekly] = Visit.select(:user_id).distinct(:user_id).where(started_at: weekly).count
    total_users[:monthly] = Visit.select(:user_id).distinct(:user_id).where(started_at: monthly).count
    total_users[:quarterly] = Visit.select(:user_id).distinct(:user_id).where(started_at: quarterly).count
    total_users[:yearly] = Visit.select(:user_id).distinct(:user_id).where(started_at: yearly).count
    analytics.push(total_users)

    # TODO: DAILY ACTIVE USERS - get from visits

    # DAILY NEW USERS
    new_users = {}
    new_users[:metric] = 'New users'
    new_users[:daily] = User.where(created_at: daily).count
    new_users[:weekly] = User.where(created_at: weekly).count
    new_users[:monthly] = User.where(created_at: monthly).count
    new_users[:quarterly] = User.where(created_at: quarterly).count
    new_users[:yearly] = User.where(created_at: yearly).count
    analytics.push(new_users)

    # TODO: AVERAGE FREQUENCY LOGIN - get from visits

    # AVERAGE POST BY USER
    average_posts_from_user = {}
    average_posts_from_user[:metric] = 'Average Post by user'
    number_of_posts_daily = Post.where(created_at: daily).group(:user_id).count.values
    number_of_posts_weekly = Post.where(created_at: weekly).group(:user_id).count.values
    number_of_posts_monthly = Post.where(created_at: monthly).group(:user_id).count.values
    number_of_posts_quarterly = Post.where(created_at: quarterly).group(:user_id).count.values
    number_of_posts_yearly = Post.where(created_at: yearly).group(:user_id).count.values

    average_posts_from_user[:daily] = getAverage(number_of_posts_daily)
    average_posts_from_user[:weekly] = getAverage(number_of_posts_weekly)
    average_posts_from_user[:monthly] = getAverage(number_of_posts_monthly)
    average_posts_from_user[:quarterly] = getAverage(number_of_posts_quarterly)
    average_posts_from_user[:yearly] = getAverage(number_of_posts_yearly)
    analytics.push(average_posts_from_user)

    # TODO: FREQUENCY OF POST BY USER

    # AVERAGE BRAND POINTS
    number_of_points_daily = Point.where.not(brand_id: nil).where(created_at: daily).group(:user_id).count.values
    number_of_points_weekly = Point.where.not(brand_id: nil).where(created_at: weekly).group(:user_id).count.values
    number_of_points_monthly = Point.where.not(brand_id: nil).where(created_at: monthly).group(:user_id).count.values
    number_of_points_quarterly = Point.where.not(brand_id: nil).where(created_at: quarterly).group(:user_id).count.values
    number_of_points_yearly = Point.where.not(brand_id: nil).where(created_at: yearly).group(:user_id).count.values
    # AVERAGE POINTS
    avg_brand_points = {}
    avg_brand_points[:metric] = 'Average brand score per user'
    avg_brand_points[:daily] = getAverage(number_of_points_daily)
    avg_brand_points[:weekly] = getAverage(number_of_points_weekly)
    avg_brand_points[:monthly] = getAverage(number_of_points_monthly)
    avg_brand_points[:quarterly] = getAverage(number_of_points_quarterly)
    avg_brand_points[:yearly] = getAverage(number_of_points_yearly)
    analytics.push(avg_brand_points)
    # TOP POINTS
    top_brand_points = {}
    top_brand_points[:metric] = 'Top brand score'
    top_brand_points[:daily] = number_of_points_daily.compact.max || 0
    top_brand_points[:weekly] = number_of_points_weekly.compact.max || 0
    top_brand_points[:monthly] = number_of_points_monthly.compact.max || 0
    top_brand_points[:quarterly] = number_of_points_quarterly.compact.max || 0
    top_brand_points[:yearly] = number_of_points_yearly.compact.max || 0
    analytics.push(top_brand_points)
    # BOTTOM POINTS
    low_brand_points = {}
    low_brand_points[:metric] = 'Bottom brand score'
    low_brand_points[:daily] = number_of_points_daily.compact.min || 0
    low_brand_points[:weekly] = number_of_points_weekly.compact.min || 0
    low_brand_points[:monthly] = number_of_points_monthly.compact.min || 0
    low_brand_points[:quarterly] = number_of_points_quarterly.compact.min || 0
    low_brand_points[:yearly] = number_of_points_yearly.compact.min || 0
    analytics.push(low_brand_points)

    # AVERAGE LIFESTYLE POINTS
    all_points_daily = Point.where(created_at: daily).group(:user_id).count.values
    all_points_weekly = Point.where(created_at: weekly).group(:user_id).count.values
    all_points_monthly = Point.where(created_at: monthly).group(:user_id).count.values
    all_points_quarterly = Point.where(created_at: quarterly).group(:user_id).count.values
    all_points_yearly = Point.where(created_at: yearly).group(:user_id).count.values
    # AVERAGE POINTS
    avg_lifestyle_points = {}
    avg_lifestyle_points[:metric] = 'Average lifestyle score per user'
    avg_lifestyle_points[:daily] = getAverage(all_points_daily)
    avg_lifestyle_points[:weekly] = getAverage(all_points_weekly)
    avg_lifestyle_points[:monthly] = getAverage(all_points_monthly)
    avg_lifestyle_points[:quarterly] = getAverage(all_points_quarterly)
    avg_lifestyle_points[:yearly] = getAverage(all_points_yearly)
    analytics.push(avg_lifestyle_points)
    # TOP POINTS
    top_lifestyle_points = {}
    top_lifestyle_points[:metric] = 'Top lifestyle score'
    top_lifestyle_points[:daily] = all_points_daily.compact.max || 0
    top_lifestyle_points[:weekly] = all_points_weekly.compact.max || 0
    top_lifestyle_points[:monthly] = all_points_monthly.compact.max || 0
    top_lifestyle_points[:quarterly] = all_points_quarterly.compact.max || 0
    top_lifestyle_points[:yearly] = all_points_yearly.compact.max || 0
    analytics.push(top_lifestyle_points)
    # BOTTOM POINTS
    low_lifestyle_points = {}
    low_lifestyle_points[:metric] = 'Bottom lifestyle score'
    low_lifestyle_points[:daily] = all_points_daily.compact.min || 0
    low_lifestyle_points[:weekly] = all_points_weekly.compact.min || 0
    low_lifestyle_points[:monthly] = all_points_monthly.compact.min || 0
    low_lifestyle_points[:quarterly] = all_points_quarterly.compact.min || 0
    low_lifestyle_points[:yearly] = all_points_yearly.compact.min || 0
    analytics.push(low_lifestyle_points)

    # AVERAGE NUMBER OF BRANDS PER USERS
    brands_per_users_daily = Post.distinct(:brand_id).select(:brand_id).where(created_at: daily).group(:user_id).count.values
    brands_per_users_weekly = Post.distinct(:brand_id).select(:brand_id).where(created_at: weekly).group(:user_id).count.values
    brands_per_users_monthly = Post.distinct(:brand_id).select(:brand_id).where(created_at: monthly).group(:user_id).count.values
    brands_per_users_quarterly = Post.distinct(:brand_id).select(:brand_id).where(created_at: quarterly).group(:user_id).count.values
    brands_per_users_yearly = Post.distinct(:brand_id).select(:brand_id).where(created_at: yearly).group(:user_id).count.values

    avg_brand_per_user = {}
    avg_brand_per_user[:metric] = 'Average number of brands per users'
    avg_brand_per_user[:daily] = getAverage(brands_per_users_daily)
    avg_brand_per_user[:weekly] = getAverage(brands_per_users_weekly)
    avg_brand_per_user[:monthly] = getAverage(brands_per_users_monthly)
    avg_brand_per_user[:quarterly] = getAverage(brands_per_users_quarterly)
    avg_brand_per_user[:yearly] = getAverage(brands_per_users_yearly)
    analytics.push(avg_brand_per_user)

    # NUMBER OF USERS FROM USER TO AMBASSADOR
    user_to_ambassador = {}
    user_to_ambassador[:metric] = 'No. of users to ambassadors'
    user_to_ambassador[:daily] = Ahoy::Event.distinct(:user_id).where(name: Badge::AHOY_EVENT_NEW_AMBASSADOR).where(time: daily).count
    user_to_ambassador[:weekly] = Ahoy::Event.distinct(:user_id).where(name: Badge::AHOY_EVENT_NEW_AMBASSADOR).where(time: weekly).count
    user_to_ambassador[:monthly] = Ahoy::Event.distinct(:user_id).where(name: Badge::AHOY_EVENT_NEW_AMBASSADOR).where(time: monthly).count
    user_to_ambassador[:quarterly] = Ahoy::Event.distinct(:user_id).where(name: Badge::AHOY_EVENT_NEW_AMBASSADOR).where(time: quarterly).count
    user_to_ambassador[:yearly] = Ahoy::Event.distinct(:user_id).where(name: Badge::AHOY_EVENT_NEW_AMBASSADOR).where(time: yearly).count
    analytics.push(user_to_ambassador)

    # NUMBER OF USERS FROM AMBASSADOR TO GURU
    ambassador_to_guru = {}
    ambassador_to_guru[:metric] = 'No. of users to ambassadors'
    ambassador_to_guru[:daily] = Ahoy::Event.distinct(:user_id).where(name: Badge::AHOY_EVENT_NEW_GURU).where(time: daily).count
    ambassador_to_guru[:weekly] = Ahoy::Event.distinct(:user_id).where(name: Badge::AHOY_EVENT_NEW_GURU).where(time: weekly).count
    ambassador_to_guru[:monthly] = Ahoy::Event.distinct(:user_id).where(name: Badge::AHOY_EVENT_NEW_GURU).where(time: monthly).count
    ambassador_to_guru[:quarterly] = Ahoy::Event.distinct(:user_id).where(name: Badge::AHOY_EVENT_NEW_GURU).where(time: quarterly).count
    ambassador_to_guru[:yearly] = Ahoy::Event.distinct(:user_id).where(name: Badge::AHOY_EVENT_NEW_GURU).where(time: yearly).count
    analytics.push(ambassador_to_guru)

    panel 'Analytics with time_range' do
      table_for analytics do
        column 'Metric', :metric
        column 'Daily', :daily
        column 'Weekly', :weekly
        column 'Monthly', :monthly
        column 'Quarterly', :quarterly
        column 'Yearly', :yearly
      end
    end

    # ====================================================================
    # ANALYTICS WITH NO RANGE OF TIMELINES LIKE YEARLY, MONTHLY OR WEEKLY
    # ====================================================================

    # TOTAL ATTRITION - USERS WHO HAVE NOT POSTED FOR 30 DAYS +
    # USERS WHO HAVE NOT LOGGED IN FOR 30 DAYS + APP UNINSTALLS[not hanlding ]
    churn = {}
    churn[:metric] = 'Churn (No. of users who not logged-in/posted for 30days)'
    users_posted = Post.select(:user_id).distinct.pluck(:user_id)
    users_not_posted_for_30days = all_users_id - users_posted
    users_not_logged_in_for_30days = Visit.where.not(started_at: monthly).pluck(:user_id)
    churn[:column1] = (users_not_posted_for_30days + users_not_logged_in_for_30days).uniq.count
    analytics_with_no_range.push(churn)

    # NO. OF GURUS AND AMBASSADORS
    gurus_and_ambassadors = {}
    gurus_and_ambassadors[:metric] = 'No. of Gurus and Ambassadors'
    gurus_and_ambassadors[:column1] = '# Gurus : ' + Badge.where(badge_type: Badge::BRAND_GURU).count.to_s
    gurus_and_ambassadors[:column2] = '# Ambassadors : ' + Badge.where(badge_type: Badge::BRAND_AMBASSADOR).count.to_s
    analytics_with_no_range.push(gurus_and_ambassadors)

    panel 'Analytics with no time_range' do
      table_for analytics_with_no_range do
        column 'Metric', :metric
        column '-', :column1
        column '-', :column2
      end
    end

    top_users_with_lifestyle_points = User.where('lifestyle_score > ?', 0).order(:lifestyle_score).reverse.first(20)

    panel 'Top 20  users with most lifestyle points' do
      table_for top_users_with_lifestyle_points do
        column 'user_id', :id
        column 'Name', :fname
        column 'E-mail', :email
        column 'Lifestyle Score', :lifestyle_score
      end
    end

    top_users_with_brand_points = User.where('brand_score > ?', 0).order(:brand_score).reverse.first(20)

    panel 'Top 20  users with most lifestyle points' do
      table_for top_users_with_brand_points do
        column 'user_id', :id
        column 'Name', :fname
        column 'E-mail', :email
        column 'Brand Score', :brand_score
      end
    end
  end # content
end
