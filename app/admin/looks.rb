# frozen_string_literal: true

ActiveAdmin.register Look do
  permit_params :name, :description
  index do
    selectable_column
    column :id
    column :description
    column :user
    column :created_at
    column "Brands" do |look|
      look.brands.pluck(:name).join(",")
    end
    column "Products" do |look|
      look.products.pluck(:name).join(",")
    end
    column "Image" do |look|
      image_tag look.attachments.first.image.url, size: '200x100' unless look.attachments.blank?
    end
    column :actions do |resource|
      (link_to I18n.t('active_admin.view'), resource_path(resource)) + ' ' +
        (link_to I18n.t('active_admin.edit'), edit_resource_path(resource)) + ' ' +
        (link_to "Delete", '#', class: 'look_remove', id: resource.id)
    end
  end

  show do
    attributes_table :name, :description, :user, :created_at, :updated_at, :brand, :product, :custom_brand, :custom_product, :price

    panel 'Look Images' do
      table_for post.attachments do |t|
        t.column('Image') { |attachment| image_tag attachment.media.url, size: '200x100' }
      end
    end

    panel 'Look comments' do
      paginated_collection(Kaminari.paginate_array(post.comments).page(params[:comments_page]).per(10), param_name: 'comments_page') do
        table_for collection do |t|
          t.column('Comment', &:comment)
          t.column('Commented On', &:created_at)
          t.column('Commented By', &:user)
        end
      end
    end
  end
end
