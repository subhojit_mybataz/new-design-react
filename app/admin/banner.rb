# frozen_string_literal: true

ActiveAdmin.register Banner do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  permit_params :image, :brand_country_id
  remove_filter :created_at, :updated_at, :img_url, :image_file_name, :image_content_type, :image_file_size, :image_updated_at

  filter :brand_country, as: :select, collection: proc { get_brand_country }
  filter :active

  index do
    selectable_column
    column :id
    column :active
    column :brand_country do |banner|
      banner.brand_country.brand.name + ' | ' + banner.brand_country.country.name
    end
    actions
  end

  show do
    attributes_table do
      row :image do |banner|
        image_tag banner.image.url, size: '200x100'
      end

      row :active
      row :brand_country do |banner|
        banner.brand_country.brand.name + ' | ' + banner.brand_country.country.name
      end
    end
  end

  form do |f|
    f.inputs do
      if f.object.image.exists?
        f.input :image, as: :file, hint: image_tag(f.object.image.url, size: '200x100')
      else
        f.input :image, as: :file
      end
      f.input :brand_country, label: 'Brand Country', as: :select, collection: get_brand_country, include_blank: false
    end
    f.actions
  end
end
