# frozen_string_literal: true

ActiveAdmin.register Brand do
  remove_filter :created_at, :updated_at, :brand_countries, :countries, :logo_file_name, :logo_content_type,
                :logo_file_size, :logo_updated_at, :banners, :total_guru_badges, :total_ambassador_badges

  controller do
    def find_resource
      scoped_collection.friendly.find(params[:id])
    end
  end

  permit_params :name, :logo, :is_active, :description, :preferred_for, :priority, banners_attributes: %i[id image _destroy]

  show do
    attributes_table do
      row :name
      row :logo do |brand|
        image_tag brand.logo.url, size: '200x100'
      end
      row :preferred_for do |b|
        b.preferred_for == 'm' ? 'Male' : (b.preferred_for == 'f' ? 'Female' : 'All')
      end
      row :priority
      row :is_active
      row :posts_count
      row :looks_count
      row :claps_count
      row :comments_count
    end

    panel 'Banners' do
      table_for brand.banners do |t|
        t.column(:id)
        t.column('Image') {|banner| image_tag banner.image.url, size: '200x100'}
      end
    end
  end

  index do
    selectable_column
    column :id
    column :name
    column :preferred_for do |b|
      b.preferred_for == 'm' ? 'Male' : (b.preferred_for == 'f' ? 'Female' : 'All')
    end
    column :priority
    column :claps_count
    column :comments_count
    column :posts_count
    column :looks_count
    column('Logo') {|brand| image_tag brand.logo.url(:thumb), size: '200x100'}
    actions
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs do
      if f.object.logo.exists?
        f.input :logo, as: :file, hint: image_tag(f.object.logo.url(:resized_image))
      else
        f.input :logo, as: :file
      end
      f.input :name
      f.input :preferred_for, :as => :select, :collection => [['Male', 'm'], ['Female', 'f'], ['All', 'all']]
      f.input :priority, :as => :select, :collection => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
      f.input :is_active
    end

    f.inputs do
      f.has_many :banners, heading: 'Banners', allow_destroy: true do |a|
        if a.object.image.exists?
          a.input :image, as: :file, hint: image_tag(a.object.image.url, size: '200x100')
        else
          a.input :image, as: :file
        end
      end
    end

    f.actions
  end
end
