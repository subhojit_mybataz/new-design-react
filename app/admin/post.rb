# frozen_string_literal: true

ActiveAdmin.register Post do
  permit_params :name, :description, :brand_id, :product_id, :price, :deleted, :is_active, :custom_brand, :custom_product

  index do
    selectable_column
    column :id
    column :description
    column :user
    column :created_at
    column "Brands" do |post|
      post.brands.pluck(:name).join(",")
    end
    column "Products" do |post|
      post.products.pluck(:name).join(",")
    end
    column "Image" do |post|
      image_tag post.attachments.first.image.url, size: '200x100' unless post.attachments.blank?
    end
    column :actions do |resource|
      (link_to I18n.t('active_admin.view'), resource_path(resource)) + ' ' +
      (link_to I18n.t('active_admin.edit'), edit_resource_path(resource)) + ' ' +
      (link_to "Delete", '#', class: 'post_remove', id: resource.id)
    end
  end

  show do
    attributes_table :name, :description, :user, :created_at, :updated_at, :brand, :product, :custom_brand, :custom_product, :price

    panel 'Post Images' do
      table_for post.attachments do |t|
        t.column('Image') { |attachment| image_tag attachment.media.url, size: '200x100' }
      end
    end

    panel 'Post comments' do
      paginated_collection(Kaminari.paginate_array(post.comments).page(params[:comments_page]).per(10), param_name: 'comments_page') do
        table_for collection do |t|
          t.column('Comment', &:comment)
          t.column('Commented On', &:created_at)
          t.column('Commented By', &:user)
        end
      end
    end
  end
end
