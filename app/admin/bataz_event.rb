# frozen_string_literal: true

ActiveAdmin.register BatazEvent do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  permit_params :name, :image, :link

  controller do
    def destroy
      if resource.id == 1
        flash[:error] = 'This element cannot be deleted'
        redirect_to action: :index
      else
        super
      end
    end
  end

  show do
    attributes_table do
      row :name
      row :link
      row :image do |_event|
        image_tag bataz_event.image.url
      end
    end
  end

  form do |f|
    f.inputs do
      if f.object.image.exists?
        f.input :image, as: :file, hint: image_tag(f.object.image.url)
      else
        f.input :image, as: :file
      end
      f.input :name
      f.input :link
    end
    f.actions
  end
end
