# frozen_string_literal: true

ActiveAdmin.register BrandCountry, as: 'BrandCommunity' do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  controller do
    def show
      @page_title = 'Brand Community' + ' #' + params[:id]
      super
    end
  end

  remove_filter :created_at, :updated_at
  permit_params :brand_id, :country_id, :trending, :brand_rank
  show do
    attributes_table :id, :brand, :country, :trending, :brand_rank
    active_admin_comments
  end

  index do
    selectable_column
    column :id
    column :brand
    column :country
    column :trending
    column :brand_rank
    actions
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs do
      f.input :brand
      f.input :country
      f.input :trending
      f.input :brand_rank,
              hint: 'Enter rank, if the brand is trending'
    end
    f.actions do
      f.action :submit, label: 'Update Brand Community'
    end
  end
end
