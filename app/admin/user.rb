# frozen_string_literal: true

ActiveAdmin.register User do
  permit_params :name, :email, :top_user, :blacklisted, :country_id, :admin

  filter :user_types, collection: proc { UserType.pluck(:user_type) }
  filter :provider
  filter :brand_score
  filter :lifestyle_score
  filter :app_version
  filter :referer_code
  filter :email
  filter :phone
  filter :fname, label: 'First Name'
  filter :username
  filter :report_abuses_count
  filter :created_at

  actions :all, except: %i[new destroy]

  show do
    attributes_table do
      row :id
      row :email
      row :full_name
      row :gender
      row :dob
      row :bio
      row :city
      row :country
      row :username
      row :phone
      row :image do |user|
        image_tag user.image.url, size: '200x100'
      end
      row :total_likes
      row :posts_count
      row :total_guru_badges
      row :total_ambassador_badges
      row :followers_count
      row :followees_count
      row :posts_count
      row :total_guru_badges
      row :total_ambassador_badges
      row :followers_count
      row :followees_count
      row :provider
      row :uid
      row :referer_code
      row :user_types do |user|
        user.user_types.blank? ? '' : user.user_types.pluck(:user_type)
      end
      row :lifestyle_score
      row :brand_score
      row :top_user
      row :bataz_icon_user
      row :blacklisted
      row :admin
      row :app_version
      row :current_sign_in_at
      row :last_sign_in_at
      row :current_sign_in_ip
      row :last_sign_in_ip
      row :created_at
      row :updated_at
    end

    panel 'Posts' do
      paginated_collection(Kaminari.paginate_array(user.posts).page(params[:posts_page]).per(10), param_name: 'posts_page') do
        table_for collection do |t|
          t.column('Name') { |item| link_to item.name, admin_post_path(item) }
          t.column('Description', &:description)
          t.column('Likers count', &:likers_count)
          t.column('Comments count', &:comments_count)
          t.column('Custom brand', &:custom_brand)
          t.column('Custom product', &:custom_product)
        end
      end
    end

    panel 'Post comments' do
      paginated_collection(Kaminari.paginate_array(user.post_comments).page(params[:post_comments_page]).per(10), param_name: 'post_comments_page') do
        table_for collection do |t|
          t.column('Title', &:title)
          t.column('Comment', &:comment)
          t.column('Post title') { |item| item.commentable.try(:name) }
          t.column('Commentable Type') { |item| link_to 'post', admin_post_path(item) }
        end
      end
    end

    panel 'Comment comments' do
      paginated_collection(Kaminari.paginate_array(user.comment_comments).page(params[:comment_comments_page]).per(10), param_name: 'comment_comments_page') do
        table_for collection do |t|
          t.column('Title', &:title)
          t.column('Comment', &:comment)
          t.column('Commentable Type') { |item| link_to 'comment', admin_comment_path(item) }
        end
      end
    end
  end

  index do
    selectable_column
    column :id
    column :email
    column :full_name
    column :referer_code
    column :top_user
    column :blacklisted
    column :admin
    actions
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs do
      f.input :id, as: :string, input_html: { disabled: true }
      f.input :email, input_html: { disabled: true }
      f.input :fname, input_html: { disabled: true }
      f.input :lname, input_html: { disabled: true }
      f.input :gender, input_html: { disabled: true }
      f.input :city, input_html: { disabled: true }
      f.input :country
      f.input :top_user
      f.input :blacklisted
      f.input :admin
      f.input :bataz_icon_user, input_html: { disabled: true }
    end
    f.actions
  end

  # Kaminari.configure do |config|
  #   config.page_method_name = :per_page_kaminari
  # end
end
