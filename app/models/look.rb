# frozen_string_literal: true

class Look < Story
  include ApplicationHelper
  has_many :save_looks, dependent: :destroy
  has_many :report_abuses, class_name: 'ReportAbuse', as: :reportable, dependent: :destroy
  belongs_to :user, counter_cache: :looks_count
  accepts_nested_attributes_for :save_looks, limit: 1, allow_destroy: true
  accepts_nested_attributes_for :report_abuses, limit: 1
end
