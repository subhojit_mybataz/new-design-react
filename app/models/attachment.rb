# frozen_string_literal: true

class Attachment < ActiveRecord::Base
  acts_as_paranoid
  IMAGE_CONTENT_TYPES = ['image/jpg', 'image/jpeg', 'image/png'].freeze
  VIDEO_CONTENT_TYPES = ['application/x-mp4',
                         'video/mpeg',
                         'video/quicktime',
                         'video/x-la-asf',
                         'video/x-ms-asf',
                         'video/x-msvideo',
                         'video/x-sgi-movie',
                         'video/x-flv',
                         'flv-application/octet-stream',
                         'video/3gpp',
                         'video/3gpp2',
                         'video/3gpp-tt',
                         'video/BMPEG',
                         'video/BT656',
                         'video/CelB',
                         'video/DV',
                         'video/H261',
                         'video/H263',
                         'video/H263-1998',
                         'video/H263-2000',
                         'video/H264',
                         'video/JPEG',
                         'video/MJ2',
                         'video/MP1S',
                         'video/MP2P',
                         'video/MP2T',
                         'video/mp4',
                         'video/MP4V-ES',
                         'video/MPV',
                         'video/mpeg4',
                         'video/mpeg4-generic',
                         'video/nv',
                         'video/parityfec',
                         'video/pointer',
                         'video/raw',
                         'video/rtx'].freeze
  default_scope -> { order('created_at DESC') }

  has_attached_file :image,
                    styles: {
                      fixed_dimension: '-auto-orient',
                      sidebar_dimension: '110x120#',
                      app_compressed_dimension: {
                        geometry: '500x400>',
                        processor_options: {
                          compression: {
                            jpeg: {
                              command: 'jpegtran',
                              options: '-copy none -optimize'
                            }
                          }
                        }
                      }
                    },
                    convert_options: {
                      sidebar_dimension: ' -auto-orient'
                    },
                    processors: %i[thumbnail compression],
                    path: 'attachments/:id/:style',
                    preserve_files: "true"

  has_attached_file :video,
                    styles: {
                      thumb: {
                        geometry: '200x200#',
                        format: 'mp4',
                        time_offset: 5
                      },
                      video_thumb: {
                        geometry: '800x800#',
                        format: 'mp4',
                        vframes: 4,
                        time_offset: 0
                      },
                      video: {
                        geometry: '800x800#',
                        format: 'mp4'
                      }
                    },
                    processors: [:video_thumbnail]

  validates_attachment :image, content_type: { content_type: IMAGE_CONTENT_TYPES }
  validates_attachment :video, content_type: { content_type: VIDEO_CONTENT_TYPES }

  #validate_media_type: false

  belongs_to :attachable, polymorphic: true
  belongs_to :user
end
