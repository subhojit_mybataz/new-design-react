# frozen_string_literal: true

class ReportAbuseAction < ApplicationRecord
  belongs_to :report_abuse
end
