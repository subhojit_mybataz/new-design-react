# frozen_string_literal: true

class Brand < ApplicationRecord
  NO_OF_TOP_CONTRIBUTORS = 3
  acts_as_paranoid
  acts_as_followable
  has_many :brand_countries
  has_many :countries, through: :brand_countries
  has_many :banners, as: :bannerable, dependent: :destroy
  extend FriendlyId
  friendly_id :name, use: :slugged

  default_scope {order(priority: :desc)}

  has_attached_file :logo, styles: { fixed_dimension: 'auto-orient', medium: '300x300>', thumb: '75x75#' }, default_url: '/placeholder.svg', path: '/:class/:attachment/:id_partition/:style/:filename'
  validates_attachment :logo, content_type: { content_type: ['image/jpg', 'image/jpeg', 'image/png'] }

  accepts_nested_attributes_for :banners, allow_destroy: true

  def top_contributors
    contributors = Point.select('user_id, SUM(num) as TOTAL_POINTS').where(brand_id: self.id).group(:user_id).order('TOTAL_POINTS desc').limit(NO_OF_TOP_CONTRIBUTORS)
    top_contributors = contributors.map do |contributor|
      info = {}
      user = contributor.user
      info[:id] = user.id
      info[:name] = user.full_name
      info[:followers] = user.followers_count
      info[:posts] = user.posts_count
      info[:likes] = user.total_likes
      info[:image] = user.profile_pic[:thumb]
      info
    end
    top_contributors
  end

  def stories
    Story.joins(:story_brands).where("story_brands.brand_id = ?", self.id)
  end
end
