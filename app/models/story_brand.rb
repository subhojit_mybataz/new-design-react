# frozen_string_literal: true

class StoryBrand < ApplicationRecord
  include StiBase

  #validates :brandable_id, presence: true
  #validates :brandable_type, presence: true
  #validates :brand_id, presence: true
  #validates :association_type, inclusion: { in: ['Post', 'Look'] }

  acts_as_paranoid
  belongs_to :brand
  belongs_to :brandable, polymorphic: true, counter_cache: :brands_count

  def brandable_type=(sType)
    super(sType.to_s.classify.constantize.base_class.to_s)
  end
end
