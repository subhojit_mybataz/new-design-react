# frozen_string_literal: true

class Notification < ApplicationRecord
  TRACKABLE_TYPE_POST = 'Post'
  TRACKABLE_TYPE_USER = 'User'
  TRACKABLE_TYPE_COMMENT = 'Comment'
  TRACKABLE_TYPE_CLAP = "Clap"
  OWNER_TYPE = 'User'
  ACTIVITY_TYPE_COMMENTED = 'commented'
  ACTIVITY_TYPE_FOLLOWED = 'followed'
  ACTIVITY_TYPE_WARNED = 'warning'
  ACTIVITY_TYPE_REMOVED = 'removed'
  ACTIVITY_TYPE_CLAPPED = 'clapped'
  ACTIVITY_TYPES = [ACTIVITY_TYPE_CLAPPED, ACTIVITY_TYPE_FOLLOWED, ACTIVITY_TYPE_COMMENTED]
  RECEPIENT_TYPE = 'User'

  after_commit :send_app_notification
  belongs_to :recipient, class_name: 'User'

  default_scope -> { where(recipient_id: User.current.id, read: false).order('created_at DESC') }

  def send_app_notification
    return unless ACTIVITY_TYPES.include?(activity_type)
    return unless recipient_type.casecmp('user').zero?
    app_helper = AppNotification::Helper.new
    if app_helper.check_pn_limit(recipient)
      PushNotifTracker.create(user_id: recipient.id)
      data = {
        activity: activity_type,
        post_link: metadata['post_link'],
        title: 'Bataz',
        body: metadata['message']
      }
      options = {
        data: data,
        priority: 'high',
        collapse_key: 'updated_score'
      }
      res = FCM.new(Rails.application.secrets.firebase_server_key, timeout: 5).send([recipient.firebasePNId], options)
    end
  end
end
