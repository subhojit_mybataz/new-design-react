# frozen_string_literal: true

class Clap < ActiveRecord::Base
  acts_as_paranoid
  belongs_to :clapable, polymorphic: true
  belongs_to :user
  default_scope -> { order('count DESC') }

  validates :clapable_id, presence: true
  validates :clapable_type, presence: true

  before_create :set_brand_story_product_claps
  before_update :set_brand_story_product_claps

  after_create :send_clap_notification
  after_update :send_clap_notification

  validates_numericality_of :count, on: %i[create update], less_than_or_equal_to: 5

  def story_owner_clap?
    clapable.user_id == self.user_id
  end

  def send_clap_notification
    return if story_owner_clap?
    notif = Notification.unscoped.where('recipient_id=? and owner_id=? and trackable_type=? and trackable_id=?', clapable.user_id, self.user_id, clapable_type, clapable_id).where(activity_type: Notification::ACTIVITY_TYPE_CLAPPED).order(:created_at).last
    if notif.nil?
      metadata = {
        message: "#{user.full_name.strip} clapped on your #{clapable.type.downcase}",
        type: clapable.type.downcase == 'look' ? 'looks' : 'posts'
      }
      notification = Notification.create(
          trackable_type: clapable_type,
          trackable_id: clapable_id,
          owner_id: self.user.id,
          owner_type: Notification::OWNER_TYPE,
          activity_type: Notification::ACTIVITY_TYPE_CLAPPED,
          recipient_id: clapable.user_id,
          recipient_type: Notification::RECEPIENT_TYPE,
          read: false,
          metadata: metadata
        )
    else
      metadata = {
        message: "#{user.full_name.strip} clapped your post #{self.count} times"
      }
      notif.metadata = metadata
      notif.read = false
      notif.save
    end
  end

  def set_brand_story_product_claps
    if count_changed? || new_record?
      clapable.update(claps_count: clapable.claps_count + 1)
      clapable.brands.each do |brand|
        brand.update(claps_count: brand.claps_count + 1)
      end
      clapable.products.each do |product|
        product.update(claps_count: product.claps_count + 1)
      end
    end
  end
end