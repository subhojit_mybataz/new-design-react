# frozen_string_literal: true

class Emoticon < ApplicationRecord
  validates :name, presence: true
  validates :url, presence: true
end
