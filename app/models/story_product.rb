# frozen_string_literal: true

class StoryProduct < ApplicationRecord
  include StiBase
  acts_as_paranoid

  # validates :productable_id, presence: true
  # validates :productable_type, presence: true
  # validates :product_id, presence: true
  # validates :association_type, inclusion: { in: ['Post', 'Look'] }

  belongs_to :product
  belongs_to :productable, polymorphic: true, counter_cache: :products_count
end
