# frozen_string_literal: true

module StiBase
  extend ActiveSupport::Concern
  included do
    before_create :set_association_type
    before_create :increment_counter
    before_destroy :decrement_counter
  end

  def polymorphic_type
    self.class.reflect_on_all_associations(:belongs_to).select { |m| m.options[:polymorphic] }.first.plural_name.singularize
  end

  def parent_type
    polymorphic_type.gsub(/able/, '')
  end

  def set_association_type
    self.association_type = send(polymorphic_type.to_s).send('type')
  end

  def increment_counter
    return if send(parent_type).nil?
    send(parent_type).update("#{association_type.downcase.pluralize}_count".to_sym => send(parent_type).send("#{association_type.downcase.pluralize}_count") + 1)
  end

  def decrement_counter
    return if send(parent_type).nil?
    send(parent_type).update("#{association_type.downcase.pluralize}_count".to_sym => send(parent_type).send("#{association_type.downcase.pluralize}_count") - 1)
  end
end
