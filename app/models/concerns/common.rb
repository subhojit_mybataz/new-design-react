# frozen_string_literal: true

require 'active_support/concern'

module Common
  extend ActiveSupport::Concern

  included do
  end

  def createFirebaseToken(user_email)
    token = ''
    now_seconds = Time.now.to_i

    firebase_private_key = Rails.application.secrets.PRIVATE_KEY

    private_key = OpenSSL::PKey::RSA.new firebase_private_key
    payload = { iss: Rails.application.secrets.SERVICE_ACCOUNT_EMAIL,
                sub: Rails.application.secrets.SERVICE_ACCOUNT_EMAIL,
                aud: 'https://identitytoolkit.googleapis.com/google.identity.identitytoolkit.v1.IdentityToolkit',
                iat: now_seconds,
                exp: now_seconds + (60 * 60), # Maximum expiration time is one hour
                uid: user_email,
                claims: { premium_account: false } }
    token = JWT.encode payload, private_key, 'RS256'
    token
  end
end
