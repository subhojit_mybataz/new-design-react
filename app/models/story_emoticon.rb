# frozen_string_literal: true
class StoryEmoticon < ApplicationRecord
  acts_as_paranoid
  belongs_to :emoticon
  belongs_to :story
  belongs_to :user
  validates :story, presence: true
  validates :emoticon, presence: true
end
