# frozen_string_literal: true

class Tag < ApplicationRecord
  acts_as_paranoid

  validates :name, presence: true
  validates :user_id, presence: true

  belongs_to :user
  has_many :tagable
  has_many :story_tags
end
