# frozen_string_literal: true

class UserType < ApplicationRecord
  has_many :user_user_types, dependent: :destroy
  has_many :users, through: :user_user_types
  validates :user_type, presence: true
end
