# frozen_string_literal: true

class Follow < Socialization::ActiveRecordStores::Follow
  after_create :send_follow_notification, if: proc { |follow|
    follow.followable_type.casecmp('user').zero? && follow.followable.email != User::MYBATAZ_EMAIL
  }

  def send_follow_notification
    notif = Notification.unscoped.where('recipient_id=? and owner_id=?', followable.id, follower.id).where(activity_type: 'followed').order(:created_at).last
    if notif.nil?
      metadata = {
        message: "#{follower.full_name.strip} is now following you"
      }
      notification = Notification.create(trackable_type: Notification::TRACKABLE_TYPE_USER, trackable_id: followable.id, owner_type: Notification::OWNER_TYPE, owner_id: follower.id, activity_type: Notification::ACTIVITY_TYPE_FOLLOWED, recipient_type: Notification::RECEPIENT_TYPE, recipient_id: followable.id, read: false, metadata: metadata)
      notification.save
    else
      notif.created_at = DateTime.now
      notif.save
    end
  end
end
