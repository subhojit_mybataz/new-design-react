# frozen_string_literal: true

class Product < ApplicationRecord
  # belongs_to :productable, :polymorphic => true
  default_scope {order(priority: :desc)}
end
