# frozen_string_literal: true

class SaveLook < ApplicationRecord
  acts_as_paranoid
  belongs_to :look, counter_cache: :save_looks_count
  belongs_to :user
end
