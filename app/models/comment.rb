# frozen_string_literal: true

class Comment < ActiveRecord::Base
  include ApplicationHelper
  include ActsAsCommentable::Comment
  acts_as_paranoid
  belongs_to :commentable, polymorphic: true, counter_cache: true
  has_many :report_abuses, class_name: 'ReportAbuse', as: :reportable, dependent: :destroy

  default_scope -> { order('created_at DESC') }

  scope :post_comment, -> { where(commentable_type: 'Post').order('created_at DESC') }
  scope :comment_comment, -> { where(commentable_type: 'Comment').order('created_at DESC') }
  belongs_to :user, counter_cache: :comments_count

  validates :comment, presence: true
  validates :commentable_type, presence: true
  validates :commentable_id, presence: true
  validates :association_type, inclusion: { in: ['Post', 'Look'] }

  before_create :set_users
  before_create :set_brand_product_comments

  after_create :send_comment_notification

  def story_owner_comment?
    self.user_id == commentable.user_id
  end

  def send_comment_notification
    return if story_owner_comment?
    notif = Notification.unscoped.where('recipient_id=? and owner_id=? and trackable_type=? and trackable_id=?', commentable.user_id, self.user_id, commentable_type, commentable_id).where(activity_type: Notification::ACTIVITY_TYPE_COMMENTED).order(:created_at).last
    if notif.nil?
      metadata = {
        message: "#{user.full_name.strip} commented on your #{commentable.type.downcase}",
        type: commentable.type.downcase == 'look' ? 'looks' : 'posts'
      }
      notification = Notification.create(
                        trackable_type: commentable_type,
                        trackable_id: commentable_id,
                        owner_type: Notification::OWNER_TYPE, 
                        owner_id: self.user_id, 
                        activity_type: Notification::ACTIVITY_TYPE_COMMENTED, 
                        recipient_type: Notification::RECEPIENT_TYPE, 
                        recipient_id: commentable.user_id, 
                        read: false, 
                        metadata: metadata)
      notification.save
    else
      notif.created_at = DateTime.now
      notif.read = false
      notif.save
    end
  end

  def set_brand_product_comments
    if new_record?
      commentable.brands.each do |brand|
        brand.update(comments_count: brand.comments_count + 1)
      end
      commentable.products.each do |product|
        product.update(comments_count: product.comments_count + 1)
      end
    end
  end

  def set_users
    self.user_id = User.current.id
  end

  def getFeedJSON
    hash_response = {}

    hash_response[:user] = user.full_name
    hash_response[:profile_pic] = User.find(user.id).image.url(:thumb)
    hash_response[:comment] = comment
    hash_response[:id] = id
    hash_response[:user_id] = user.id

    hash_response
  end
end
