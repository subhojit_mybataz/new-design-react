# frozen_string_literal: true

class Country < ApplicationRecord
  has_many :brand_countries
  has_many :brands, through: :brand_countries
  has_many :users
end
