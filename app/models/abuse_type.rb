# frozen_string_literal: true

class AbuseType < ApplicationRecord
  has_many :report_abuses, dependent: :destroy
end
