# frozen_string_literal: true

class BrandCountry < ApplicationRecord
  belongs_to :brand
  belongs_to :country

  validates_uniqueness_of :brand_id, scope: :country_id
end
