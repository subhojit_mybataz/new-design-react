# frozen_string_literal: true

class Banner < ApplicationRecord
  acts_as_paranoid
  belongs_to :brand_country
  belongs_to :bannerable, polymorphic: true
  has_attached_file :image, styles: { medium: '850x315#' }, default_url: '/placeholder.svg'
  validates_attachment :image, content_type: { content_type: ['image/jpg', 'image/jpeg', 'image/png'] }
end
