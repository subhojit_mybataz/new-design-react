# frozen_string_literal: true

class UserUserType < ApplicationRecord
  belongs_to :user_type
  belongs_to :user
end
