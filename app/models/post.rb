# frozen_string_literal: true

class Post < Story
  has_many :bookmark_posts, dependent: :destroy
  has_many :report_abuses, class_name: 'ReportAbuse', as: :reportable, dependent: :destroy
  accepts_nested_attributes_for :bookmark_posts, limit: 1, allow_destroy: true
  accepts_nested_attributes_for :report_abuses, limit: 1

  def tagable_type=(_sType)
    'Post'
  end
end
