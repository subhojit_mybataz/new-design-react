# frozen_string_literal: true
require 'link_thumbnailer'
class Story < ApplicationRecord
  include ApplicationHelper

  RANGE = 2

  validates :type, inclusion: { in: ['Post', 'Look'] }
  # validates :brands, :length => { :minimum => 1 }, on: :create
  # validates :products, :length => { :minimum => 1 }, on: :create

  acts_as_paranoid recover_dependent_associations: true
  acts_as_likeable
  acts_as_commentable dependent: :destroy

  has_many :attachments, as: :attachable, dependent: :destroy
  has_many :story_brands, as: :brandable, dependent: :destroy
  has_many :brands, through: :story_brands
  has_many :story_products, as: :productable, dependent: :destroy
  has_many :products, through: :story_products
  has_many :claps, as: :clapable, dependent: :destroy
  has_many :tags, through: :story_tags
  has_many :story_tags, as: :tagable, dependent: :destroy
  has_many :story_emoticons
  has_many :emoticons, through: :story_emoticons
  belongs_to :user

  # accepts_nested_attributes_for :brands
  # accepts_nested_attributes_for :products
  accepts_nested_attributes_for :attachments
  accepts_nested_attributes_for :comments, limit: 1, allow_destroy: true

  attr_accessor :images_attributes
  attr_accessor :videos_attributes
  attr_accessor :clap_attribute
  attr_accessor :emoticon_ids
  attr_accessor :brand_ids
  attr_accessor :product_ids

  scope :post_stories, -> { where(type: 'Post') }
  scope :look_stories, -> { where(type: 'Look') }

  after_save :increment_points
  before_create :set_user
  after_commit :follow_brand, on: :create
  before_commit :create_attachments, on: :create
  before_commit :create_claps, on: :update
  before_commit :create_story_emoticons, on: :update
  before_commit :create_tags, on: :create
  before_commit :create_links_meta, on: :create
  before_commit :create_brands, on: :create
  before_commit :create_products, on: :create
  before_create :increment_counter
  before_destroy :decrement_counter

  def create_story_emoticons
    return if self.emoticon_ids.nil?
    self.emoticon_ids.each do |id|
      story_emoticon = story_emoticons.find_or_initialize_by(user_id: User.current.id, emoticon_id: id)
      if story_emoticon.new_record?
        story_emoticon.save
      else  
        story_emoticon.update(count: story_emoticon.count + 1)
      end
    end
    self.emoticon_ids = nil
  end

  def increment_counter
    self.user.update("#{self.type.downcase.pluralize}_count".to_sym => self.user.send("#{self.type.downcase.pluralize}_count") + 1)
  end

  def decrement_counter
    self.user.update("#{self.type.downcase.pluralize}_count".to_sym => self.user.send("#{self.type.downcase.pluralize}_count") - 1)
  end

  def create_links_meta
    return if description.empty? || description.nil?
    metadata[:links_meta] ||= [] 
    url =  URI.extract(description).last
    return unless url.present?
    link_object = LinkThumbnailer.generate(url)
    metadata[:links_meta].push({url: url, title: link_object.title, favicon: link_object.favicon, description: link_object.description, image: link_object.images.first.src.to_s})
  end

  def create_tags
    return if description.empty? || description.nil?
    description.scan(/#\w+/).flatten.each do |t|
      t = t[1..-1]
      t_stripped = t.gsub(/[^0-9A-Za-z]/, '')
      tag = Tag.find_or_initialize_by(name: t_stripped)
      if tag.new_record?
        tag.user = User.current
        tag.save
      end
      StoryTag.create(tag: tag, name: t, tagable: self)
    end
  end

  def create_claps
    return if clap_attribute.nil?
    return unless clap_attribute[:clap]
    clap = claps.where(user_id: current_user.id).first
    clap_attribute[:clap] = false
    if clap.nil?
      claps.create(user_id: current_user.id)
    else
      clap.count += 1
      clap.save
    end
  end

  def last_comment
    comments.first
  end

  def set_user
    self.user_id = current_user.id
    self.price = 0.0 if self.price.nil?
  end

  def create_attachments
    if images_attributes.present?
      images_attributes.each do |file|
        attachments.create(image: file, attachable_id: id, attachable_type: type, file_type: 'image')
      end
    end
    if videos_attributes.present?
      videos_attributes.each do |file|
        attachments.create(video: file, attachable_id: id, attachable_type: type, file_type: 'video')
      end
    end
  end

  def create_brands
    return if brand_ids.blank?
    brand_ids.each do |i|
      brand = Brand.find_by(id: i)
      if brand.nil?
        brand = Brand.find_or_create_by(name: i)
        brand.update(is_active: false)
      end
      StoryBrand.create(brandable_id: id, brandable_type: 'Story', brand_id: brand.id)
    end
  end

  def create_products
    return if product_ids.blank?
    product_ids.each do |i|
      product = Product.find_by(id: i)
      if product.nil?
        product = Product.find_or_create_by(name: i)
      end
      StoryProduct.create(productable_id: id, productable_type: 'Story', product_id: product.id)
    end
  end

  def increment_points
    # Point.increment_points_for_post current_user_from_base, params[:Brand], params[:custom_brand]
  end

  def self.find_as_sorted(ids)
    return [] if ids.count == 0
    values = []
    ids.each_with_index do |id, index|
      values << "(#{id}, #{index + 1})"
    end
    relation = self.joins("JOIN (VALUES #{values.join(",")}) as x (id, ordering) ON #{table_name}.id = x.id").includes({comments: [:user]}, {claps: [:user]}, :attachments, :brands, :products, {user: [:claps]})
    relation = relation.order('x.ordering')
    relation
  end

  def emoticon_aggregate
    self.emoticons.select("emoticon_id as id, url_logo, sum(story_emoticons.count)").group(:emoticon_id, :url_logo).order('sum(story_emoticons.count) desc')
  end

  ### TO be cleaned after database migration 

  has_attached_file :image, :styles => { :fixed_dimension => '-auto-orient', :sidebar_dimension=> '110x120#' },
                      :convert_options => {sidebar_dimension: " -auto-orient"}, bucket: 'mybataz', path: '/:class/images/:id_partition/:style/:filename'
  validates_attachment :image, content_type: { content_type:     ["image/jpg", "image/jpeg", "image/png"] }

  ###

  def follow_brand
    return if self.brands.blank?
    self.brands.each do |brand|
      self.user.toggle_follow!(brand) unless self.user.follows?(brand)
    end
  end
end
