# frozen_string_literal: true

class BookmarkPost < ApplicationRecord
  acts_as_paranoid
  belongs_to :story, counter_cache: :bookmark_posts_count
  belongs_to :user
end
