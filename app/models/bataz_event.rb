# frozen_string_literal: true

class BatazEvent < ApplicationRecord
  has_attached_file :image, styles: { right_sidebar_dimension: '480x360#' }, path: '/:class/:attachment/:id_partition/:style/:filename'
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  def get_element
    hash_response = {}
    hash_response[:image] = image.url(:right_sidebar_dimension) if image.present?
    hash_response[:link] = link
    hash_response[:name] = name
    hash_response
  end
end
