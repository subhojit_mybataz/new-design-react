# frozen_string_literal: true

class User < ApplicationRecord
  MYBATAZ_EMAIL = 'social@myabatz.com'

  has_attached_file :image, styles: {fixed_dimension: 'auto-orient', medium: '300x300>', thumb: '75x75#'}, default_url: '/missing.jpg', path: '/:class/images/:id_partition/:style/:filename'
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
  validates_length_of :user_types, maximum: 3
  extend Common

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, omniauth_providers: %i[facebook google_oauth2 instagram]

  acts_as_paranoid recover_dependent_associations: true

  # Added so that user can like a post
  acts_as_liker

  # Added so that users can follow each other
  acts_as_followable dependent: :destroy
  acts_as_follower dependent: :destroy

  belongs_to :country
  has_many :points, dependent: :destroy
  has_many :posts, -> {post_stories}, class_name: 'Story'
  has_many :looks, -> {look_stories}, class_name: 'Story'
  has_many :stories, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :claps, dependent: :destroy
  has_many :post_comments, -> {post_comment}, class_name: 'Comment'
  has_many :comment_comments, -> {comment_comment}, class_name: 'Comment'
  has_many :report_abuses, class_name: 'ReportAbuse', as: :reportable
  has_many :user_user_types, dependent: :destroy
  has_many :user_types, through: :user_user_types

  accepts_nested_attributes_for :user_types

  before_create :set_referer_code
  after_create :check_referer_point

  attr_accessor :refered_code, :name

  def as_json(options = {})
    if options[:register] || options[:login]
      {
        id: id,
        name: full_name,
        email: email,
        t: random_auth_token
      }
    elsif (options[:notification])
      {
        id: id,
        name: full_name,
        image_url: image.url(:thumb)
      }
    end
  end

  def self.register(params)
    token = SecureRandom.hex(9).to_s.upcase
    user = User.find_by(email: params[:email])
    if user.present?
      response = {status: 300, message: I18n.t('user.duplicate'), data: {id: user.id, t: token}}
    else
      if params[:email].nil? || params[:password].nil? || params[:name].nil?
        response = {status: 300, message: I18n.t('user.invalid'), data: nil}
      else
        splitNames = params[:name].split(' ', 2)

        new_user = User.new(
          email: params[:email],
          password: params[:password],
          fname: splitNames[0],
          lname: splitNames[1],
          firebasePNId: params[:pnId],
          random_auth_token: token,
          firebase_auth_token: createFirebaseToken(params[:email]),
          refered_code: params[:referer_code]
        )

        if new_user.save
          UserNotifierMailer.send_signup_email(new_user).deliver_later
          response = {status: 200, message: I18n.t('user.create'), data: new_user.as_json(register: true)}
        else
          response = {status: 300, message: I18n.t('user.error'), data: nil}
        end
      end
    end
  end

  def self.social_register(params)
    if params[:accesstoken]
      user = Identity.where(uid: params[:uid], provider: params[:provider]).first_or_create(
        name: params[:name],
        accesstoken: params[:accesstoken],
        email: params[:email]
      ).user

      first_login = auth_helper(params) if user.nil?
      response = {status: 200, message: 'Registered successfully.', data: {first_login: first_login || false}}
    else
      response = {status: 400, message: 'Invalid Access Token', data: nil}
    end
  end

  def self.auth_helper(params)
    user = User.find_by(email: params[:email])
    identity = Identity.find_by(email: params[:email])
    if user.nil?
      first_login = true
      splitNames = params[:name].split(' ', 2)

      user = User.create(
        email: params[:email],
        fname: splitNames[0],
        lname: splitNames[1],
        provider: params[:provider],
        uid: params[:uid],
        gender: params[:gender] || '',
        bataz_icon_user: false,
        password: Devise.friendly_token[0, 20],
        referer_code: params[:referer_code],
        firebasePNId: params[:pnId]
      )
      UserNotifierMailer.send_signup_email(user).deliver_now
    else
      first_login = false
    end
    identity.update_attribute(:user_id, user.id)
    first_login
  end

  def self.social_login(params)
    user = User.find_by_email(params[:email])
    if user
      token = SecureRandom.hex(9).to_s.upcase
      user.random_auth_token = token
      user.firebase_auth_token = createFirebaseToken(params[:email])
      user.firebasePNId = params[:pnId] if params[:pnId]
      user.save
      response = {status: 200, message: 'Logged in successfully.', data: user.as_json(register: true)}
    else
      response = {status: 300, message: 'Your account is inactive', data: nil}
    end
  end

  def self.login(params)
    user = User.find_by_email(params[:email])
    if user.present?
      if user.valid_password?(params[:password])
        token = SecureRandom.hex(9).to_s.upcase
        user.random_auth_token = token
        user.firebase_auth_token = createFirebaseToken(params[:email])
        user.firebasePNId = params[:pnId] if params[:pnId]
        user.save
        response = {status: 200, message: 'Logged in successfully.', data: user.as_json(login: true)}
      else
        response = {status: 300, message: 'passwordMisMatch', data: nil}
      end
    else
      response = {status: 300, message: 'Email does not exist', data: nil}
    end
  end

  def full_name
    "#{fname} #{lname}"
  end

  def is_ambassador_for_brand(brand_id)
    Badge.where(brand_id: brand_id, badge_type: Badge::BRAND_AMBASSADOR).exists?
  end

  def get_location
    user_city = city.nil? ? '' : city
    user_country = country.nil? ? '' : country.name

    if !city.nil? && !country.nil?
      user_city + ', ' + user_country
    else
      user_city + user_country
    end
  end

  def set_referer_code
    self.referer_code = SecureRandom.hex(5).upcase
  end

  def referer_link
    referer_code
  end

  def send_referer_link(email)
    Mailer.send_referer_mail(email, self).deliver_now
  end

  def check_referer_point
    unless refered_code.present?
      referer_user = User.find_by_referer_code(refered_code)
      return if referer_user.nil?
      invite_point_rule = PointRule.find_by_action_type('refer')
      return if invite_point_rule.nil?
      referer_user.points.create(points: invite_point_rule.points, point_rule_id: invite_point_rule.id, action_by: id, item_id: id, item_type: 'user')
    end
  end

  def self.current
    Thread.current[:user]
  end

  def self.current=(user)
    Thread.current[:user] = user
  end

  def story_owner?(story_id)
    self.stories.where(id: story_id).first.present?
  end

  def profile_pic
    {
      fixed: self.image.url(:fixed_dimension),
      medium: self.image.url(:medium),
      thumb: self.image.url(:thumb)
    }
  end

  def follow_mybataz
    user_to_follow = User.where(email: User::MYBATAZ_EMAIL).first
    self.toggle_follow!(user_to_follow) unless self.follows?(user_to_follow) && user_to_follow.nil?
  end
end
