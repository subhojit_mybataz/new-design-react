# frozen_string_literal: true

class Badge < ApplicationRecord
  belongs_to :brand
  belongs_to :user

  BRAND_AMBASSADOR = 'BRAND_AMBASSADOR'
  BRAND_GURU = 'BRAND_GURU'

  AHOY_EVENT_NEW_AMBASSADOR = 'NEW_BRAND_AMBASSADOR'
  AHOY_EVENT_NEW_GURU = 'NEW_BRAND_GURU'

  NO_OF_POST_FOR_AMBASSADOR = 5
  NO_OF_POST_FOR_GURU = 10

  NO_OF_LIKES_FOR_GURU_OR_AMBASSADOR = 10

  def self.compute_badges(user, brand_id)
    # query DB if its points are engouh to add badge
    posts = Post.where('user_id = ? AND brand_id = ?', user.id, brand_id)
    if posts.size >= NO_OF_POST_FOR_AMBASSADOR
      total_likes = posts.inject(0) { |sum, post| post.likers_count + sum }
      if total_likes >= NO_OF_LIKES_FOR_GURU_OR_AMBASSADOR
        current_badge = Badge.where(user_id: user.id, brand_id: brand_id)[0]
        current_brand = Brand.find brand_id
        if current_badge.nil?
          Badge.create(user_id: user.id, brand_id: brand_id, badge_type: BRAND_AMBASSADOR)
          user.increment! :total_guru_badges
          current_brand.increment! :total_guru_badges
          ahoy.track AHOY_EVENT_NEW_AMBASSADOR, brand_id: current_brand.id
        elsif current_badge.badge_type == BRAND_AMBASSADOR && posts.size >= NO_OF_POST_FOR_GURU
          current_badge.update(badge_type: BRAND_GURU)
          user.decrement! :total_guru_badges
          current_brand.decrement! :total_guru_badges
          user.increment! :total_ambassador_badges
          current_brand.increment! :total_ambassador_badges
          ahoy.track AHOY_EVENT_NEW_GURU, brand_id: current_brand.id
        end
      end
    end
  end
end
