# frozen_string_literal: true

class Point < ApplicationRecord

  COMMENT_POINT = 0.5
  CLAP_POINT = 0.2

  belongs_to :brand
  belongs_to :user
  validates_presence_of :item_type, :item_id, :points, :point_rule_id, :action_by
  # SCORE_FOR_COMMENTING=1
  # SCORE_FOR_POSTING_WITH_IMAGE=5
  # SCORE_FOR_POSTING_WITHOUT_IMAGE=2

  # def self.increment_points_for_comments(user, brand_id, custom_brand)
  #   points = SCORE_FOR_COMMENTING
  #   points = 2 * points if user.is_ambassador_for_brand brand_id
  #   increment_points(user, points, brand_id, custom_brand)
  # end

  # def self.increment_points_for_post(user, brand_id, custom_brand)
  #   points = SCORE_FOR_POSTING_WITH_IMAGE
  #   points = 2 * points if user.is_ambassador_for_brand brand_id
  #   increment_points(user, points, brand_id, custom_brand)
  #   Badge.compute_badges user, brand_id
  # end

  # private
  # def self.increment_points(user, action_points, brand_id, custom_brand)
  #   points = Point.new
  #   points.user_id = user.id
  #   points.num = action_points
  #   unless brand_id.nil?
  #     points.brand_id = brand_id
  #   else
  #     points.custom_brand = custom_brand
  #   end
  #   points.save

  #   unless brand_id.nil?
  #     user.brand_score += action_points
  #   end
  #   user.lifestyle_score += action_points
  #   user.save

  # end
end
