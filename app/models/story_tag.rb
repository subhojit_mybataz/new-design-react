# frozen_string_literal: true

class StoryTag < ApplicationRecord
  include StiBase
  acts_as_paranoid

  validates :name, presence: true
  validates :tagable_id, presence: true
  validates :tagable_type, presence: true
  validates :tag_id, presence: true
  validates :association_type, inclusion: { in: ['Post', 'Look'] }

  belongs_to :tag
  belongs_to :tagable, polymorphic: true
end
