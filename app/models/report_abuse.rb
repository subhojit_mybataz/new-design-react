# frozen_string_literal: true

class ReportAbuse < ApplicationRecord
  belongs_to :reportable, polymorphic: true, counter_cache: true
  belongs_to :user
  belongs_to :abuse_type

  has_many :report_abuse_actions, dependent: :destroy

  before_save :set_user

  validates :reportable_id, presence: true
  validates :reportable_type, presence: true
  validates :abuse_type_id, presence: true

  def set_user
    self.user_id = User.current.id
  end

  def send_warning_to_resource
    reported_for = reportable_type.constantize.find(reportable_id)
    return unless reported_for
    action = ReportAbuseAction.new(report_abuse_id: id, action: 'Warned')
    send_warning_notification(reported_for)
    action.save
  end

  def remove_resource
    reported_for = reportable_type.constantize.find(reportable_id)
    return unless reported_for
    report_abuse_actions << ReportAbuseAction.create(report_abuse_id: id, action: 'Destroyed')
    reported_for.destroy
    send_removal_notification(reported_for, nil)
  end

  def send_notification(user, resource, message, activity)
    notif = Notification.where('recipient_id=? and owner_id=?', user.id, User.current.id).where(activity_type: activity).order(:created_at).last
    if notif.nil?
      metadata = {
        message: message
      }

      notification = Notification.create(
        trackable_type: Notification::TRACKABLE_TYPE_POST,
        trackable_id: resource.id,
        owner_type: Notification::OWNER_TYPE,
        owner_id: User.current.id,
        activity_type: Notification::ACTIVITY_TYPE_REMOVED,
        recipient_type: Notification::RECEPIENT_TYPE,
        recipient_id: user.id,
        read: false, metadata: metadata
      )
    end
  end

  # TODO: Msg need update
  def send_removal_notification(resource, abuse_type_id)
    if reportable_type == 'User'
      UserNotifierMailer.send_block_email(resource).deliver_later
    elsif reportable_type == 'Post'
      post_user = User.find(resource.user_id)
      UserNotifierMailer.post_removal_email(post_user, resource, abuse_type_id = abuse_type_id).deliver_later if post_user
      #message = 'Your post have been removed.'
      #send_notification(post_user, resource, message, 'removed')
    end
  end

  # TODO: Msg need update
  def send_warning_notification(resource)
    if reportable_type == 'User'
      message = 'You have been warned from MyBataz'
      #send_notification(resource, resource, message, 'warned')
      UserNotifierMailer.send_warning_email(resource).deliver_later
    elsif reportable_type == 'Post'
      post_user = User.find(resource.user_id)
      message = "You have been warned from MyBataz for this #{link_to 'Post', post_path(resource)}".html_safe
      UserNotifierMailer.post_removal_email(post_user, resource, nil).deliver_later unless post_user
      #send_notification(post_user, resource, message, 'warned')
    end
  end
end
