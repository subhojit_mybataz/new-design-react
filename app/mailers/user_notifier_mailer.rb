# frozen_string_literal: true

class UserNotifierMailer < ApplicationMailer
  default from: 'noreply@mybataz.com'

  # send a signup email to the user, pass in the user object that   contains the user's email address
  def send_signup_email(user)
    @user = user
    mail(to: @user.email,
         subject: 'Thanks for signing up for our MyBataz app')
  end

  def send_block_email(user)
    @user = user
    mail(to: @user.email,
         subject: 'Your MyBataz account is temporarily blocked')
  end

  def post_removal_email(user, post, abuse_type_id)
    @abuse_type = AbuseType.where(id: abuse_type_id).first.try(:name)
    @user = user
    @post = Story.with_deleted.where(id: post.id).first
    @type = post.type == 'look' ? 'looks' : 'posts'
    mail(to: @user.email,
         subject: 'Your post is removed')
  end

  def send_warning_email(user)
    @user = user
    mail(to: @user.email,
         subject: 'Your have been warned')
  end
end
