# frozen_string_literal: true

class Mailer < ActionMailer::Base
  def invite_shopping_spree(email)
    mail to: email,
         subject: 'Join your friend and help them shop',
         skip_premailer: true
  end

  def send_invite_mail(email)
    mail to: email,
         subject: 'Join me on MyBataz'
  end

  def send_referer_mail(email, user)
    @user_name = user.fname
    host = Rails.application.config.action_mailer.default_url_options[:host]
    host += ":#{Rails.application.config.action_mailer.default_url_options[:port]}" if Rails.application.config.action_mailer.default_url_options[:port].present?
    @url = host + "/users/sign_up?referer_code=#{user.referer_code}"
    mail to: email,
         subject: 'Join me on MyBataz'
  end
end
