# frozen_string_literal: true

class AccessPolicy
  include AccessGranted::Policy

  def configure
    role :user do
      can %i[create index show update], Post
      can %i[destroy], Post do |post, current_user|
        post.user.id == current_user.id || current_user.admin
      end

      can %i[create index show update], Look
      can %i[destroy], Look do |look, current_user|
        look.user.id == current_user.id || current_user.admin
      end

      can %i[create index show], User
      can %i[update destroy], User do |user, current_user|
        user.id == current_user.id
      end

      can %i[index show], AbuseType
      can %i[index show], UserType

      can %i[index show], Notification
      can %[update], Notification do |notification, current_user|
        notification.recipient_id = current_user.id
      end

      can %i[index show], Brand
    end
  end
end
