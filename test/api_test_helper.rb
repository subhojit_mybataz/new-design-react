# frozen_string_literal: true

module ApiTestHelper
  def setup
    user = users :one
    token = Doorkeeper::AccessToken.new(resource_owner_id: user.id)
    ApplicationController.any_instance.stubs(:doorkeeper_token).returns(token)
 end

  def test_create
    setup
    api_url = "/api/v1/#{resource_name.to_s.pluralize}"
    post api_url, params: create_params
    assert_response :success
  end
end
