# frozen_string_literal: true

require 'test_helper'
require 'api_test_helper'
class Api::V1::PostsControllerTest < ActionDispatch::IntegrationTest
  include ApiTestHelper

  def resource_name
    :post
  end

  def create_params
    {
      post: {
        description: 'test description',
        brand_ids: [brands(:one).id],
        product_ids: [products(:one).id]
      }
    }
  end

  def test_v1
    assert true
  end

  test 'the truth' do
    assert true
  end
end
