# frozen_string_literal: true

require 'test_helper'

class SocialPreviewControllerTest < ActionDispatch::IntegrationTest
  test 'should get index' do
    get social_preview_index_url
    assert_response :success
  end
end
