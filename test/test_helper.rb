# frozen_string_literal: true

ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'mocha/test_unit'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
  # setup do
  # 	user = users :one
  # 	token = Doorkeeper::AccessToken.new(resource_owner_id: user.id)
  #   ApplicationController.any_instance.stubs(:doorkeeper_token).returns(token)
  # end
end

class ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
end
