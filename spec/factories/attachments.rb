FactoryGirl.define do
  factory :attachment do
    Badge "MyString"
    Banner "MyString"
    BatazEvent "MyString"
    BookmarkPost "MyString"
    Brand "MyString"
    BrandCountry "MyString"
  end
end
