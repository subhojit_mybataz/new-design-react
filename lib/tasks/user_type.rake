# frozen_string_literal: true

namespace :user_type do
  desc 'Populate user type table'
  task populate: :environment do
    UserType.where(user_type: 'Shopper addict').first_or_create!
    UserType.where(user_type: 'Fashion addict').first_or_create!
    UserType.where(user_type: 'Trend setter').first_or_create!
    UserType.where(user_type: 'Blogger').first_or_create!
    UserType.where(user_type: 'Retailer').first_or_create!
    UserType.where(user_type: 'Photographer').first_or_create!
    UserType.where(user_type: 'Designer').first_or_create!
    UserType.where(user_type: 'Model').first_or_create!
    UserType.where(user_type: 'Actor').first_or_create!
    UserType.where(user_type: 'Actress').first_or_create!
    UserType.where(user_type: 'Tailor').first_or_create!
    UserType.where(user_type: 'Video maker').first_or_create!
    UserType.where(user_type: 'Makeup artist').first_or_create!
    UserType.where(user_type: 'Hairdresser').first_or_create!
    UserType.where(user_type: 'Image consultant').first_or_create!
    UserType.where(user_type: 'Wedding Consultant').first_or_create!
    UserType.where(user_type: 'Nutritionist').first_or_create!
    UserType.where(user_type: 'Fitness Guru').first_or_create!
    UserType.where(user_type: 'Jeweller').first_or_create!
    UserType.where(user_type: 'Media').first_or_create!
  end
end
