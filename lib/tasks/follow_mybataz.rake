# frozen_string_literal: true

namespace :mybataz do
  desc 'Follow mybataz user by all current user'
  task follow: :environment do
    user_to_follow = User.where(email: User::MYBATAZ_EMAIL).first
    unless user_to_follow.nil?
      User.all.each do |user|
        user.toggle_follow!(user_to_follow) unless user.follows?(user_to_follow)
      end
    end
  end
end
