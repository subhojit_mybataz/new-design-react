namespace :abuse_type do
  desc 'Populate abuse type table data'
  task populate: :environment do
    AbuseType.where(name: 'I’m not interested in this Post.', abuse_type_for: 'Post').first_or_create!
    AbuseType.where(name: 'It’s spam.', abuse_type_for: 'Post').first_or_create!
    AbuseType.where(name: 'This content is inappropriate.', abuse_type_for: 'Post').first_or_create!
    AbuseType.where(name: 'This post should not be on Mybataz.', abuse_type_for: 'Post').first_or_create!
    AbuseType.where(name: 'Nudity content in this Post.', abuse_type_for: 'Post').first_or_create!
    AbuseType.where(name: 'I’m not interested in this Look.', abuse_type_for: 'Look').first_or_create!
    AbuseType.where(name: 'It’s spam.', abuse_type_for: 'Look').first_or_create!
    AbuseType.where(name: 'This content is inappropriate.', abuse_type_for: 'Look').first_or_create!
    AbuseType.where(name: 'This look should not be on Mybataz.', abuse_type_for: 'Look').first_or_create!
    AbuseType.where(name: 'Nudity content in this Look.', abuse_type_for: 'Look').first_or_create!
    AbuseType.where(name: 'Report something shared by $ .', abuse_type_for: 'User').first_or_create!
    AbuseType.where(name: 'Report this profile.', abuse_type_for: 'User').first_or_create!
    AbuseType.where(name: 'It’s spam.', abuse_type_for: 'Comment').first_or_create!
    AbuseType.where(name: 'This comment is inappropriate.', abuse_type_for: 'Comment').first_or_create!
    AbuseType.where(name: 'Image quality is not appropriate.', abuse_type_for: 'Admin').first_or_create!
    AbuseType.where(name: 'Incorrect tagged product.', abuse_type_for: 'Admin').first_or_create!
    AbuseType.where(name: 'Brand and product tagged doesn’t match.', abuse_type_for: 'Admin').first_or_create!
    AbuseType.where(name: 'Counterfeit product (knockoffs).', abuse_type_for: 'Admin').first_or_create!
    AbuseType.where(name: 'Rotate/ Flip the image.', abuse_type_for: 'Admin').first_or_create!
    AbuseType.where(name: 'Not a genuine post.', abuse_type_for: 'Admin').first_or_create!
    AbuseType.where(name: 'Post/ Product not acceptable.', abuse_type_for: 'Admin').first_or_create!
    AbuseType.where(name: 'Multiple posts.', abuse_type_for: 'Admin').first_or_create!
    AbuseType.where(name: 'Over editing.', abuse_type_for: 'Admin').first_or_create!
    AbuseType.where(name: 'Incorrect Brand.', abuse_type_for: 'Admin').first_or_create!
    AbuseType.where(name: 'Nudity.', abuse_type_for: 'Admin').first_or_create!
    AbuseType.where(name: 'Screenshots.', abuse_type_for: 'Admin').first_or_create!
  end
end