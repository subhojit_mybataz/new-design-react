# frozen_string_literal: true

namespace :preference do
  desc 'Populate preference table'
  task populate: :environment do
    Preference.where(preference: 'Trendy').first_or_create!
    Preference.where(preference: 'Casual').first_or_create!
    Preference.where(preference: 'Ethnic').first_or_create!
    Preference.where(preference: 'Vibrant').first_or_create!
    Preference.where(preference: 'Sexy').first_or_create!
    Preference.where(preference: 'Elegant').first_or_create!
    Preference.where(preference: 'Bohemian').first_or_create!
    Preference.where(preference: 'Girly').first_or_create!
    Preference.where(preference: 'Formal').first_or_create!
    Preference.where(preference: 'Business Causal').first_or_create!
  end
end
