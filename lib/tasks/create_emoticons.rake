# frozen_string_literal: true

namespace :db do
  desc 'create emoticons for posts and looks'
  task create_emoticons: :environment do
    Emoticon.where(name: 'Sad').first_or_create!(url: 'https://mybataz.com', url_logo: 'https://mybataz.com')
    Emoticon.where(name: 'Happy').first_or_create!(url: 'https://mybataz.com', url_logo: 'https://mybataz.com')
    Emoticon.where(name: 'Stonned').first_or_create!(url: 'https://mybataz.com', url_logo: 'https://mybataz.com')
    Emoticon.where(name: 'Dead').first_or_create!(url: 'https://mybataz.com', url_logo: 'https://mybataz.com')
  end
end
