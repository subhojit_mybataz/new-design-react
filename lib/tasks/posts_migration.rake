# frozen_string_literal: true
require 'aws-sdk'
require 'open-uri'

namespace :db do
  desc 'stories with brand_id or product id should honour has_many through'
  task posts_migration: :environment do
    Story.where("brand_id is not NULL OR product_id is not null").find_in_batches(batch_size: 100).each do |stories|
      stories.each do |story|

        # migrate story brands 
        # handle posts_count and looks count in brands
        # handles brands_count in stories
        if story.brand_id.present?
          story.story_brands.create(brand_id: story.brand_id, brandable_id: story.id, brandable_type: story.type.capitalize)
          story.update(brands_count: story.brands_count + 1)
        end

        # migrate product brands
        # handles posts_count and looks_count in products
        # hanldes products_count in stories

        if story.product_id.present?
          story.story_products.create(product_id: story.product_id, productable_id: story.id, productable_type: story.type.capitalize)
          story.update(products_count: story.products_count + 1)
        end
      end
    end
  end

  task posts_image_migration: :environment do 
    # download original size image from s3 and use that file to create attachment for the storis
    Story.where("image_content_type is not NULL").find_in_batches(batch_size: 100).each do |stories|
      stories.each do |story|
        begin          
          fixed_dimension_image_url = story.image.url(:fixed_dimension)
          if fixed_dimension_image_url.include?('looks')
            fixed_dimension_image_url = fixed_dimension_image_url.gsub('looks', 'posts')
          end
          image_file = open(URI.parse(fixed_dimension_image_url))
          image_original_filename = fixed_dimension_image_url.split('/').last.split('?').first
          res = story.attachments.create!(image: image_file, attachable_id: story.id, attachable_type: story.type, file_type: 'image', image_file_name: image_original_filename, image_content_type: image_file.content_type)
          puts "==== images migrated for #{story.id}"
        rescue Exception => e
          puts "=== fails #{e.message} #{story.id}"
        end
      end
    end
  end

  task comments_follows_migration: :environment do 
    Comment.where(commentable_type: 'Post').find_in_batches(batch_size: 100).each do |comments|
      comments.each do |comment|
        comment.update(commentable_type: 'Story', association_type: 'Look')
      end
    end
    Follow.where(followable_type: 'Post').find_in_batches(batch_size: 100).each do |follows|
      follows.each do |follow|
        follow.update(followable_type: 'Story')
      end
    end
  end

  task migrate_user_referer_code: :environment do
    User.where("referer_code is NULL OR referer_code=''").find_in_batches(batch_size: 100).each do |users|
      users.each do |user|
        user.update(referer_code: SecureRandom.hex(5).upcase)
      end
    end
  end

  task migrate_post_as_look: :environment do 
    Story.all.find_in_batches(batch_size: 100).each do |stories|
      stories.each do |story|
        story.update(type: "Look")
      end
    end
  end

  task migrate_custom_brand_to_brand: :environment do
    Story.all.find_in_batches(batch_size: 100).each do |stories|
      stories.each do |story|
        if story.brand_id.nil?
          brand = Brand.find_or_create_by(name: story.custom_brand)
          brand.update(is_active: false)
          StoryBrand.create(brandable_id: story.id, brandable_type: 'Story', brand_id: brand.id)
        end
      end
    end
  end
end


# user total_posts rename to looks_count - done
# add column posts_count default 0 - done
# comments_count default 0



# remove brand_id and product_id from products
# remove image_file_name, image_content_type, image_file_size, image_updated_at

