namespace :brand do
  desc 'Brand Images and other data migrations'
  task image_migration: :environment do
    Brand.where("logo_content_type is not NULL").find_in_batches(batch_size: 100).each do |brands|
      brands.each do |brand|
        begin
          brand.logo.reprocess!
          puts "==== images migrated for #{brand.id}"
        rescue Exception => e
          puts "=== fails #{e.message} #{brand.id}"
        end
      end
    end
  end
end