namespace :point_rule do
  desc 'populate point table'
  task populate: :environment do
    PointRule.where(action_type: 'refer').first_or_create!(points: 10)
    PointRule.where(action_type: 'signup').first_or_create!(points: 50)
    PointRule.where(action_type: 'profile_completion').first_or_create!(points: 50)
    PointRule.where(action_type: 'rate_looks').first_or_create!(points: 10)
    PointRule.where(action_type: 'upload_looks').first_or_create!(points: 10)
    PointRule.where(action_type: 'contest').first_or_create!(points: 150)
    PointRule.where(action_type: 'chat_expert').first_or_create!(points: 500)
  end
end