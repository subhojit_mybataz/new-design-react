# frozen_string_literal: true

namespace :db do
  desc 'check existence of brand community page for each brand'
  task check_brand_community: :environment do
    brands = Brand.all
    brands.each do |brand|
      brand_id = brand.id
      brand_country = BrandCountry.where(brand_id: brand_id)
      next unless brand_country.empty?
      new_brand_country = BrandCountry.new
      new_brand_country.brand_id = brand_id
      new_brand_country.country_id = Country.find_by_name('India').id
      new_brand_country.save
    end
  end
end
