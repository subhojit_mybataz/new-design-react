# frozen_string_literal: true

namespace :db do
  desc 'User follows non-custom brand tagged in post'
  task user_follow_brand: :environment do
    posts = Post.all
    posts.each do |post|
      if post.brand_id.present?
        user = post.user
        user.follow!(post.brand) unless user.follows? post.brand
      end
    end
  end
end
