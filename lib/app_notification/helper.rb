# frozen_string_literal: true

class AppNotification::Helper
  OLD_APP_VERSION = 1.1
  PN_LIMIT = 100
  def check_app_version(user)
    unless user.app_version.nil?
      return true if user.app_version.to_f > OLD_APP_VERSION
    end
    false
  end

  def check_pn_limit(user)
    counter = PushNotifTracker.where('user_id = ? and created_at > ? and created_at < ?', user.id, 1.day.ago, Time.now)
    return true if counter.size < PN_LIMIT
    false
  end
end
