# frozen_string_literal: true

class Search::SearchManager
  attr_accessor :query_list, :page, :size, :resource, :search_model
  attr_reader :index_preloads, :resource_name, :sort, :order

  def initialize(resource_name, params, page, size, index_preloads)
    @query_list = params[:query].split(',') if params[:query].present?
    @query_list ||= []
    @page = page.to_i
    @size = size.to_i
    if %i[look post].include?(resource_name)
    	resource_name = 'story'
    end
    @resource_name = resource_name
    @resource = resource_name.to_s.split('_').map(&:capitalize).join('').constantize
    @search_model = "SearchModels::#{resource_name.to_s.split('_').map(&:capitalize).join('')}Model".constantize.new
  	@index_preloads = index_preloads
  	@sort = params[:sort] || search_model.sort_fields[:default][:field]
  	@order = params[:order] || search_model.sort_fields[:default][:order]
  end

  def search
    conditional_query = ''
    joins = []
    query_list.each_with_index do |query, index|
      values = query.split(':')
      key = values[0]
      key_fields = search_model.search_fields[key.to_sym]
      value = values[1]
      field_query = ''
      key_fields.each_with_index do |key_field, index_key_field|
        field_query += get_operator(value, key_field) + ' '
        field_query += 'OR ' unless index_key_field + 1 == key_fields.size
        conditional_query += "(#{key_field[:join_query]}) AND" if key_field[:join_query].present?
        joins << key_field[:join] unless key_field[:join].nil?
      end
      conditional_query += "(#{field_query})"
      conditional_query += ' AND ' unless index + 1 == query_list.size
    end
    join_query = ''
    joins.uniq.each do |join|
      join_query += "INNER JOIN #{join} "
    end
    # check if sort is relevance

    if sort == 'relevance' && resource_name == 'story'
    	# sql_query = build_relevance_query("stories.*", join_query, conditional_query, false)
    	# sql_query_count = build_relevance_query("stories.id", join_query, conditional_query, true)
      # ids = resource.find_by_sql(sql_query).pluck(:id)
    	# resources = resource.find_as_sorted(ids)

      resources = resource.where(conditional_query).joins(join_query).order(created_at: :desc).offset((page - 1) * size).take(size)

      resources_count = Story.all.count
    #elsif resource_name == :brand && query_list.blank? && conditional_query.blank?
    #  resources = resource.where(conditional_query).joins(join_query).eager_load(index_preloads).order(looks_count: :desc).offset((page - 1) * size).take(size)
    #  resources_count = resource.where(conditional_query).joins(join_query).count
		else
			resources = resource.where(conditional_query).joins(join_query).eager_load(index_preloads).offset((page - 1) * size).take(size)
    	resources_count = resource.where(conditional_query).joins(join_query).count
    end
    [resources, resources_count]
  end

  private

  def build_relevance_query(select_clause, join_query = "", conditional_query = "", count)
  	conditional_query = " AND #{conditional_query}" if conditional_query.present?
  	sql_query = "select #{select_clause} from
    	(
    		select 
    			stories.id, 
    			date_diff(#{Story::RANGE}, stories.created_at) as diff,
    			(#{Point::COMMENT_POINT} * stories.comments_count + #{Point::CLAP_POINT} * stories.claps_count) as score
    		from stories as stories
    		LEFT JOIN 
    			follows as f on f.follower_id = #{User.current.id} AND f.followable_type = 'Brand' and f.followable_id IN (select s_b.brand_id from story_brands as s_b where s_b.brandable_id = stories.id) #{join_query} 
    		WHERE 
    			stories.deleted_at is NULL
    			#{conditional_query}
        order by
          case 
            when (date_part('minutes', now() - stories.created_at) < 5)
              then 1
            else 0
          end desc,
          diff ASC,
          case 
            WHEN(f.id is not null)
              then 1
            else 0
          end DESC,
	        score DESC,
	        stories.created_at DESC
	    ) 
		as stories"
		sql_query = sql_query + " offset #{(page - 1)*size} limit #{size}" unless count
		sql_query
  end

  def get_operator(value, field)
    case field[:operator]
    when 'like'
      "#{field[:field]} ilike '%#{value}%'"
    when 'equal'
      "#{field[:field]} = '#{value}'"
    when 'bool'
      "#{field[:field]} = #{value}"
    end
  end
end
