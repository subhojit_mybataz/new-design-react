# frozen_string_literal: true

class SearchModels::UserTypeModel
  def search_fields
    {
      q: [{ field: 'user_types.user_type', join: nil, operator: 'like' }]
    }
  end

  def sort_fields
    {
      default: { field: 'user_types.created_at', join: nil, order: 'desc' }
    }
  end
end
