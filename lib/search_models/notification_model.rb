class SearchModels::NotificationModel
  def search_fields
    {
      trackable_type: [{ field: 'notifications.trackable_type' }]
    }
  end

  def sort_fields
    {
      default: { field: 'notifications.created_at', join: nil, order: 'desc' },
    }
  end
end
