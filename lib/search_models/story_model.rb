# frozen_string_literal: true

class SearchModels::StoryModel
  def search_fields
    {
      q: [{ field: 'stories.name', join: nil, operator: 'like' }, { field: 'stories.description', join: nil, operator: 'like' }],
      name: [{ field: 'stories.name', join: nil, operator: 'like' }],
      tag: [{field: 'story_tags.name', join: 'story_tags ON story_tags.tagable_id = stories.id',
             join_query: "story_tags.tagable_type IN ('Story')", operator: 'equal'}],
      description: [{ field: 'stories.description', join: nil, operator: 'like' }],
      brand_id: [{ field: 'story_brands.brand_id', join: 'story_brands ON story_brands.brandable_id = stories.id',
                   join_query: "story_brands.brandable_type IN ('Story')", operator: 'equal' }],
      user_id: [{field: 'stories.user_id', operator: 'equal'}],
      product_id: [{ field: 'story_products.product_id', join: 'story_products ON story_products.productable_id = stories.id',
                     join_query: "story_products.productable_type IN ('Story')", operator: 'equal' }],
      uuid: [{ field: "stories.uuid", operator: 'equal'}]
    }
  end

  def sort_fields
    {
      default: { field: 'stories.created_at', join: nil, order: 'desc' },
      name: { field: 'stories.name', join: nil },
      created_at: {field: 'stories.created_at', join: nil},
      popularity: {field: nil, join: nil}
    }
  end
end
