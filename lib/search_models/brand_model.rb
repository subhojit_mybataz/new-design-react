# frozen_string_literal: true

class SearchModels::BrandModel
  def search_fields
    {
      q: [{ field: 'brands.name', join: nil, operator: 'like' }],
      slug: [{ field: 'brands.slug', join: nil, operator: 'equal'}],
      trending: [{ field: 'brand_countries.trending', join: 'brand_countries on brands.id = brand_countries.brand_id', operator: 'bool' }],
      description: [{ field: 'brands.description', join: nil, operator: 'equal' }]
    }
  end

  def sort_fields
    {
      default: { field: 'brands.created_at', join: nil, order: 'desc' },
      name: { field: 'brands.name', join: nil },
      brand_rank: { field: 'brand_countries.brand_rank', join: 'brand_countries on brands.id = brand_countries.brand_id', order: 'equal' }
    }
  end
end
