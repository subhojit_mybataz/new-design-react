# frozen_string_literal: true

class SearchModels::AbuseTypeModel
  def search_fields
    {
      type: [{ field: 'abuse_types.abuse_type_for', join: nil, operator: 'equal'}],
    }
  end

  def sort_fields
    {
      default: { field: 'abuse_types.created_at', join: nil, order: 'desc' },
    }
  end
end
