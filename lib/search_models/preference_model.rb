# frozen_string_literal: true

class SearchModels::PreferenceModel
  def search_fields
    {
      q: [{ field: 'preferences.preference', join: nil, operator: 'like' }]
    }
  end

  def sort_fields
    {
      default: { field: 'preferences.created_at', join: nil, order: 'desc' }
    }
  end
end
