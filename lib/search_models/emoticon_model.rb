# frozen_string_literal: true

class SearchModels::EmoticonModel
  def search_fields
    {
      name: [{field: 'emoticons.name', join: nil, operator: 'like'}],
      id: [{field: 'emoticons.id', join: nil, operator: 'equal'}] 
    }
  end

  def sort_fields
    {
      default: { field: 'emoticons.created_at', join: nil, order: 'desc' },
    }
  end
end
