# frozen_string_literal: true

class SearchModels::UserModel
  def search_fields
    {
      q: [{ field: 'users.fname', join: nil, operator: 'like' }, { field: 'users.lname', join: nil, operator: 'like' }],
      fname: [{ field: 'users.fname', join: nil, operator: 'like' }],
      brand_id: [{ field: 'follows.followable_id', join: 'follows ON users.id = follows.follower_id', join_query: "follows.followable_type = 'Brand'", operator: 'equal' }],
      user_id: [{ field: 'follows.followable_id', join: 'follows ON users.id = follows.follower_id', join_query: "follows.followable_type = 'User' and follows.follower_type = 'User'", operator: 'equal' }],
      follower_id: [{ field: 'follows.follower_id', join: 'follows ON users.id = follows.followable_id', join_query: "follows.followable_type = 'User' and follows.follower_type = 'User'", operator: 'equal' }],
      trending: [{ field: 'users.top_user', operator: 'bool' }]
    }
  end

  def sort_fields
    {
      default: { field: 'users.created_at', join: nil, order: 'desc' }
    }
  end
end
