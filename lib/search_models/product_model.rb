class SearchModels::ProductModel
  def search_fields
    {
      q: [{ field: 'products.name', join: nil, operator: 'like' }],
    }
  end

  def sort_fields
    {
      default: { field: 'products.created_at', join: nil, order: 'desc' },
      name: { field: 'products.name', join: nil }
    }
  end
end
