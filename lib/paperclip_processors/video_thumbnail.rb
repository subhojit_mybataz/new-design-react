# frozen_string_literal: true

module Paperclip
  class VideoThumbnail < Processor
    attr_accessor :time_offset, :geometry, :whiny, :vframes

    def initialize(file, options = {}, attachment = nil)
      super
      @time_offset = options[:time_offset] || '-4'
      unless options[:geometry].nil? || (@geometry = Geometry.parse(options[:geometry])).nil?
        @geometry.width = (@geometry.width / 2.0).floor * 2.0
        @geometry.height = (@geometry.height / 2.0).floor * 2.0
        @geometry.modifier = ''
      end
      @vframes = options[:vframes] || 3
      @whiny = options[:whiny].nil? ? true : options[:whiny]
      @basename = File.basename(file.path, File.extname(file.path))
    end

    def make
      dst = Tempfile.new([@basename, 'jpg'].compact.join('.'))
      dst.binmode
      cmd = "-itsoffset #{time_offset} -i #{File.expand_path(file.path)} -y -vcodec mjpeg -vframes #{vframes} -an -f rawvideo "
      cmd += "-s #{geometry} " unless geometry.nil?
      cmd += File.expand_path(dst.path).to_s
      begin
        success = Paperclip.run('ffmpeg', cmd)
      rescue PaperclipCommandLineError
        raise PaperclipError, "There was an error processing the thumbnail for #{@basename}" if whiny
      end
      dst
    end
  end
end
