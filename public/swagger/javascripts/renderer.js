var myBataz = myBataz || {};

myBataz.ApiRenderer = {

  init: function (json_source) {
    $.ajax({
      type: "GET",
      url: json_source,
      cache: false,
      dataType: "json",
      success: function (response) {
        var requiredUrl = "http://json-schema.org/draft-04/schema";

        $.get(requiredUrl, "", function (data, starus, xhr) {
          var schema = data;
          var validator = new ZSchema();
          var valid = validator.validate(response, JSON.parse(schema));
          var errors = validator.getLastErrors();

          if (valid) {
            //Resolve all references in the Swagger Definition
            JsonRefs.resolveRefs(response, {
              //filter: ['relative', 'remote']
            })
              .then(function (res) {
                myBataz.ApiRenderer.renderSwaggerDoc(res.resolved);
              }, function (err) {
                console.log(err.stack);
              });
          } else {
            alert("Invalid swagger 2.0 file!");
          }

          errors === undefined
        });
      }
    });
  },

  renderSwaggerDoc: function (swaggerJson) {
    var ejs = new EJS({url: '/swagger/javascripts/views/tag.ejs'});
    $(".swagger-ui").html(ejs.render({swaggerJson: swaggerJson}));
  },

  getObjectSample: function (schema) {
    var example = schema.example || schema.items.example;
    return JSON.stringify(example, null, ' ');
  },

  submitGet: function (data, el) {
    var headers = {};
    var url = data.basepath + data.path + "?";
    var parent = el.parent().parent();

    data.parameters.forEach(function (param) {
      if (param.in == "header")
        headers[param.name] = parent.find(".api-parameter-value-" + param.name).val();

      if (param.in == "path")
        data.path = data.path.replace("{"+ param.name +"}", $(".api-parameter-value-" + param.name).val());
    });

    $.ajax({
      type: "GET",
      url: data.path,
      cache: false,
      headers: headers,
      contentType: 'application/json; charset=utf-8',
      dataType: "json",
      success: function (response) {
        parent.find(".execute-wrapper").addClass("hidden");
        myBataz.ApiRenderer.showServerResponse(myBataz.ApiRenderer.syntaxHighlight(JSON.stringify(response, undefined, 4)), parent);
      },
      error: function (xmlRequest) {
        parent.find(".execute-wrapper").addClass("hidden");
        alert(xmlRequest.status + ' \n\r ' +
          xmlRequest.statusText + '\n\r' +
          xmlRequest.responseText);
      }
    });
  },

  submitPost: function (data, el, type) {
    var headers = {};
    var url = data.path + "?";
    var parent = el.parent();

    data.parameters.forEach(function (param) {
      if (param.in == "header")
        headers[param.name] = parent.find(".api-parameter-value-" + param.name).val();

      if (param.in == "path")
        url = url.replace("{"+ param.name +"}", $(".api-parameter-value-" + param.name).val());
    });

    $.ajax({
      type: type,
      url: url,
      cache: false,
      headers: headers,
      contentType: 'application/json; charset=utf-8',
      dataType: "json",
      data: parent.find(".api-parameter-value-body").val(),
      success: function (response) {
        parent.find(".execute-wrapper").addClass("hidden");
        myBataz.ApiRenderer.showServerResponse(myBataz.ApiRenderer.syntaxHighlight(JSON.stringify(response, undefined, 4)), parent);
      },
      error: function (xmlRequest) {
        parent.find(".execute-wrapper").addClass("hidden");
        alert(xmlRequest.status + ' \n\r ' +
          xmlRequest.statusText + '\n\r' +
          xmlRequest.responseText);
      }
    });
  },

  //Helper methods for displaying results
  syntaxHighlight: function (json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
      var cls = 'number';
      if (/^"/.test(match)) {
        if (/:$/.test(match)) {
          cls = 'key';
        } else {
          cls = 'string';
        }
      } else if (/true|false/.test(match)) {
        cls = 'boolean';
      } else if (/null/.test(match)) {
        cls = 'null';
      }
      return '<span class="' + cls + '">' + match + '</span>';
    });
  },

  showServerResponse: function (res, el) {
    el.find(".server-response").find("pre").html(res);
    el.find(".server-response").show();
  }
};

$(function() {
  $(document).on("click", ".try-out", function () {
    var parent = $(this).parent().parent();
    parent.find("input, textarea, .execute-wrapper").removeClass("hidden");
    parent.find(".body-param__example").addClass("hidden");
    $(this).children().html("Cancel");
    $(this).addClass("cancel");
  });

  $(document).on("click", ".execute-wrapper", function() {
    var element = $(this);
    var stringJSON = $(this).find("input:hidden").val();
    var verb = $(this).data("verb");
    var data = JSON.parse(stringJSON);
    data.path = $(this).data("path");

    switch (verb) {
      case "get":
        myBataz.ApiRenderer.submitGet(data, element);
        break;
      case "post":
        myBataz.ApiRenderer.submitPost(data, element, "post");
        break;
      case "put":
        myBataz.ApiRenderer.submitPost(data, element, "put");
        break;
      case "delete":
        myBataz.ApiRenderer.submitPost(data, element, "delete");
        break;
    }
  });

  $(document).on("click", ".cancel", function () {
    var parent = $(this).parent().parent();
    parent.find("input, textarea, .execute-wrapper").addClass("hidden");
    parent.find(".body-param__example").removeClass("hidden");
    parent.find(".server-response").hide();
    $(this).children().html("Try it out");
    $(this).removeClass("cancel");
  });

  $(document).on( "click", ".opblock-tag", function() {
    $expand = $(this).find(".glyphicon");

    if($expand.hasClass("glyphicon-chevron-right")) {
      $expand.addClass("glyphicon-chevron-down").removeClass("glyphicon-chevron-right");
    } else {
      $expand.addClass("glyphicon-chevron-right").removeClass("glyphicon-chevron-down");
    }
  });
});